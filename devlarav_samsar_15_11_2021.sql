-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 15, 2021 at 11:45 AM
-- Server version: 5.7.36
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devlarav_samsar`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dribbble` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `address`, `phone`, `country`, `date_of_birth`, `role`, `status`, `gender`, `image`, `google`, `facebook`, `twitter`, `linkedin`, `skype`, `dribbble`, `remember_token`, `created_at`, `updated_at`) VALUES
(70, 'customer', '123', 'customer123', 'customer@farazisoft.com', '$2y$10$tTSsbjlAFJBGlRI0H5.2yeJMAP7feDHcASke3CUiHEiGu7lXMITHK', 'test purpoe', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '2019-01-12 05:57:53', '2019-01-12 05:57:53'),
(71, 'kaml', 'Farazi', 'kamalfarazi', 'kamal@gmail.com', '$2y$10$RLOX7oG0n7JDuih84cESTe9yxDdwL4oWA9HgVwPul2GU2N1hILm.i', 'Dhaka, Bangladesh', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '2019-01-13 11:34:37', '2019-01-13 11:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(100) NOT NULL,
  `order_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_add1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_add2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_add3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_pincode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_to_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dimensions_length` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dimensions_width` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dimensions_height` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internal_notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gift_wrap` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gift_messages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'initial',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `item_id`, `order_id`, `user_id`, `order_date`, `order_value`, `service`, `ship_to_name`, `ship_to_company`, `ship_to_add1`, `ship_to_add2`, `ship_to_add3`, `ship_to_state`, `ship_to_city`, `ship_to_pincode`, `ship_to_country`, `ship_to_phone`, `ship_to_email`, `total_weight`, `dimensions_length`, `dimensions_width`, `dimensions_height`, `customer_notes`, `internal_notes`, `gift_wrap`, `gift_messages`, `status`, `created_at`, `updated_at`) VALUES
(799, 'BA-2-1002', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'keith krieser', 'keith krieser', '3407 Mandeville Canyon Rd  ', NULL, NULL, 'CA', 'Los Angeles', '90049-1019', 'US', NULL, NULL, '5.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(800, 'BA-2-1003', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'Peter Manship', 'Peter Manship', '696 Rt 100n  ', NULL, NULL, 'VT', 'Ludlow', '5149', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(801, 'BA-2-1004', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'patricia eastland', 'patricia eastland', '229 west street ext.  ', NULL, NULL, 'NY', 'gloversville', '12078-6215', 'US', NULL, NULL, '2.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(802, 'BA-2-1005', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'Morgan West', 'Morgan West', '1310 Newfound Harbor Dr  ', NULL, NULL, 'FL', 'Merritt Island', '32952-2790', 'US', NULL, NULL, '9.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(803, 'BA-2-1006', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'Martha Clark', 'Martha Clark', '11418 Tartans Hill Road  ', NULL, NULL, 'KY', 'Goshen', '40026-9739', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(200, 'US-464', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Kim Green', 'Kim Green', '21 Sharrott St NW', NULL, NULL, 'AL', 'Hartselle', '35640-1524', 'US', NULL, NULL, '5.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-06-25 13:15:49', '2020-07-11 15:35:42'),
(199, 'US-463', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Donna Holmes', 'Sasha Hawke', '224 Falls St', 'Apt J', NULL, 'NC', 'Morganton', '28655-4361', 'US', NULL, NULL, '1.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-06-25 13:15:49', '2020-07-11 15:35:42'),
(198, 'US-462', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Kathleen E. Lindquester', 'Kathleen Lindquester', '727 SE Parrott St', NULL, NULL, 'OR', 'Roseburg', '97470-3460', 'US', NULL, NULL, '1.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-06-25 13:15:49', '2020-07-11 15:35:42'),
(197, 'US-461', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'martha Linkenhoker', 'martha Linkenhoker', '208 Village Ct', NULL, NULL, 'WV', 'Scott Depot', '25560-9350', 'US', NULL, NULL, '2.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-06-25 13:15:49', '2020-07-11 15:33:56'),
(807, 'BA-886', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Elizabeth Campo', 'Elizabeth Campo', '144 Center St  ', NULL, NULL, 'CT', 'Manchester', '06040-5007', 'US', NULL, NULL, '8.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(805, 'BA-2-1008', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'mariah suarez', 'mariah suarez', '1227 Pizarro St  ', NULL, NULL, 'FL', 'Coral Gables', '33134-2469', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(798, 'BA-2-1001', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'Polly Hathorn', 'Polly Hathorn', '5001 West lake drive  ', NULL, NULL, 'GA', 'Conyers', '30094-4404', 'US', NULL, NULL, '2.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(1170, 'RZ-1932', '80', 75, '7/21/2020', '9.99', 'Standard Shipping', 'REX HAYNES ', 'REX HAYNES ', '153 CALOOSA LAKE CIRCLE N ', NULL, NULL, 'FL', 'LAKE WALES', '33859-8604', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-21 12:34:47', '2020-07-21 12:35:21'),
(1171, 'RZ-1931', '80', 75, '7/21/2020', '9.99', 'Standard Shipping', 'KIMBERLY WALSH', 'KIMBERLY WALSH', 'PO BOX 525 NINE MILE ', NULL, NULL, 'WA', 'FALLS', '99026-0525', 'US', NULL, NULL, '3.25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-21 12:34:47', '2020-07-21 12:35:21'),
(1172, 'RZ-1930', '80', 75, '7/21/2020', '9.99', 'Standard Shipping', 'DONALD KASTEN', 'DONALD KASTEN', '107 E MAPLE ST APT 1 ', NULL, NULL, 'TN', 'JOHNSON CITY', '37601-6817', 'US', NULL, NULL, '0.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-21 12:34:47', '2020-07-21 12:35:21'),
(1173, 'RZ-1929', '80', 75, '7/21/2020', '9.99', 'Standard Shipping', 'ELAINE DOLD', 'ELAINE DOLD', '7600 ELK TIP DR ', NULL, NULL, 'MI', 'RAPID CITY', '49676-9637', 'US', NULL, NULL, '0.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-21 12:34:47', '2020-07-21 12:35:21'),
(266, 'US-461', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'martha Linkenhoker', 'martha Linkenhoker', '208 Village Ct', NULL, NULL, 'WV', 'Scott Depot', '25560-9350', 'US', NULL, NULL, '2.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-06-30 14:46:56', '2020-07-11 15:33:56'),
(267, 'US-462', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Kathleen E. Lindquester', 'Kathleen Lindquester', '727 SE Parrott St', NULL, NULL, 'OR', 'Roseburg', '97470-3460', 'US', NULL, NULL, '1.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-06-30 14:46:56', '2020-07-11 15:35:42'),
(268, 'US-463', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Donna Holmes', 'Sasha Hawke', '224 Falls St', 'Apt J', NULL, 'NC', 'Morganton', '28655-4361', 'US', NULL, NULL, '1.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-06-30 14:46:56', '2020-07-11 15:35:42'),
(269, 'US-464', '51', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Kim Green', 'Kim Green', '21 Sharrott St NW', NULL, NULL, 'AL', 'Hartselle', '35640-1524', 'US', NULL, NULL, '5.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-06-30 14:46:56', '2020-07-11 15:35:42'),
(1159, 'RZ-1943', '71', 75, '7/21/2020', '9.99', 'Standard Shipping', 'KATRINA FIEBIG ', 'KATRINA FIEBIG ', '99 CARRIER ST ', NULL, NULL, 'NC', 'ASHVILLE ', '28806-2442', 'US', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-21 12:34:47', '2020-07-21 12:39:43'),
(1160, 'RZ-1942', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'DEBORAH R LANE ', 'DEBORAH R LANE ', '3455 RELAY RD ', NULL, NULL, 'FL', 'ORMOND BEACH', '32174-7935', 'US', NULL, NULL, '1.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1161, 'RZ-1941', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'MARY FERLAUTO', 'MARY FERLAUTO', '42 WOODLAKE DR', NULL, NULL, 'NY', 'THIELLS', '10984-1304', 'US', NULL, NULL, '0.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1162, 'RZ-1940', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'RANDALL WHITTIER', 'RANDALL WHITTIER', '126 NW ELMWOOD AVE ', NULL, NULL, 'KS', 'TOPEKA', '66606-1203', 'US', NULL, NULL, '2.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1163, 'RZ-1939', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'D.M PETERS', 'D.M PETERS', '46727 SCHIMMEL CT ', NULL, NULL, 'MI', 'SHELBY TOWNSHIP', '48317-3880', 'US', NULL, NULL, '0.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1164, 'RZ-1938', '91', 76, '7/21/2020', '9.99', 'Standard Shipping', 'CRAIG NEW', 'CRAIG NEW', '1030 QUINCY  CT ', NULL, NULL, 'KY', 'BEREA ', '40403-8767', 'US', NULL, NULL, '1.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-23 06:23:37'),
(1165, 'RZ-1937', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'DENISE  GREENWADE ', 'DENISE  GREENWADE ', '187  CLEARWATER AVE NE ', NULL, NULL, 'OR', 'SALEM ', '97301-5240', 'US', NULL, NULL, '3.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1166, 'RZ-1936', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'CHERYL HERMANN', 'CHERYL HERMANN', '6017 HONEYGROVE ST', NULL, NULL, 'NV', 'LAS VEGAS', '89110-1886', 'US', NULL, NULL, '0.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1167, 'RZ-1935', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'BEVERLY LEWIS', 'BEVERLY LEWIS', '316 NATHAN DR ', NULL, NULL, 'NJ', 'CINNAMINSON', '08077-1583', 'US', NULL, NULL, '2.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1168, 'RZ-1934', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'DIANE   RAMSEY', 'DIANE   RAMSEY', '44 SOUTHWOOD DR ', NULL, NULL, 'CA', 'ORINDA ', '94563-3011', 'US', NULL, NULL, '0.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(393, 'US-461', '71', 75, '6/25/2020', '9.99', 'Standard Shipping', 'martha Linkenhoker', 'martha Linkenhoker', '208 Village Ct', NULL, NULL, 'WV', 'Scott Depot', '25560-9350', 'US', NULL, NULL, '2.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-03 10:53:17', '2020-07-11 15:33:56'),
(394, 'US-462', '71', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Kathleen E. Lindquester', 'Kathleen Lindquester', '727 SE Parrott St', NULL, NULL, 'OR', 'Roseburg', '97470-3460', 'US', NULL, NULL, '1.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-03 10:53:17', '2020-07-11 15:35:42'),
(395, 'US-463', '71', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Donna Holmes', 'Sasha Hawke', '224 Falls St', 'Apt J', NULL, 'NC', 'Morganton', '28655-4361', 'US', NULL, NULL, '1.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-03 10:53:17', '2020-07-11 15:35:42'),
(396, 'US-464', '71', 75, '6/25/2020', '9.99', 'Standard Shipping', 'Kim Green', 'Kim Green', '21 Sharrott St NW', NULL, NULL, 'AL', 'Hartselle', '35640-1524', 'US', NULL, NULL, '5.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-03 10:53:17', '2020-07-11 15:35:42'),
(1140, 'RZ-1963', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'ROBERT SUMMERS', 'ROBERT SUMMERS', '452 VALLE DEL MAR DRIVE', NULL, NULL, 'TX', 'SOCORRO', '79927', 'US', NULL, NULL, '5.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1141, 'RZ-1962', '92', 74, '7/21/2020', '9.99', 'Standard Shipping', 'KRISTEN HAMILTON', 'KRISTEN HAMILTON', '623 PEAR DR', NULL, NULL, 'CO', 'CANON CITY', '81212', 'US', NULL, NULL, '2.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-21 12:34:47', '2020-07-21 13:50:19'),
(1142, 'RZ-1961', '91', 76, '7/21/2020', '9.99', 'Standard Shipping', 'WUNCY ETAKE SBASHHOPA', 'WUNCY ETAKE SBASHHOPA', '6903 NE 79TH CT, STE 202', NULL, NULL, 'OR', 'PORTLAND', '97218-2815', 'US', NULL, NULL, '1.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-23 06:23:37'),
(1143, 'RZ-1960', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'ANTONIO PACHECO', 'ANTONIO PACHECO', '8500 BLUFFSTONE COVE #b203', NULL, NULL, 'TX', 'AUSTIN', '78759', 'US', NULL, NULL, '3.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1144, 'RZ-1959', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'PHILIP MARCHIONE', 'PHILIP MARCHIONE', '44 BERNARD STREET', NULL, NULL, 'NY', 'FARMINGDALE', '11735', 'US', NULL, NULL, '2.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1146, 'RZ-1957', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'ZENA FERMANO', 'ZENA FERMANO', '1279 RIVER ROAD', NULL, NULL, 'NJ', 'TEANECK', '7666', 'US', NULL, NULL, '1.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1147, 'RZ-1956', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'CARRIE THOMPSON', 'CARRIE THOMPSON', '8882 PURPLE LN', NULL, NULL, 'MD', 'ELKRIDGE', '21075', 'US', NULL, NULL, '2.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1148, 'RZ-1954', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'JOEL PASIGAN', 'JOEL PASIGAN', '523 CHARMINGDALE RD ', NULL, NULL, 'CA', 'DIAMOND BAR', '91765-2131', 'US', NULL, NULL, '1.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1149, 'RZ-1953', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'KRISTIN FJELDHEIM ', 'KRISTIN FJELDHEIM ', '333 N SUNNYVALE AVE ', NULL, NULL, 'CA', 'SUNNYVALE', '94085-4318', 'US', NULL, NULL, '1.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1150, 'RZ-1952', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'RANDALL WHITTIER', 'RANDALL WHITTIER', '126 NW ELMWOOD AVE ', NULL, NULL, 'KS', 'TOPEKA', '66606-1203', 'US', NULL, NULL, '2.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1151, 'RZ-1951', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DALE N. KINDRED', 'DALE N. KINDRED', '1033 S 74TH ST ', NULL, NULL, 'KS', 'KANSAS CITY', '66111-3278', 'US', NULL, NULL, '1.07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1152, 'RZ-1950', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DENISE GREENWADE', 'DENISE GREENWADE', '187 CLEARWATER AVE NE ', NULL, NULL, 'OR', 'SALEM', '97301-5240', 'US', NULL, NULL, '3.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1153, 'RZ-1949', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'GOLDEN GARY', 'GOLDEN GARY', '7725 W STENE DR.', NULL, NULL, 'CO', 'LITTLETON', '80128', 'US', NULL, NULL, '1.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1154, 'RZ-1948', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'SHERRY STANLEY', 'SHERRY STANLEY', '696 SAN RAMON VALLEY BLVD # 225', NULL, NULL, 'CA', 'DANVILLE', '94526-4022', 'US', NULL, NULL, '1.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1155, 'RZ-1947', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'ROSEMARY KLEIN', 'ROSEMARY KLEIN', '15 VALLEY RD. ', NULL, NULL, 'MA', 'LEXINGTON', '02421-4218', 'US', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1169, 'RZ-1933', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'STAN JONES', 'STAN JONES', '11 PONY EXPRESS DR ', NULL, NULL, 'NM', 'EDGEWOOD', '87015-6603', 'US', NULL, NULL, '1.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1176, 'RZ-1926', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'JOANNE K CARTER ', 'JOANNE K CARTER ', '36 MECKES ST ', NULL, NULL, 'NJ', 'SPRINGFIELD', '07081-2802', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1226, 'RZ-1875', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'BARCLAY  GILLAND', 'BARCLAY  GILLAND', '2529 STEVENS DR NE ', NULL, NULL, 'NM', 'ALBUQUERQUE', '87112-1559', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1225, 'RZ-1876', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'ANTHONY LEACH', 'ANTHONY LEACH', '1319   1ST AVE N APT 7', NULL, NULL, 'MI', 'ESCANABA', '49829-2846', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1224, 'RZ-1877', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DAN BOYD', 'DAN BOYD', '250 S ROSE DR SPC 5', NULL, NULL, 'CA', 'PLACENTIA', '92870-1005', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1223, 'RZ-1878', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'LAURA SERVILLAS', 'LAURA SERVILLAS', '91 MANDEVILLE CT', NULL, NULL, 'CA', 'OAKLY', '94561-2632', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1222, 'RZ-1879', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'JAMES RYAN', 'JAMES RYAN', '10 ROCKLAND ST APT 2 ', NULL, NULL, 'MA', 'WAKEFIELD', '01880-2425', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1221, 'RZ-1880', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'GEORGE TADLOCK', 'GEORGE TADLOCK', '2198-236 BLVD', NULL, NULL, 'ID', 'FAIRFIELD', '52556', 'US', NULL, NULL, '1.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:28'),
(1220, 'RZ-1881', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'BOB CARRAWAY ', 'BOB CARRAWAY ', '1399 SEATON RD', NULL, NULL, 'RD', 'RAYMOND ', '39154-9187', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1219, 'RZ-1882', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DAVID SCHWARZ', 'DAVID SCHWARZ', '1633 PRAIRIE GROVE DR', NULL, NULL, 'TX', 'HOUSTON ', '77077-3168', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1218, 'RZ-1883', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DONNA GALLENTINE', 'DONNA GALLENTINE', '112 FIRST ST P.O. BOX 145', NULL, NULL, 'PA', 'MATHER', '15346-0145', 'US', NULL, NULL, '0.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1217, 'RZ-1884', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DAN LOWRY', 'DAN LOWRY', '32344 1/4MI.OLD NASH RD. PO BOX 3015', NULL, NULL, 'AK', 'SEWARD', '99664-3015', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1216, 'RZ-1885', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'NICK BERGFIELD MSPSY', 'NICK BERGFIELD MSPSY', '245 COUNTY STREET  2800', NULL, NULL, 'OK', 'MINCO', '73059-8037', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1215, 'RZ-1886', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'GENE DAVIN ', 'GENE DAVIN ', '2525 S. VIRGINA DR.# 19', NULL, NULL, 'AZ', 'YUMA', '85364-7239', 'US', NULL, NULL, '0.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1214, 'RZ-1887', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'JESSE WILLIAM ', 'JESSE WILLIAM ', '180 ANDREW ST LOT 10', NULL, NULL, 'MN', 'MCGEORGE', '55760-5912', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1213, 'RZ-1888', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'JOHN SWANN', 'JOHN SWANN', '101 POINT SOUTH LN ', NULL, NULL, 'SC', 'LEXINGTON', '29073-9540', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1212, 'RZ-1889', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'KIMBERLEY RINKER', 'KIMBERLEY RINKER', '497 LUCY ST.', NULL, NULL, 'OH', 'AKRON', '44306-1821', 'US', NULL, NULL, '1.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1210, 'RZ-1891', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DONALD MACKENZIE', 'DONALD MACKENZIE', '3751 CENTRAL AVE APT 3 ', NULL, NULL, 'CA', 'SAN DIEGO', '92105', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1209, 'RZ-1892', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'SYED ZAIDI ', 'SYED ZAIDI ', '24 CHAIN O HILLS  ROAD ', NULL, NULL, 'NJ', 'ISELIN', '08830-2203', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1208, 'RZ-1893', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'JESSE MCCLURE ', 'JESSE MCCLURE ', '324 KENWOOD ST ', NULL, NULL, 'MO', 'FARMINGTON', '63640-1031', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1207, 'RZ-1894', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'ROGER HANSON', 'ROGER HANSON', '813 COLONIAL CT ', NULL, NULL, 'OH', 'VERMILION', '44089-3145', 'US', NULL, NULL, '1.07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1206, 'RZ-1895', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'MAX CARRIER', 'MAX CARRIER', '5301 BULWER AVE ', NULL, NULL, 'MO', 'SAINT LOUIS', '63147-3111', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1205, 'RZ-1896', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'KIRBY PUFFENBARGER', 'KIRBY PUFFENBARGER', '2132 WAKE FOREST RD ', NULL, NULL, 'VA', 'BLACKSBURG', '24060-0978', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1204, 'RZ-1897', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'GAYE KLEBBA ', 'GAYE KLEBBA ', '1769 N NEWBURGH  RD ', NULL, NULL, 'MI', 'WESTLAND', '48185-3313', 'US', NULL, NULL, '0.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1203, 'RZ-1898', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'SARAH POE', 'SARAH POE', '705 S BUCHANAN ST APT A ', NULL, NULL, 'OH', 'FREMONT', '43420-4546', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1202, 'RZ-1899', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'ROY C DUKE', 'ROY C DUKE', '1012 N HIGH ST', NULL, NULL, 'VA', 'FRANKLIN', '23851-1315', 'US', NULL, NULL, '3.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1201, 'RZ-1900', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'REBECCA GROSSO', 'REBECCA GROSSO', '21 OHELO PL ', NULL, NULL, 'HL', 'KULA', '96790', 'US', NULL, NULL, '1.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1200, 'RZ-1901', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'MOLLY DOUGAN ', 'MOLLY DOUGAN ', '6087 LEILANI DR ', NULL, NULL, 'CO', 'CASTLE  ROCK', '80108', 'US', NULL, NULL, '14.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1199, 'RZ-1902', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DEBORTH LEONARD ', 'DEBORTH LEONARD ', '86 ARLANTIC DRIVE ', NULL, NULL, 'FL', 'KEY LARGO', '33037-4312', 'US', NULL, NULL, '2.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1198, 'RZ-1903', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'JUAN MENDOZA', 'JUAN MENDOZA', '1926 GEORGE WASHINGTO WAY APT F3', NULL, NULL, 'WA', 'RICHLAND', '99354-2352', 'US', NULL, NULL, '3.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1197, 'RZ-1904', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'NYCOLE MISSLING', 'NYCOLE MISSLING', '124 BUCHANNON ST N ', NULL, NULL, 'MN', 'WATERVILLE', '56096', 'US', NULL, NULL, '1.07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1196, 'RZ-1905', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'SUMMER MILLER', 'SUMMER MILLER', '18493 DOCKERY RD', NULL, NULL, 'MO', 'RAYVILLE', '64084-9782', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1195, 'RZ-1907', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'IISA MACCI', 'IISA MACCI', '8791 SONOMA LAKE BLVD', NULL, NULL, 'FL', 'BOCA RATON', '33434-4068', 'US', NULL, NULL, '3.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1194, 'RZ-1908', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'THERESA GARZA', 'THERESA GARZA', '1001 SPRINGVIEW CT ', NULL, NULL, 'IN', 'VALPARAISO', '46383-0820', 'US', NULL, NULL, '1.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1193, 'RZ-1909', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'AMIE WILLIAMS', 'AMIE WILLIAMS', '2107 ELMGATE DR ', NULL, NULL, 'TX', 'HOUSTON ', '77080-6428', 'US', NULL, NULL, '3.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1192, 'RZ-1910', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DEBORTH REITER ANDERSON ', 'DEBORTH REITER ANDERSON ', '298 OSSEWEE PLAISANCE', NULL, NULL, 'WI', 'SPOONER', '54801-7341', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1191, 'RZ-1911', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'CARRIE THOMAS', 'CARRIE THOMAS', '330 E MILLAN ST ', NULL, NULL, 'CA', 'CHULA VISTA ', '91910-6314', 'US', NULL, NULL, '6.43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1190, 'RZ-1912', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'BRYONNA JONES', 'BRYONNA JONES', '580 25TH ST # P101', NULL, NULL, 'UT', 'OGDEN', '84401-2438', 'US', NULL, NULL, '1.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1189, 'RZ-1913', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'PAMELA VANN', 'PAMELA VANN', '3256 HIGHWAY 77 ', NULL, NULL, 'FL', 'CHIPLEY', '32428-4968', 'US', NULL, NULL, '3.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1188, 'RZ-1914', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DIANE HODGES', 'ALLE PROCESSING ON CORP', ' MAIN OFFICE ON SECOND FLOOR 5620 59TH ST', NULL, NULL, 'NY', 'MASPETH', '11378-2314', 'US', NULL, NULL, '1.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1187, 'RZ-1915', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'TINA PARKER ', 'TINA PARKER ', '14 TOMATO FARM RD ', NULL, NULL, 'NM', 'PERALTA', '87042-5317', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1186, 'RZ-1916', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DEBORAH R LANE', 'DEBORAH R LANE', '3455 RELAY  RD ', NULL, NULL, 'FL', 'ORMOND BEACH', '32174-7935', 'US', NULL, NULL, '0.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1185, 'RZ-1917', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'DONALD W LEE', 'DONALD W LEE', '1316 INDIAN PASS RD PORT SAINT', NULL, NULL, 'FL', 'JOE ', '32456-7810', 'US', NULL, NULL, '0.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1184, 'RZ-1918', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'KORRIE HOLMES', 'KORRIE HOLMES', '313 ORTING AVE NW ', NULL, NULL, 'WA', 'ORTING', '98360-8416', 'US', NULL, NULL, '1.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1183, 'RZ-1919', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'GARY MCCRACKEN', 'GARY MCCRACKEN', '8062 N DIAMOND VALLEY DR ', NULL, NULL, 'UT', 'ST GEORGE', '84770-6018', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1182, 'RZ-1920', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'CERJIO LEON', 'CERJIO LEON', '135 ESTES WAY ', NULL, NULL, 'CA', 'SACRAMENTO', '95838-2564', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1181, 'RZ-1921', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'LOUISE WHELAN', 'LOUISE WHELAN', '4101 FOX HOLLOW CT ', NULL, NULL, 'TX', 'MIDLAND', '79707-4634', 'US', NULL, NULL, '2.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1180, 'RZ-1922', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'KATE ', 'KATE ', '607 3RD ST LOT # 8', NULL, NULL, 'LA', 'RAYNE ', '70578-5109', 'US', NULL, NULL, '1.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1179, 'RZ-1923', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'GEORGE TADLOCK', 'GEORGE TADLOCK', '2198-236 BLVD', NULL, NULL, 'IA', 'FAIRFIELD', '52556', 'US', NULL, NULL, '1.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1178, 'RZ-1924', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'TAMMY MCINTOSH', 'TAMMY MCINTOSH', '20020 127TH AVE SE ', NULL, NULL, 'EA', 'SNOHOMISH', '98296-5428', 'US', NULL, NULL, '1.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(1177, 'RZ-1925', '84', 73, '7/21/2020', '9.99', 'Standard Shipping', 'MG BLANKENSHIP', 'MG BLANKENSHIP', '236 HAWKINSRD S', NULL, NULL, 'WA', 'WINLOCK', '98596', 'US', NULL, NULL, '1.07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(715, 'TEST-123', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'IMZADI VARNELL -PEREZ', 'IMZADI VARNELL -PEREZ', '4010 LOLA DRIVE ', NULL, NULL, 'NC', 'AYDEN ', '28513', 'US', NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(716, 'TEST-124', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'GARRETT  PERNEY ', 'GARRETT  PERNEY ', 'P.O.BOX 4599  PASO', NULL, NULL, 'CA', 'ROBLES', '93447', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(717, 'TEST-125', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'CHRISTINE ROTHNIE', 'CHRISTINE ROTHNIE', 'PO BOX  6724 ', NULL, NULL, 'HI', 'OCEAN VIEW ', '96737-6724', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(718, 'TEST-126', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'MAYA FREDO ', 'MAYA FREDO ', 'P.O.BOX 249 ', NULL, NULL, 'WV', 'GREAT CACAPON ', '25422', 'US', NULL, NULL, '6.9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(719, 'TEST-127', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'NOREEN DOBBELS', 'NOREEN DOBBELS', '311 12TH AVE', NULL, NULL, 'IL', 'SILVIS', '61282-1869', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(720, 'TEST-128', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'SHIRLEY ENGELHARDT', 'SHIRLEY ENGELHARDT', '1401 OCEAN AVE', 'APT 4H', NULL, 'NY', 'BROOKLYN', '11230-3908', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(721, 'TEST-129', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'DIANA POMPIE', 'DIANA POMPIE', '146 LESTER DR', NULL, NULL, 'WV', 'DANIELS', '25832-9557', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(722, 'TEST-130', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'VLAD DUBROVSKI', 'VLAD DUBROVSKI', '113 BRANDYWINE DR', NULL, NULL, 'PA', 'BETHLEHEM', '18020-9575', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(723, 'TEST-131', '50', 75, '7/8/20', '9.99', 'Standard Shipping', 'DONNA P SILVA', 'DONNA P SILVA', '3691 GATLIN DR', NULL, NULL, 'FL', 'VIERA', '32955-6050', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-11 12:22:42', '2020-07-11 12:25:50'),
(724, 'SHOE-03', '52', 75, '7/8/2020', '11.99', 'Standard Shipping', 'BESMIR MUSTAFAJ', 'BESMIR MUSTAFAJ', 'PO BOX 1856', NULL, NULL, 'NY', 'OSSINING', '10562', 'US', NULL, NULL, '10.71428571', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-14 15:45:24', '2020-07-14 15:47:42'),
(725, 'SHOE-02', '52', 75, '7/8/2020', '10.99', 'Standard Shipping', 'EDEN DARLING', 'EDEN DARLING', '315 N SHERMAN STREET JENNINGS', NULL, NULL, 'LA', 'LOUISIANA', '70546', 'US', NULL, NULL, '14.28571429', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-14 15:45:24', '2020-07-14 15:47:42'),
(726, 'SHOE-01', '52', 75, '7/8/2020', '9.99', 'Standard Shipping', 'MELISSA SIETSMA ', 'MELISSA SIETSMA ', 'PO BOX 1277', NULL, NULL, 'AK', 'TALKEETNA', '99676', 'US', NULL, NULL, '14.28571429', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-14 15:45:24', '2020-07-14 15:47:42'),
(809, 'BA-884', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'BETH WILLIS', 'BETH WILLIS', '1210 OAK ST APT 2  ', NULL, NULL, 'WV', 'KENOVA', '25530-1349', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(808, 'BA-885', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Betty Wright', 'Betty Wright', '1693 Kensington Cir  ', NULL, NULL, 'NC', 'Newton', '28658-9431', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(806, 'BA-2-1009', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'garris leisten-gregory', 'garris leisten-gregory', '850 SW Touchmark Way Apt 402 ', NULL, NULL, 'OR', 'Portland', '97225-6457', 'US', NULL, NULL, '3.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(804, 'BA-2-1007', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'Mary  Larios', 'Mary  Larios', '6117 Central Ave  ', NULL, NULL, 'MD', 'Capitol Heights', '20743-6160', 'US', NULL, NULL, '6.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(793, 'FA-8667', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'Martha Clark', 'Martha Clark', '11418 Tartans Hill Road  ', NULL, NULL, 'KY', 'Goshen', '40026-9739', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(794, 'FA-8668', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'Mary  Larios', 'Mary  Larios', '6117 Central Ave  ', NULL, NULL, 'MD', 'Capitol Heights', '20743-6160', 'US', NULL, NULL, '6.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(795, 'FA-8669', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'mariah suarez', 'mariah suarez', '1227 Pizarro St  ', NULL, NULL, 'FL', 'Coral Gables', '33134-2469', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(796, 'FA-8670', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'garris leisten-gregory', 'garris leisten-gregory', '850 SW Touchmark Way Apt 402 ', NULL, NULL, 'OR', 'Portland', '97225-6457', 'US', NULL, NULL, '3.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(797, 'BA-2-1000', '70', 74, '7/11/20', '9.99', 'Standard Shipping', 'Tae Bo', 'Tae Bo', '51844 Cheryl Dr  ', NULL, NULL, 'IN', 'Granger', '46530-6804', 'US', NULL, NULL, '0.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-16 08:55:25', '2020-07-16 08:58:58'),
(788, 'FA-8662', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'Polly Hathorn', 'Polly Hathorn', '5001 West lake drive  ', NULL, NULL, 'GA', 'Conyers', '30094-4404', 'US', NULL, NULL, '2.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(789, 'FA-8663', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'keith krieser', 'keith krieser', '3407 Mandeville Canyon Rd  ', NULL, NULL, 'CA', 'Los Angeles', '90049-1019', 'US', NULL, NULL, '5.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(790, 'FA-8664', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'Peter Manship', 'Peter Manship', '696 Rt 100n  ', NULL, NULL, 'VT', 'Ludlow', '5149', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(791, 'FA-8665', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'patricia eastland', 'patricia eastland', '229 west street ext.  ', NULL, NULL, 'NY', 'gloversville', '12078-6215', 'US', NULL, NULL, '2.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(792, 'FA-8666', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'Morgan West', 'Morgan West', '1310 Newfound Harbor Dr  ', NULL, NULL, 'FL', 'Merritt Island', '32952-2790', 'US', NULL, NULL, '9.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(783, 'BA-860', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'Martha Clark', 'Martha Clark', '11418 Tartans Hill Road  ', NULL, NULL, 'KY', 'Goshen', '40026-9739', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(784, 'BA-859', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'Mary  Larios', 'Mary  Larios', '6117 Central Ave  ', NULL, NULL, 'MD', 'Capitol Heights', '20743-6160', 'US', NULL, NULL, '6.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(785, 'BA-858', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'mariah suarez', 'mariah suarez', '1227 Pizarro St  ', NULL, NULL, 'FL', 'Coral Gables', '33134-2469', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(786, 'BA-857', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'garris leisten-gregory', 'garris leisten-gregory', '850 SW Touchmark Way Apt 402 ', NULL, NULL, 'OR', 'Portland', '97225-6457', 'US', NULL, NULL, '3.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(787, 'FA-8661', '69', 73, '7/11/20', '9.99', 'Standard Shipping', 'Tae Bo', 'Tae Bo', '51844 Cheryl Dr  ', NULL, NULL, 'IN', 'Granger', '46530-6804', 'US', NULL, NULL, '0.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 08:22:22', '2020-07-16 08:34:53'),
(778, 'BA-865', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'Polly Hathorn', 'Polly Hathorn', '5001 West lake drive  ', NULL, NULL, 'GA', 'Conyers', '30094-4404', 'US', NULL, NULL, '2.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(779, 'BA-864', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'keith krieser', 'keith krieser', '3407 Mandeville Canyon Rd  ', NULL, NULL, 'CA', 'Los Angeles', '90049-1019', 'US', NULL, NULL, '5.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(780, 'BA-863', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'Peter Manship', 'Peter Manship', '696 Rt 100n  ', NULL, NULL, 'VT', 'Ludlow', '5149', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(781, 'BA-862', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'patricia eastland', 'patricia eastland', '229 west street ext.  ', NULL, NULL, 'NY', 'gloversville', '12078-6215', 'US', NULL, NULL, '2.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(782, 'BA-861', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'Morgan West', 'Morgan West', '1310 Newfound Harbor Dr  ', NULL, NULL, 'FL', 'Merritt Island', '32952-2790', 'US', NULL, NULL, '9.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(773, 'BA-880', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Marcia Lins', 'Marcia Lins', '1217 SW Shoremont Ave  ', NULL, NULL, 'WA', 'Normandy Park', '98166-3651', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(774, 'BA-879', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Rich Parciany', 'Rich Parciany', '1500 Guthrie Dr  ', NULL, NULL, 'IL', 'Inverness', '60010-5723', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(775, 'BA-878', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Andrea Lehman', 'Andrea Lehman', '204 W McKinley St  ', NULL, NULL, 'NC', 'Mebane', '27302-3144', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(776, 'BA-877', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'michele tucker', 'michele tucker', '929 east main street  ', NULL, NULL, 'OR', 'Hillsboro', '97123-4227', 'US', NULL, NULL, '3.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(777, 'BA-866', '68', 75, '7/11/20', '9.99', 'Standard Shipping', 'Tae Bo', 'Tae Bo', '51844 Cheryl Dr  ', NULL, NULL, 'IN', 'Granger', '46530-6804', 'US', NULL, NULL, '0.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 06:48:32', '2020-07-16 21:29:34'),
(768, 'BA-885', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Betty Wright', 'Betty Wright', '1693 Kensington Cir  ', NULL, NULL, 'NC', 'Newton', '28658-9431', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(769, 'BA-884', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'BETH WILLIS', 'BETH WILLIS', '1210 OAK ST APT 2  ', NULL, NULL, 'WV', 'KENOVA', '25530-1349', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(770, 'BA-883', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Victoria Mclaughlin', 'Victoria Mclaughlin', '101 N Walnut St  ', NULL, NULL, 'IA', 'New London', '52645-1323', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(771, 'BA-882', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Andrea Rootham', 'Andrea Rootham', '602 State St NE  ', NULL, NULL, 'MN', 'Glyndon', '56547-4204', 'US', NULL, NULL, '2.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(772, 'BA-881', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Tony Lapaglia', 'Tony Lapaglia', '2795 FM 2223 Red barn house behind barn ', NULL, NULL, 'TX', 'Bryan', '77808-6003', 'US', NULL, NULL, '7.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(767, 'BA-886', '67', 75, '7/11/20', '9.99', 'Standard Shipping', 'Elizabeth Campo', 'Elizabeth Campo', '144 Center St  ', NULL, NULL, 'CT', 'Manchester', '06040-5007', 'US', NULL, NULL, '8.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-15 13:07:30', '2020-07-16 21:29:34'),
(762, 'BA1-Box 4', '63', 74, '06/05/20', '4.99', 'Standard Shipping', 'Krista A. Blakely', 'Krista A. Blakely', '702 Meadow Lark Ln', NULL, NULL, 'UT', 'Smithfield', '84335-1003', 'United States', NULL, NULL, '0.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-15 06:47:20', '2020-07-15 12:30:25'),
(763, 'BA2-Box 4', '63', 74, '06/05/20', '4.99', 'Standard Shipping', 'sheryl colburn', 'sheryl colburn', '59 Linden St Unit 1402', NULL, NULL, 'MA', 'taunton', '02780-3664', 'United States', NULL, NULL, '2.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-15 06:47:20', '2020-07-15 12:30:24'),
(764, 'BA3-Box 4', '63', 74, '06/05/20', '4.99', 'Standard Shipping', 'Robin Eckholt', 'Robin Eckholt', '12601 SE River Rd Apt 117', NULL, NULL, 'OR', 'Milwaukie', '97222-9708', 'United States', NULL, NULL, '1.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-15 06:47:20', '2020-07-15 12:30:25'),
(765, 'BA4-Box 4', '63', 74, '06/05/20', '4.99', 'Standard Shipping', 'Anita Neuburger', 'Anita Neuburger', '11037 Lynn Lake Circle', NULL, NULL, 'FL', 'Tampa', '33625-5642', 'United States', NULL, NULL, '4.19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-15 06:47:20', '2020-07-15 12:30:25'),
(766, 'BA5-Box 4', '63', 74, '06/05/20', '4.99', 'Standard Shipping', 'Becky S Love', 'Becky S Love', '13370 NW Westlawn Ter, Apt 6', NULL, NULL, 'OR', 'Portland', '97229-5547', 'United States', NULL, NULL, '3.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-15 06:47:20', '2020-07-15 12:30:24'),
(810, 'BA-883', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Victoria Mclaughlin', 'Victoria Mclaughlin', '101 N Walnut St  ', NULL, NULL, 'IA', 'New London', '52645-1323', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(811, 'BA-882', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Andrea Rootham', 'Andrea Rootham', '602 State St NE  ', NULL, NULL, 'MN', 'Glyndon', '56547-4204', 'US', NULL, NULL, '2.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(812, 'BA-881', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Tony Lapaglia', 'Tony Lapaglia', '2795 FM 2223 Red barn house behind barn ', NULL, NULL, 'TX', 'Bryan', '77808-6003', 'US', NULL, NULL, '7.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(813, 'BA-880', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Marcia Lins', 'Marcia Lins', '1217 SW Shoremont Ave  ', NULL, NULL, 'WA', 'Normandy Park', '98166-3651', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(814, 'BA-879', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Rich Parciany', 'Rich Parciany', '1500 Guthrie Dr  ', NULL, NULL, 'IL', 'Inverness', '60010-5723', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(815, 'BA-878', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Andrea Lehman', 'Andrea Lehman', '204 W McKinley St  ', NULL, NULL, 'NC', 'Mebane', '27302-3144', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(816, 'BA-877', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'michele tucker', 'michele tucker', '929 east main street  ', NULL, NULL, 'OR', 'Hillsboro', '97123-4227', 'US', NULL, NULL, '3.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(817, 'BA-876', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jenny Kucherawy', 'Jenny Kucherawy', '115 Wilmont Avenue  ', NULL, NULL, 'PA', 'Washington', '15301-3526', 'US', NULL, NULL, '2.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(818, 'BA-875', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Brian Smith', 'Brian Smith', '2100 USA Drive  ', NULL, NULL, 'TX', 'Plano', '75025-3109', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(819, 'BA-874', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Shari Williams', 'Shari Williams', '6029 Tompkins Trail  ', NULL, NULL, 'TX', 'Flower Mound', '75028-2321', 'US', NULL, NULL, '1.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(820, 'BA-873', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'kris  johnson', 'kris  johnson', '908 Lyle Ct  ', NULL, NULL, 'KY', 'Lagrange', '40031-2300', 'US', NULL, NULL, '5.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(821, 'BA-872', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Suzzana Mountain', 'Suzzana Mountain', '32035 SW Willamette Way E  ', NULL, NULL, 'OR', 'Wilsonville', '97070-9596', 'US', NULL, NULL, '2.07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(822, 'BA-871', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Mary Lynn Turner', 'Mary Lynn Turner', '6323 Springfield St  ', NULL, NULL, 'CA', 'San Diego', '92114-2025', 'US', NULL, NULL, '3.43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(823, 'BA-870', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Shane Henson ', 'Shane Henson ', '1835 N 64th Pl  ', NULL, NULL, 'AZ', 'Scottsdale', '85257-2539', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(824, 'BA-869', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Heather Jepsen', 'Heather Jepsen', '2372 Westcliffe Ln  ', NULL, NULL, 'CA', 'Walnut Creek', '94597-3375', 'US', NULL, NULL, '5.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34');
INSERT INTO `item` (`id`, `item_id`, `order_id`, `user_id`, `order_date`, `order_value`, `service`, `ship_to_name`, `ship_to_company`, `ship_to_add1`, `ship_to_add2`, `ship_to_add3`, `ship_to_state`, `ship_to_city`, `ship_to_pincode`, `ship_to_country`, `ship_to_phone`, `ship_to_email`, `total_weight`, `dimensions_length`, `dimensions_width`, `dimensions_height`, `customer_notes`, `internal_notes`, `gift_wrap`, `gift_messages`, `status`, `created_at`, `updated_at`) VALUES
(825, 'BA-868', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'April Fletchall', 'April Fletchall', '813 Del Rio Pike Apt D5  ', NULL, NULL, 'TN', 'Franklin', '37064-2185', 'US', NULL, NULL, '0.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(826, 'BA-867', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Steven Murdoch', 'Steven Murdoch', '6747 Pinebrooke Dr  ', NULL, NULL, 'OH', 'Hudson', '44236-3296', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(827, 'BA-866', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Tae Bo', 'Tae Bo', '51844 Cheryl Dr  ', NULL, NULL, 'IN', 'Granger', '46530-6804', 'US', NULL, NULL, '0.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(828, 'BA-865', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Polly Hathorn', 'Polly Hathorn', '5001 West lake drive  ', NULL, NULL, 'GA', 'Conyers', '30094-4404', 'US', NULL, NULL, '2.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(829, 'BA-864', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'keith krieser', 'keith krieser', '3407 Mandeville Canyon Rd  ', NULL, NULL, 'CA', 'Los Angeles', '90049-1019', 'US', NULL, NULL, '5.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(830, 'BA-863', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Peter Manship', 'Peter Manship', '696 Rt 100n  ', NULL, NULL, 'VT', 'Ludlow', '05149', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(831, 'BA-862', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'patricia eastland', 'patricia eastland', '229 west street ext.  ', NULL, NULL, 'NY', 'gloversville', '12078-6215', 'US', NULL, NULL, '2.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(832, 'BA-861', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Morgan West', 'Morgan West', '1310 Newfound Harbor Dr  ', NULL, NULL, 'FL', 'Merritt Island', '32952-2790', 'US', NULL, NULL, '9.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(833, 'BA-860', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Martha Clark', 'Martha Clark', '11418 Tartans Hill Road  ', NULL, NULL, 'KY', 'Goshen', '40026-9739', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(834, 'BA-859', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Mary  Larios', 'Mary  Larios', '6117 Central Ave  ', NULL, NULL, 'MD', 'Capitol Heights', '20743-6160', 'US', NULL, NULL, '6.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(835, 'BA-858', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'mariah suarez', 'mariah suarez', '1227 Pizarro St  ', NULL, NULL, 'FL', 'Coral Gables', '33134-2469', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(836, 'BA-857', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'garris leisten-gregory', 'garris leisten-gregory', '850 SW Touchmark Way Apt 402 ', NULL, NULL, 'OR', 'Portland', '97225-6457', 'US', NULL, NULL, '3.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(837, 'BA-856', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Rhonda Theriault', 'Rhonda Theriault', '117 E 4th St  ', NULL, NULL, 'WA', 'Cle Elum', '98922-1117', 'US', NULL, NULL, '2.43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(838, 'BA-855', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kathy Giffels', 'Kathy Giffels', '680 Autumn Valley Dr  ', NULL, NULL, 'MI', 'Ortonville', '48462-8313', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(839, 'BA-854', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kathleen E. Lindquester', 'Kathleen E. Lindquester', '727 SE Parrott St  ', NULL, NULL, 'OR', 'Roseburg', '97470-3460', 'US', NULL, NULL, '1.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(840, 'BA-853', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'JoAnn Okey', 'JoAnn Okey', '329 Ann Arbor St  ', NULL, NULL, 'MI', 'Manchester', '48158-9796', 'US', NULL, NULL, '2.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(841, 'BA-852', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Myra Carty', 'Myra Carty', '5068 Warner Rd  ', NULL, NULL, 'OH', 'Westerville', '43081-9331', 'US', NULL, NULL, '4.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(842, 'BA-851', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jennifer Mielcarek', 'Jennifer Mielcarek', '501 Gulf Rd # G501 ', NULL, NULL, 'NY', 'Cohoes', '12047-4982', 'US', NULL, NULL, '0.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(843, 'BA-850', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jennifer Scott', 'Jennifer Scott', '2555 Barbey Dr   ', NULL, NULL, 'UT', 'Salt lake city', '84109-1801', 'US', NULL, NULL, '4.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(844, 'BA-849', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'iloveallthisstuff', 'iloveallthisstuff', 'PO Box 2485  ', NULL, NULL, 'GA', 'Acworth', '30102-0009', 'US', NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(845, 'BA-848', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kristin Fjeldheim', 'Kristin Fjeldheim', '333 N Sunnyvale Ave  ', NULL, NULL, 'CA', 'Sunnyvale', '94085-4318', 'US', NULL, NULL, '0.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(846, 'BA-847', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Neal Austinson', 'Neal Austinson', '916 Nason St  ', NULL, NULL, 'CA', 'Santa Rosa', '95404-3810', 'US', NULL, NULL, '2.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(847, 'BA-846', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'NIna Berzinska', 'NIna Berzinska', '2445 SW 18th Ter Apt 1116  ', NULL, NULL, 'FL', 'Ft Lauderdale', '33315-2241', 'US', NULL, NULL, '2.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(848, 'BA-845', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Marie Flores', 'Marie Flores', '50 E 106th St Apt 11B ', NULL, NULL, 'NY', 'New York', '10029-4541', 'US', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(849, 'BA-844', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Carrie Baer', 'Carrie Baer', '1017 Queen ST  ', NULL, NULL, 'MI', 'Lansing', '48915-2229', 'US', NULL, NULL, '2.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(850, 'BA-843', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'LYNA LOW', 'LYNA LOW', '11614 Hazelnut Ct  ', NULL, NULL, 'OR', 'OREGON CITY', '97045-6750', 'US', NULL, NULL, '3.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(851, 'BA-842', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jo Chapman', 'Jo Chapman', '101 E South St  ', NULL, NULL, 'IL', 'Pinckneyville', '62274-1425', 'US', NULL, NULL, '4.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(852, 'BA-841', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Donn Eastwood', 'Donn Eastwood', '110 W Olympic Pl Apt 202 ', NULL, NULL, 'WA', 'Seattle', '98119-4752', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(853, 'BA-840', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Debra Winner', 'Debra Winner', '427 Shoemaker St  ', NULL, NULL, 'PA', 'Swoyersville', '18704-2032', 'US', NULL, NULL, '9.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(854, 'BA-839', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Pam DiMaggio', 'Pam DiMaggio', '805 W Main St  ', NULL, NULL, 'IL', 'Carmi', '62821-1351', 'US', NULL, NULL, '1.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(855, 'BA-838', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Cynthia Fisher', 'Cynthia Fisher', '37818 Argyle Rd Unit B ', NULL, NULL, 'CA', 'Fremont', '94536-4923', 'US', NULL, NULL, '0.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(856, 'BA-837', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Deborah Reiter Anderson', 'Deborah Reiter Anderson', '298 Oseewee Plaisance  ', NULL, NULL, 'WI', 'Spooner', '54801-7341', 'US', NULL, NULL, '2.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(857, 'BA-836', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'shayleen webb', 'shayleen webb', '1903 Hamburg St  ', NULL, NULL, 'MT', 'Anaconda', '59711-1731', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(858, 'BA-835', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Daria Selianova', 'Daria Selianova', '5216 Jackson st  ', NULL, NULL, 'TX', 'Houston', '77004-5925', 'US', NULL, NULL, '1.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(859, 'BA-834', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Cheryl Ricardo', 'Cheryl Ricardo', '308 Crisan Ct  ', NULL, NULL, 'FL', 'Orlando', '32824-6071', 'US', NULL, NULL, '1.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(860, 'BA-833', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Carla Besosa', 'Carla Besosa', '171 Somervelle St Apt 313 ', NULL, NULL, 'VA', 'Alexandria', '22304-8651', 'US', NULL, NULL, '6.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(861, 'BA-832', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'claudia zimmermann', 'claudia zimmermann', '3333 Santa Fe St  ', NULL, NULL, 'TX', 'Corpus Christi', '78411-1439', 'US', NULL, NULL, '1.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(862, 'BA-831', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Sabina Wise', 'Sabina Wise', '507 N 200 W  ', NULL, NULL, 'UT', 'Salt Lake City', '84103-1302', 'US', NULL, NULL, '0.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(863, 'BA-830', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'SUSAN C.NOLTING', 'SUSAN C.NOLTING', '15543 GARNET WAY  ', NULL, NULL, 'MN', 'APPLE VALLEY', '55124-5158', 'US', NULL, NULL, '2.25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(864, 'BA-829', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Bettina Tweddle', 'Bettina Tweddle', '10356 Sunstream Ln  ', NULL, NULL, 'FL', 'Boca Raton', '33428-4229', 'US', NULL, NULL, '13.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(865, 'BA-828', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Diana M. Wilder', 'Diana M. Wilder', '16 Summit Dr  ', NULL, NULL, 'CT', 'Windsor', '06095-3238', 'US', NULL, NULL, '3.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(866, 'BA-827', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Dana Greene-Coward', 'Dana Greene-Coward', '7405A Greenback Ln # 215 ', NULL, NULL, 'CA', 'Citrus Heights', '95610-5603', 'US', NULL, NULL, '4.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(867, 'BA-826', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'ANNE  HOWARD', 'ANNE  HOWARD', '3145 EL KU AVE  ', NULL, NULL, 'CA', 'ESCONDIDO', '92025-7333', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(868, 'BA-825', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'James Hill', 'James Hill', '1483 S Holt Ave  ', NULL, NULL, 'CA', 'Los Angeles', '90035-3609', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(869, 'BA-824', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Ana Kirk', 'Ana Kirk', '1329 River Oaks Dr  ', NULL, NULL, 'VA', 'Norfolk', '23502-2026', 'US', NULL, NULL, '0.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(870, 'BA-823', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Ana P Kiss', 'Ana P Kiss', '9 Suffolk Down  ', NULL, NULL, 'NY', 'Shoreham', '11786-1464', 'US', NULL, NULL, '1.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(871, 'BA-822', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Selene Smith', 'Selene Smith', '423 Broadway # 305  ', NULL, NULL, 'CA', 'Millbrae', '94030-1905', 'US', NULL, NULL, '2.07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(872, 'BA-821', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'harriet gollaher', 'harriet gollaher', '17955 pike 258  ', NULL, NULL, 'MO', 'bowling green', '63334-3832', 'US', NULL, NULL, '0.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(873, 'BA-820', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Betty Meyer', 'Betty Meyer', '3855 Madison Ave  ', NULL, NULL, 'UT', 'South Ogden', '84403-2203', 'US', NULL, NULL, '4.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(874, 'BA-819', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Yvette Lopez', 'Yvette Lopez', '2555 NW 207th St Apt 219 ', NULL, NULL, 'FL', 'Miami Gardens', '33056-5246', 'US', NULL, NULL, '8.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(875, 'BA-818', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Michele K Bingham', 'Michele K Bingham', '16823 Carrollton Creek Ln  ', NULL, NULL, 'TX', 'Houston', '77084-5879', 'US', NULL, NULL, '1.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(876, 'BA-817', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'willis cook', 'willis cook', '290 E. 16th Street  ', NULL, NULL, 'CA', 'Pittsburg', '94565-3807', 'US', NULL, NULL, '10.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(877, 'BA-816', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Earl Will', 'Earl Will', '417 1st Ave W PO#113 ', NULL, NULL, 'IA', 'Thompson', '50478-5030', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(878, 'BA-815', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Virginia Hampton', 'Virginia Hampton', '8520 Alydar Cir  ', NULL, NULL, 'TX', 'Boerne', '78015-4434', 'US', NULL, NULL, '10.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(879, 'BA-814', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Valerie Melson', 'Valerie Melson', '8233 Countrywood Rd NE  ', NULL, NULL, 'NM', 'Albuquerque', '87109-5263', 'US', NULL, NULL, '6.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(880, 'BA-813', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'L.A. Shubin', 'L.A. Shubin', '415 19th St Apt B ', NULL, NULL, 'CA', 'Huntington Beach', '92648-3833', 'US', NULL, NULL, '0.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(881, 'BA-812', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Tori Adams', 'Tori Adams', '2076 Riverside Dr  ', NULL, NULL, 'WY', 'Laramie', '82070-6620', 'US', NULL, NULL, '2.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(882, 'BA-811', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'lisa henn', 'lisa henn', '3438 Golden Fox Trl  ', NULL, NULL, 'OH', 'Lebanon', '45036-8263', 'US', NULL, NULL, '12.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(883, 'BA-810', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Tiffany Collins', 'Tiffany Collins', '582 W Market St Apt 4 ', NULL, NULL, 'PA', 'York', '17401-3797', 'US', NULL, NULL, '1.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(884, 'BA-809', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Vanessa THORNTON', 'Vanessa THORNTON', '9 valleyfield cv  ', NULL, NULL, 'TN', 'Jackson', '38305-6629', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(885, 'BA-808', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Natalie Porter', 'Natalie Porter', '2502 Walnut Hill Cir Apt 104 ', NULL, NULL, 'TX', 'Arlington', '76006-5111', 'US', NULL, NULL, '0.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(886, 'BA-807', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Theresa Garza', 'Theresa Garza', '1001 Springview Ct  ', NULL, NULL, 'IN', 'Valparaiso', '46383-0820', 'US', NULL, NULL, '0.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(887, 'BA-806', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Rich Parciany', 'Rich Parciany', '1500 Guthrie Dr  ', NULL, NULL, 'IL', 'Inverness', '60010-5723', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(888, 'BA-805', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'David Furniss', 'David Furniss', 'PO Box 1293  ', NULL, NULL, 'FL', 'Perry', '32348-1293', 'US', NULL, NULL, '1.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(889, 'BA-804', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Andrea Lehman', 'Andrea Lehman', '204 W McKinley St  ', NULL, NULL, 'NC', 'Mebane', '27302-3144', 'US', NULL, NULL, '3.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(890, 'BA-803', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Liza Whitaker', 'Liza Whitaker', '2034 NW 63rd Sreet  ', NULL, NULL, 'WA', 'Seattle', '98107-2406', 'US', NULL, NULL, '0.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(891, 'BA-802', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kasey Hanley', 'Kasey Hanley', '40000 Eureka Hill Rd  ', NULL, NULL, 'CA', 'Point Arena', '95468-8842', 'US', NULL, NULL, '2.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(892, 'BA-801', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jenny Kucherawy', 'Jenny Kucherawy', '115 Wilmont Avenue  ', NULL, NULL, 'PA', 'Washington', '15301-3526', 'US', NULL, NULL, '6.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(893, 'BA-800', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Lucy Zaccaria', 'Lucy Zaccaria', '8035 SW 17th St  ', NULL, NULL, 'FL', 'Miami', '33155-1317', 'US', NULL, NULL, '3.07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(894, 'BA-799', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Sharon J. Rossum', 'Sharon J. Rossum', '8901 Eton Ave Spc 100 ', NULL, NULL, 'CA', 'Canoga Park', '91304-0926', 'US', NULL, NULL, '0.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(895, 'BA-798', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'David and Brenda Dank', 'David and Brenda Dank', '407 Merrill St  ', NULL, NULL, 'WI', 'Sparta', '54656-1617', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(896, 'BA-797', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Suzzana Mountain', 'Suzzana Mountain', '32035 SW Willamette Way E  ', NULL, NULL, 'OR', 'Wilsonville', '97070-9596', 'US', NULL, NULL, '3.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(897, 'BA-796', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Brenda Erb', 'Brenda Erb', 'PO Box 568  ', NULL, NULL, 'MD', 'Chestertown', '21620-0568', 'US', NULL, NULL, '0.86', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(898, 'BA-795', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Mary Alice', 'Mary Alice', 'PO Box 13384  ', NULL, NULL, 'AZ', 'Scottsdale', '85267-3384', 'US', NULL, NULL, '0.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(899, 'BA-794', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Heather Jepsen', 'Heather Jepsen', '2372 Westcliffe Ln  ', NULL, NULL, 'CA', 'Walnut Creek', '94597-3375', 'US', NULL, NULL, '4.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(900, 'BA-793', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Sandra Matthews', 'Sandra Matthews', '177 Davis Rd  ', NULL, NULL, 'PA', 'Valencia', '16059-2007', 'US', NULL, NULL, '10.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(901, 'BA-792', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Salvador Hernandez', 'Salvador Hernandez', '516 NW 147TH ST  ', NULL, NULL, 'WA', 'VANCOUVER', '98685-5771', 'US', NULL, NULL, '1.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(902, 'BA-791', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Nichole  peacock', 'Nichole  peacock', '119 outfitter dr  ', NULL, NULL, 'TX', 'bastrop', '78602-3969', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(903, 'BA-790', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jacqueline Lee', 'Jacqueline Lee', '2408 Kaywood Ln  ', NULL, NULL, 'MD', 'Silver Spring', '20905-6409', 'US', NULL, NULL, '3.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(904, 'BA-789', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Irene Walker', 'Irene Walker', '302 E Fork Ln  ', NULL, NULL, 'TN', 'Crawford', '38554-3617', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(905, 'BA-788', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Deborah Coleman', 'Deborah Coleman', '2017 S 71st St  ', NULL, NULL, 'PA', 'Philadelphia', '19142-1132', 'US', NULL, NULL, '4.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(906, 'BA-787', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Christy Bickett', 'Christy Bickett', '1801 Clyburn Lane  ', NULL, NULL, 'FL', 'Deltona', '32738-4178', 'US', NULL, NULL, '4.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(907, 'BA-786', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Mylee Stratton', 'Mylee Stratton', '602 Canyon Rd  ', NULL, NULL, 'NV', 'Henderson', '89002-8016', 'US', NULL, NULL, '1.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(908, 'BA-785', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Angela S Saxon', 'Angela S Saxon', '1322 El Camino Real  ', NULL, NULL, 'CA', 'Redwood City', '94063-1809', 'US', NULL, NULL, '2.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(909, 'BA-784', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'All Things', 'All Things', '101 Airstrip Rd # 178 ', NULL, NULL, 'NC', 'Kill Devil Hills', '27948-8134', 'US', NULL, NULL, '2.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(910, 'BA-783', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Penny Boring', 'Penny Boring', '169 Wistar Rd  ', NULL, NULL, 'PA', 'Fairless Hills', '19030-4007', 'US', NULL, NULL, '1.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(911, 'BA-782', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Lisa Pack', 'Lisa Pack', '9562 Loveless Rd  ', NULL, NULL, 'OH', 'mechanicsburg', '43044-9535', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(912, 'BA-781', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Suzanne Pires', 'Suzanne Pires', '13719 Van Doren Rd  ', NULL, NULL, 'VA', 'Manassas', '20112-3801', 'US', NULL, NULL, '1.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(913, 'BA-780', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Karen Dyer', 'Karen Dyer', '4780 Smitty Circle  ', NULL, NULL, 'MD', 'White Plains', '20695-2895', 'US', NULL, NULL, '1.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(914, 'BA-779', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Piotr Bracichowicz', 'Piotr Bracichowicz', '79 Wolcott St Apt 404B ', NULL, NULL, 'NY', 'Brooklyn', '11231-1564', 'US', NULL, NULL, '9.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(915, 'BA-778', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'David Jann', 'David Jann', '108 Hopkins Rd  ', NULL, NULL, 'IN', 'indianapolis', '46229-3235', 'US', NULL, NULL, '2.25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(916, 'BA-777', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'paula trapp', 'paula trapp', '515 Tionda Dr N  ', NULL, NULL, 'OH', 'Vandalia', '45377-2316', 'US', NULL, NULL, '1.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(917, 'BA-776', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'REBECCA Quartarolo', 'REBECCA Quartarolo', '8439 Marina Vista Ln  ', NULL, NULL, 'CA', 'FAIR OAKS', '95628-3866', 'US', NULL, NULL, '2.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(918, 'BA-775', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Venessa Companion', 'Venessa Companion', '14 Sandy Point rd  ', NULL, NULL, 'NH', 'stratham', '03885-2120', 'US', NULL, NULL, '3.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(919, 'BA-774', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'M J DOYLE', 'M J DOYLE', '271 Pecos Way  ', NULL, NULL, 'NV', 'Las Vegas', '89121-2404', 'US', NULL, NULL, '1.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(920, 'BA-773', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'suzanne hughes', 'suzanne hughes', '1520 13th street  ', NULL, NULL, 'SC', 'cayce', '29033-3159', 'US', NULL, NULL, '0.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(921, 'BA-772', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Pita King', 'Pita King', '1126 Edgewood Ave N  ', NULL, NULL, 'FL', 'Jacksonville', '32254-2389', 'US', NULL, NULL, '2.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(922, 'BA-771', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Diane Walter', 'Diane Walter', '1445 Valley Ranch Cir  ', NULL, NULL, 'AZ', 'Prescott', '86303-6339', 'US', NULL, NULL, '2.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(923, 'BA-770', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kenny King', 'Kenny King', '3624 robinson mews  ', NULL, NULL, 'CA', 'san diego', '92103-4028', 'US', NULL, NULL, '2.43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(924, 'BA-769', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Olga Tillett', 'Olga Tillett', '1285 Eubanks Rd  ', NULL, NULL, 'TX', 'Seagoville', '75159-6013', 'US', NULL, NULL, '8.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(925, 'BA-768', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'melissa fox', 'melissa fox', '3190 Middle Dr  ', NULL, NULL, 'IA', 'Bettendorf', '52722-3308', 'US', NULL, NULL, '3.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(926, 'BA-767', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Susan Jamison', 'Susan Jamison', '16200 Edwards Ferry Rd  ', NULL, NULL, 'MD', 'Poolesville', '20837-9237', 'US', NULL, NULL, '13.25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(927, 'BA-766', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'martha miller', 'martha miller', '1300 Dexter Corner Rd  ', NULL, NULL, 'DE', 'townsend', '19734-9237', 'US', NULL, NULL, '2.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(928, 'BA-765', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'marsha forsyth', 'marsha forsyth', '1314 Pauline St  ', NULL, NULL, 'KS', 'augusta', '67010-1915', 'US', NULL, NULL, '7.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(929, 'BA-764', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Mary Hanley', 'Mary Hanley', '17650 Meadowbridge Dr  ', NULL, NULL, 'FL', 'Lutz', '33549-5528', 'US', NULL, NULL, '0.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(930, 'BA-763', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Mary Larsen', 'Mary Larsen', '136 E La Cienega Ave  ', NULL, NULL, 'AZ', 'Goodyear', '85338-1409', 'US', NULL, NULL, '0.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(931, 'BA-762', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'garris leisten-gregory', 'garris leisten-gregory', '850 SW Touchmark Way Apt 402 ', NULL, NULL, 'OR', 'Portland', '97225-6457', 'US', NULL, NULL, '3.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(932, 'BA-761', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Janet Hofbauer', 'Janet Hofbauer', '5515 SE 14th St E101 ', NULL, NULL, 'IA', 'Des Moines', '50320-1619', 'US', NULL, NULL, '0.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(933, 'BA-760', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'lisa macci', 'lisa macci', '8791 Sonoma Lake Blvd  ', NULL, NULL, 'FL', 'Boca Raton', '33434-4068', 'US', NULL, NULL, '7.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(934, 'BA-759', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'lorna mcmorris', 'lorna mcmorris', '3150 twisted oak loop  ', NULL, NULL, 'FL', 'kissimmee', '34744-9254', 'US', NULL, NULL, '3.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(935, 'BA-758', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Adriana  Kovacevich', 'Adriana  Kovacevich', '4239 Pheasant Dr  ', NULL, NULL, 'NV', 'Carson City', '89701-3542', 'US', NULL, NULL, '1.36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(936, 'BA-757', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'lynn ann levi', 'lynn ann levi', '13126 Anza Drive  ', NULL, NULL, 'CA', 'Saratoga', '95070-4038', 'US', NULL, NULL, '0.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(937, 'BA-756', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'laura reid', 'laura reid', '395 Arbor Trl SE  ', NULL, NULL, 'GA', 'marietta', '30067-6749', 'US', NULL, NULL, '1.82', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(938, 'BA-755', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kimberly OBoyle', 'Kimberly OBoyle', '4166 Stoney Brook Rd  ', NULL, NULL, 'NY', 'Oneida', '13421-3706', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(939, 'BA-754', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kathy Giffels', 'Kathy Giffels', '680 Autumn Valley Dr  ', NULL, NULL, 'MI', 'Ortonville', '48462-8313', 'US', NULL, NULL, '1.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(940, 'BA-753', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kathleen E. Lindquester', 'Kathleen E. Lindquester', '727 SE Parrott St  ', NULL, NULL, 'OR', 'Roseburg', '97470-3460', 'US', NULL, NULL, '2.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(941, 'BA-752', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kathy Moreland', 'Kathy Moreland', '2134 Easter Lane  ', NULL, NULL, 'LA', 'New Orleans', '70114-3420', 'US', NULL, NULL, '10.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(942, 'BA-751', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Kim E. Savoy', 'Kim E. Savoy', '18535 Boysenberry Dr Apt 321  ', NULL, NULL, 'MD', 'Gaithersburg', '20879-3690', 'US', NULL, NULL, '2.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(943, 'BA-750', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jeans Rocks and Gems', 'Jeans Rocks and Gems', '664 County Road H  ', NULL, NULL, 'WI', 'Kewaskum', '53040-9038', 'US', NULL, NULL, '3.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(944, 'BA-749', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jo Angela Maniaci, CMP', 'Jo Angela Maniaci, CMP', '26 10th St W Unit 1602 ', NULL, NULL, 'MN', 'Saint Paul', '55102-1040', 'US', NULL, NULL, '6.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(945, 'BA-748', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Myra Carty', 'Myra Carty', '5068 Warner Rd  ', NULL, NULL, 'OH', 'Westerville', '43081-9331', 'US', NULL, NULL, '9.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(946, 'BA-747', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jerilynn Sutton', 'Jerilynn Sutton', '37 grant st Apt 1 ', NULL, NULL, 'NY', 'Cohoes', '12047-2717', 'US', NULL, NULL, '0.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(947, 'BA-746', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jennifer Miller', 'Jennifer Miller', '2113 Smith Road  ', NULL, NULL, 'LA', 'Lake Charles', '70607-5036', 'US', NULL, NULL, '2.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(948, 'BA-745', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jennifer Mielcarek', 'Jennifer Mielcarek', '501 Gulf Rd # G501 ', NULL, NULL, 'NY', 'Cohoes', '12047-4982', 'US', NULL, NULL, '0.64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(949, 'BA-744', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Sandra sgorbati', 'Sandra sgorbati', '809 Diablo Ave apt 6 ', NULL, NULL, 'CA', 'novato', '94947-4034', 'US', NULL, NULL, '11.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(950, 'BA-743', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jeanette Rebold Rost', 'Jeanette Rebold Rost', 'PO Box 7  ', NULL, NULL, 'TX', 'Weir', '78674-0007', 'US', NULL, NULL, '4.21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(951, 'BA-742', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jane Miklich', 'Jane Miklich', '1055 Riverview Lane  ', NULL, NULL, 'TN', 'Ashland City', '37015-3904', 'US', NULL, NULL, '1.43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(952, 'BA-741', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jacqueline Engel', 'Jacqueline Engel', '11838 Susan Dr  ', NULL, NULL, 'CA', 'Granada Hills', '91344-2637', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(953, 'BA-740', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Jacquelyn Gannon', 'Jacquelyn Gannon', '11012 S Fairfield Ave  ', NULL, NULL, 'IL', 'Chicago', '60655-1814', 'US', NULL, NULL, '7.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(954, 'BA-739', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Larissa Shy', 'Larissa Shy', '8331 Panorama Ridge CT  ', NULL, NULL, 'CA', 'Spring Valley', '91977-6211', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(955, 'BA-738', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Gary Fauss', 'Gary Fauss', '10210 Wilshire Ave NE  ', NULL, NULL, 'NM', 'Albuquerque', '87122-3015', 'US', NULL, NULL, '6.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(956, 'BA-737', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Lauren Weaver', 'Lauren Weaver', '910 A 16 th St  ', NULL, NULL, 'SC', 'Port Royal', '29935-2133', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(957, 'BA-736', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Tracy Morton', 'Tracy Morton', '129 Quaker Hwy Apt 16 ', NULL, NULL, 'MA', 'Uxbridge', '01569-1608', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(958, 'BA-735', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Francine Rizzo', 'Francine Rizzo', '3333 Monument Rd Apt 1314 ', NULL, NULL, 'FL', 'Jacksonville', '32225-3765', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(959, 'BA-734', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'William Heyworth', 'William Heyworth', '8466 Irish Rd  ', NULL, NULL, 'MI', 'Otisville', '48463-9421', 'US', NULL, NULL, '0.46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(960, 'BA-733', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Ettamarie Wasson', 'Ettamarie Wasson', '120 Sunwest Dr  ', NULL, NULL, 'NC', 'Arden', '28704-8560', 'US', NULL, NULL, '4.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(961, 'BA-732', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Emily Carmen', 'Emily Carmen', '15 Leland Rd  ', NULL, NULL, 'MA', 'Westford', '01886-2603', 'US', NULL, NULL, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(962, 'BA-731', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'LYNA LOW', 'LYNA LOW', '11614 Hazelnut Ct  ', NULL, NULL, 'OR', 'OREGON CITY', '97045-6750', 'US', NULL, NULL, '9.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(963, 'BA-730', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'debora sayne', 'debora sayne', '1450 Mackey Ferry Rd W  ', NULL, NULL, 'IN', 'Mount Vernon', '47620-7017', 'US', NULL, NULL, '1.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(964, 'BA-729', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Deborah Letteau', 'Deborah Letteau', '6324 Dante Ln NW  ', NULL, NULL, 'NM', 'Albuquerque', '87114-5021', 'US', NULL, NULL, '1.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(965, 'BA-728', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Deborah Reiter Anderson', 'Deborah Reiter Anderson', '298 Oseewee Plaisance  ', NULL, NULL, 'WI', 'Spooner', '54801-7341', 'US', NULL, NULL, '2.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(966, 'BA-727', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Chrestie Flower', 'Chrestie Flower', '844 A Laurel Avenue  ', NULL, NULL, 'GA', 'Macon', '31211-3180', 'US', NULL, NULL, '5.39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(967, 'BA-726', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Daniel Welton', 'Daniel Welton', '1048 Rio Ln Apt A ', NULL, NULL, 'CA', 'Sacramento', '95822-1744', 'US', NULL, NULL, '1.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(968, 'BA-725', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Danielle Bellando', 'Danielle Bellando', '22 Burchell Blvd  ', NULL, NULL, 'NY', 'Bay Shore', '11706-7319', 'US', NULL, NULL, '0.93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(969, 'BA-724', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Dorothy  Baumgartner', 'Dorothy  Baumgartner', '5218 Brass Lantern Pl  ', NULL, NULL, 'MO', 'Saint Louis', '63128-3502', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(970, 'BA-723', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Crystal Berman', 'Crystal Berman', '6750 Doncaster Drive  ', NULL, NULL, 'OR', 'Gladstone', '97027-1024', 'US', NULL, NULL, '0.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(971, 'BA-722', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Lori Livingston', 'Lori Livingston', '3451 NE Alameda St  ', NULL, NULL, 'OR', 'Portland', '97212-1805', 'US', NULL, NULL, '3.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(972, 'BA-721', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Constantine del Rosario', 'Constantine del Rosario', '5541 Radford Ave Apt 21 ', NULL, NULL, 'CA', 'Valley Village', '91607-2231', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(973, 'BA-720', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Carla Besosa', 'Carla Besosa', '171 Somervelle St Apt 313 ', NULL, NULL, 'VA', 'Alexandria', '22304-8651', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(974, 'BA-719', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Deborah Hanny', 'Deborah Hanny', '389 Berryman Drive  ', NULL, NULL, 'NY', 'Amherst', '14226-4371', 'US', NULL, NULL, '1.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(975, 'BA-718', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Caroline Hennessy', 'Caroline Hennessy', 'PO Box 1063 36433 N Harney LN ', NULL, NULL, 'OR', 'Burns', '97720-7063', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(976, 'BA-717', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Carol Lee', 'Carol Lee', '4141 Karl Dr  ', NULL, NULL, 'CA', 'Diamond Springs', '95619-9204', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(977, 'BA-716', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Sabina Wise', 'Sabina Wise', '507 N 200 W  ', NULL, NULL, 'UT', 'Salt Lake City', '84103-1302', 'US', NULL, NULL, '2.29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(978, 'BA-715', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Marcella Brooker', 'Marcella Brooker', '2421 NE Irving St Apt 207  ', NULL, NULL, 'OR', 'Portland', '97232-2361', 'US', NULL, NULL, '0.57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(979, 'BA-714', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Brandi cook', 'Brandi cook', '7656 Highway 178 E  ', NULL, NULL, 'MS', 'Fulton', '38843-7318', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(980, 'BA-713', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Sandra Bryan', 'Sandra Bryan', '101 Settlers Ridge Rd  ', NULL, NULL, 'NC', 'Burnsville', '28714-8340', 'US', NULL, NULL, '0.89', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(981, 'BA-712', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'SUSAN C.NOLTING', 'SUSAN C.NOLTING', '15543 GARNET WAY  ', NULL, NULL, 'MN', 'APPLE VALLEY', '55124-5158', 'US', NULL, NULL, '7.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(982, 'BA-711', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Pat Pascale', 'Pat Pascale', 'PO Box 5151  ', NULL, NULL, 'CA', 'Oroville', '95966-0151', 'US', NULL, NULL, '1.04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(983, 'BA-710', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Diana M. Wilder', 'Diana M. Wilder', '16 Summit Dr  ', NULL, NULL, 'CT', 'Windsor', '06095-3238', 'US', NULL, NULL, '3.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(984, 'BA-709', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Stacy Aralica', 'Stacy Aralica', '9021 Cape Wood Ct Box 3 ', NULL, NULL, 'NV', 'Las Vegas', '89117-2321', 'US', NULL, NULL, '0.43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(985, 'BA-708', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'ANNE  HOWARD', 'ANNE  HOWARD', '3145 EL KU AVE  ', NULL, NULL, 'CA', 'ESCONDIDO', '92025-7333', 'US', NULL, NULL, '0.79', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(986, 'BA-707', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Patricia Biagioni', 'Patricia Biagioni', '203 Ridge rd  ', NULL, NULL, 'FL', 'Jupiter', '33477-9660', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(987, 'BA-706', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Leslie  French', 'Leslie  French', '2316 Ranch Way  ', NULL, NULL, 'KS', 'Lawrence', '66047-3324', 'US', NULL, NULL, '2.25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(988, 'BA-705', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Amber Reichelt', 'Amber Reichelt', '1506 Nolan St  ', NULL, NULL, 'TX', 'San Antonio', '78202-2441', 'US', NULL, NULL, '0.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(989, 'BA-704', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Lisa M. Fantinato', 'Lisa M. Fantinato', '634 Severn Rd  ', NULL, NULL, 'MD', 'Severna Park', '21146-3421', 'US', NULL, NULL, '11.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(990, 'BA-703', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Aliyliyah  Jansma', 'Aliyliyah  Jansma', '4515 Westchester Glen Dr  ', NULL, NULL, 'TX', 'Grand Prairie', '75052-3550', 'US', NULL, NULL, '3.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(991, 'BA-702', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Dale Adams', 'Dale Adams', '1861 raven ave unit a8  ', NULL, NULL, 'CO', 'Estes Park', '80517-9417', 'US', NULL, NULL, '8.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34');
INSERT INTO `item` (`id`, `item_id`, `order_id`, `user_id`, `order_date`, `order_value`, `service`, `ship_to_name`, `ship_to_company`, `ship_to_add1`, `ship_to_add2`, `ship_to_add3`, `ship_to_state`, `ship_to_city`, `ship_to_pincode`, `ship_to_country`, `ship_to_phone`, `ship_to_email`, `total_weight`, `dimensions_length`, `dimensions_width`, `dimensions_height`, `customer_notes`, `internal_notes`, `gift_wrap`, `gift_messages`, `status`, `created_at`, `updated_at`) VALUES
(992, 'BA-701', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Betty Meyer', 'Betty Meyer', '3855 Madison Ave  ', NULL, NULL, 'UT', 'South Ogden', '84403-2203', 'US', NULL, NULL, '7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(993, 'BA-700', '72', 75, '7/11/2020', '9.99', 'Standard Shipping', 'Cathy Baillet', 'Cathy Baillet', '2459 Fir St  ', NULL, NULL, 'IL', 'Glenview', '60025-2705', 'US', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:28:55', '2020-07-16 21:29:34'),
(994, 'BA-C-1', '73', 75, '7/11/20', '9.99', 'Standard Shipping', 'Elizabeth Campo', 'Elizabeth Campo', '144 Center St  ', NULL, NULL, 'CT', 'Manchester', '06040-5007', 'US', NULL, NULL, '8.14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:35:55', '2020-07-16 21:36:05'),
(995, 'BA-C-2', '73', 75, '7/11/20', '9.99', 'Standard Shipping', 'Betty Wright', 'Betty Wright', '1693 Kensington Cir  ', NULL, NULL, 'NC', 'Newton', '28658-9431', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:35:55', '2020-07-16 21:36:05'),
(996, 'BA-C-3', '73', 75, '7/11/20', '9.99', 'Standard Shipping', 'BETH WILLIS', 'BETH WILLIS', '1210 OAK ST APT 2  ', NULL, NULL, 'WV', 'KENOVA', '25530-1349', 'US', NULL, NULL, '0.61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:35:55', '2020-07-16 21:36:05'),
(997, 'BA-C-4', '73', 75, '7/11/20', '9.99', 'Standard Shipping', 'Victoria Mclaughlin', 'Victoria Mclaughlin', '101 N Walnut St  ', NULL, NULL, 'IA', 'New London', '52645-1323', 'US', NULL, NULL, '0.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:35:55', '2020-07-16 21:36:05'),
(998, 'BA-C-5', '73', 75, '7/11/20', '9.99', 'Standard Shipping', 'Andrea Rootham', 'Andrea Rootham', '602 State St NE  ', NULL, NULL, 'MN', 'Glyndon', '56547-4204', 'US', NULL, NULL, '2.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:35:55', '2020-07-16 21:36:05'),
(999, 'BA-C-6', '73', 75, '7/11/20', '9.99', 'Standard Shipping', 'Tony Lapaglia', 'Tony Lapaglia', '2795 FM 2223 Red barn house behind barn ', NULL, NULL, 'TX', 'Bryan', '77808-6003', 'US', NULL, NULL, '7.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-16 21:35:55', '2020-07-16 21:36:05'),
(1000, 'US-1084', '74', 75, '7/17/2020', '9.99', 'Standard Shipping', 'MIZUHO SATO', 'MIZUHO SATO', '1739 PALOLO AVE APT E', NULL, NULL, 'HI', 'HONOLULU', '96816', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 10:42:02'),
(1001, 'US-1083', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'EDGAR STEPHENS', 'EDGAR STEPHENS', '11199 YUCCA TERRACE DR', NULL, NULL, 'CA', 'OAK HILLS', '92344-0275', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1002, 'US-1082', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'CATHERINE HERNANDE', 'CATHERINE HERNANDE', '784 PORT MONMOUTH RD', 'APT 5', NULL, 'NJ', 'PORT MONMOUTH', '07758-1630', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1003, 'US-1081', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'JANIE BURKHART', 'JANIE BURKHART', 'PO BOX 172', NULL, NULL, 'CA', 'BEN LOMOND', '95005', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1004, 'US-1080', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'PAMELA DUPRE', 'PAMELA DUPRE', '24 N BRISTOL CT', NULL, NULL, 'IL', 'MUNDELEIN', '60060-3240', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1005, 'US-1079', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'KATHY COURTRUGHT', 'KATHY COURTRUGHT', '4906 SECLUDED OAKS LN', NULL, NULL, 'CA', 'CARMICHAEL', '95608-0887', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1006, 'US-1078', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'JEDEDIAH SMITH', 'JEDEDIAH SMITH', '31 OLIVA DR', NULL, NULL, 'CA', 'NOVATO', '94947-7127', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1007, 'US-1077', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'TONYA SAYER ', 'TONYA SAYER ', '118 J N FLETCHER ROAD', NULL, NULL, 'LA', 'COLFAX', '71417', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1008, 'US-1076', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'RYAN SHORT', 'RYAN SHORT', '316 WICHITA DR. APT. 42', NULL, NULL, 'KY', 'NICHOLASCILLE', '40356', 'US', NULL, NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1009, 'US-1075', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'JOAN ORENDER', 'JOAN ORENDER', '7206 BEAVER CREEK LN', NULL, NULL, 'NE', 'LINCOLN', '68516-5604', 'US', '4024505917', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1010, 'US-1074', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'DIANE CACHO', 'DIANE CACHO', '8939 CADILLAC AVE', 'APT 101', NULL, 'CA', 'LOS ANELES', '90034-2044', 'US', '9548815062', NULL, '7.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1011, 'US-1073', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'stacey williams', 'stacey williams', 'po box 906', NULL, NULL, 'CO', 'arvada', '80001-0906', 'US', '7202975561', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1012, 'US-1072', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'DANNY GUTIERREZ', 'DANNY GUTIERREZ', '1250 N State college Blvd #69', NULL, NULL, 'CA', 'Anaheim', '92806-1574', 'US', '7142034886', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1013, 'US-1071', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Katherine Krohn', 'Katherine Krohn', '7 Eton Ct', NULL, NULL, 'CA', 'Berkeley', '94705-2715', 'US', '5106587930', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1014, 'US-1070', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'M J DOYLE', 'M J DOYLE', '271 Pecos Way', NULL, NULL, 'NV', 'Las Vegas', '89121-2404', 'US', '7025217244', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1015, 'US-1069', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Pamela Howard', 'Pamela Howard', '99 Bays Road', NULL, NULL, 'KY', 'Grayson', '41143-7592', 'US', '6064655831', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1016, 'US-1068', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Sandra Bryan', 'Sandra Bryan', '101 Settlers Ridge Rd', NULL, NULL, 'NC', 'Burnsville', '28714-8340', 'US', '8286823385', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1017, 'US-1067', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'catherine anderson', 'catherine anderson', '202 indian pte. drive', NULL, NULL, 'VA', 'Hardy', '24101-2624', 'US', '5407216329', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1018, 'US-1066', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Vicki Bitters', 'Vicki Bitters', '11714 Hallwood Drive', NULL, NULL, 'CA', 'El Monte', '91732-1438', 'US', '6264483802', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1019, 'US-1065', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Jamie Woodson', 'Jamie Woodson', '3927 Ingersoll Ave', 'Apt 203', NULL, 'IA', 'Des Moines', '50312-3512', 'US', '5154803250', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1020, 'US-1064', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Sam Swain', 'Sam Swain', '98 Woodside Ave', NULL, NULL, 'NY', 'buffalo', '14220-1907', 'US', '7163190816', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1021, 'US-1063', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Jodi Beer', 'Jodi Beer', '7168 Highway 31 N', NULL, NULL, 'AR', 'Lonoke', '72086-8903', 'US', '5014383961', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1022, 'US-1062', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'maureen riordan', 'maureen riordan', '5506 w rogers street', NULL, NULL, 'WI', 'West Allis', '53219-1528', 'US', '4143274271', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1023, 'US-1061', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Seneca Corriveau', 'Seneca Corriveau', '2007 North Rd', NULL, NULL, 'ME', 'Gilead', '04217-8120', 'US', '2075578245', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1024, 'US-1060', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Lila Cross', 'Lila Cross', '2999 Hamilton Ave', NULL, NULL, 'WI', 'Altoona', '54720-1192', 'US', '7154107257', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1025, 'US-1059', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Ashley R Culps', 'Ashley R Culps', '9373 E Potter Rd', NULL, NULL, 'MI', 'Davison', '48423-8156', 'US', '2486369312', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1026, 'US-1058', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Linda Moorhouse', 'Linda Moorhouse', '17 Ansaldi Rd', NULL, NULL, 'CT', 'Manchester', '06040-6206', 'US', '8606431295', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1027, 'US-1057', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Jennifer Rychlick', 'Jennifer Rychlick', '5725 Witzel Rd SE', NULL, NULL, 'OR', 'Salem', '97317-9142', 'US', '5032659980', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1028, 'US-1056', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Bridgett Garballey James', 'Bridgett Garballey James', '45 maynard street', NULL, NULL, 'MA', 'arlington', '02474-2317', 'US', '6172813547', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1029, 'US-1055', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Stephani M Astolfi', 'Stephani M Astolfi', '119 Grand Ave', NULL, NULL, 'NJ', 'Ridgefield Park', '07660-1216', 'US', '2014409640', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1030, 'US-1054', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Brenda Brebeck', 'Brenda Brebeck', '119 E Main St', 'back apartment', NULL, 'NY', 'Frankfort', '13340-1005', 'US', '3158687704', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1031, 'US-1053', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Phyllis Magal', 'Phyllis Magal', 'PO Box 315', NULL, NULL, 'HI', 'Honaunau', '96726-0315', 'US', '8087698762', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1032, 'US-1052', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Monica Leon', 'Monica Leon', '1031 W B Street', 'Apt 23', NULL, 'CA', 'Ontario', '91762-3344', 'US', '9095454547', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1033, 'US-1051', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Aesha Elgin', 'Aesha Elgin', '815 SE 223rd Ave', 'Apt 33', NULL, 'OR', 'Gresham', '97030-2539', 'US', '5037032781', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1034, 'US-1050', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'samantha roth', 'samantha roth', '5526 hillvue rd', NULL, NULL, 'WA', 'Blaine', '98230-9532', 'US', '3604839332', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1035, 'US-1049', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Stephanie Blume', 'Stephanie Blume', '1904 NE Highland St', NULL, NULL, 'OR', 'Portland', '97211-5321', 'US', '6783614625', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1036, 'US-1048', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Wesley Shores', 'Wesley Shores', '2334 Vicki Dr', NULL, NULL, 'TX', 'Abilene', '79606-1221', 'US', '3256659956', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1037, 'US-1047', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Dr William B Mount', 'Dr William B Mount', '8864 S D St', NULL, NULL, 'WA', 'Tacoma', '98444-6459', 'US', '2536866290', NULL, '4.32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1038, 'US-1046', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Jamie Woodson', 'Jamie Woodson', '3927 Ingersoll Ave', 'Apt 203', NULL, 'IA', 'Des Moines', '50312-3512', 'US', '5154803250', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1039, 'US-1045', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'robert bachmann', 'robert bachmann', '562 Saint Johns Rd', NULL, NULL, 'FL', 'Tavares', '32778-5117', 'US', '4107616886', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1040, 'US-1044', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Jerry Nolde', 'Jerry Nolde', '1707 E Blanchard Ave Lot 50', NULL, NULL, 'KS', 'Hutchinson', '67501-8158', 'US', '6202009542', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1041, 'US-1043', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Catherine C. Cupps', 'Catherine C. Cupps', 'CCAD', '60 Cleveland Avenue', NULL, 'OH', 'Columbus', '43215-3808', 'US', '6142183771', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1042, 'US-1042', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Khalell Rubio', 'Khalell Rubio', '812 Evergreen Farm Drive', NULL, NULL, 'TX', 'Temple', '76502-5357', 'US', '2549136319', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1043, 'US-1041', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Marlena McLaughlin ~ Take 1 Boutique', 'Marlena McLaughlin ~ Take 1 Boutique', '548 Green Meadows Dr', NULL, NULL, 'PA', 'Dallastown', '17313-9628', 'US', '7176005451', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1044, 'US-1040', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Rebecca S. Bell', 'Rebecca S. Bell', '504 Rockdale Rd', NULL, NULL, 'WV', 'Follansbee', '26037-1926', 'US', '3045270572', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1045, 'US-1039', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Raffaella Deuel Berman', 'Raffaella Deuel Berman', '300 Fairwood Ave', 'Apt 31', NULL, 'FL', 'Clearwater', '33759-3129', 'US', '7274235556', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1046, 'US-1038', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Antoinette Quarshie', 'Antoinette Quarshie', '474 W 238th St Apt 4E', NULL, NULL, 'NY', 'Bronx', '10463-2028', 'US', '9173286798', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1047, 'US-1037', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Rachel  White', 'Rachel  White', '1230 E 100 S', NULL, NULL, 'UT', 'Salt Lake City', '84102-1707', 'US', '8012320240', NULL, '15.25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1048, 'US-1036', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Bettina Tweddle', 'Bettina Tweddle', '4750 N Federal Highway Suite#5', 'Att: Bettinas Nails', NULL, 'FL', 'Lighthouse Point', '33064-6553', 'US', '7542450138', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1049, 'US-1035', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Sherri Wilson', 'Sherri Wilson', '5601 Tahoe Pines Rd', NULL, NULL, 'CA', 'Bakersfield', '93312-4191', 'US', '6617030051', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1050, 'US-1034', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Cheri Lowery', 'Cheri Lowery', 'PO Box 1755', NULL, NULL, 'MT', 'East Helena', '59635-1755', 'US', '4064373771', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1051, 'US-1033', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Marci Saunders', 'Marci Saunders', '1171 Browns Crossroads Rd', NULL, NULL, 'NC', 'Staley', '27355-8015', 'US', '3366227451', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1052, 'US-1032', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'maria escobar', 'maria escobar', '69 Bellevue Ave', 'A', NULL, 'CA', 'Daly City', '94014-1423', 'US', '4153703380', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1053, 'US-1031', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Anne L. Yarborough', 'Anne L. Yarborough', '201 Summerwinds Dr', NULL, NULL, 'NC', 'Cary', '27518-9640', 'US', '9198592215', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1054, 'US-1030', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Roberta Dolezal', 'Roberta Dolezal', '3411 Purdue St', NULL, NULL, 'MD', 'Hyattsville', '20783-1912', 'US', '3013263268', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1055, 'US-1029', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Shauna Ashby', 'Shauna Ashby', '1922 Sky Meadow Ave', NULL, NULL, 'WA', 'Richland', '99352-8910', 'US', '5093963061', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1056, 'US-1028', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Sharon Yglesia', 'Sharon Yglesia', '1712 Caldwell Rd', NULL, NULL, 'FL', 'South Daytona', '32119-1904', 'US', '3864518020', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1057, 'US-1027', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Phyllis Magal', 'Phyllis Magal', 'PO Box 315', NULL, NULL, 'HI', 'Honaunau', '96726-0315', 'US', '8087698762', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1058, 'US-1026', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Tamera Jemison', 'Tamera Jemison', '501 S 3rd Ave', NULL, NULL, 'WV', 'Paden City', '26159-1213', 'US', '3373778588', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1059, 'US-1025', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Pamela Bartus', 'Pamela Bartus', '17370', 'Susquehanna Trail Sourh', NULL, 'PA', 'New Freedom', '17349-8947', 'US', '4435069013', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1060, 'US-1024', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Barbara & Reich', 'Barbara & Reich', '17799 Cadena Dr', NULL, NULL, 'FL', 'Boca Raton', '33496-1068', 'US', '9546382374', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1061, 'US-1023', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Mikhail Galburt', 'Mikhail Galburt', '1671 Santa Cruz Ave', NULL, NULL, 'CA', 'Santa Clara', '95051-2922', 'US', '4084238068', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1062, 'US-1022', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Valerie Goodman', 'Valerie Goodman', 'PO Box 1722', NULL, NULL, 'CA', 'Grass Valley', '95945-1722', 'US', '5302103881', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1063, 'US-1021', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Stephanie Blume', 'Stephanie Blume', '1904 NE Highland St', NULL, NULL, 'OR', 'Portland', '97211-5321', 'US', '6783614625', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1064, 'US-1020', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Rich Parciany', 'Rich Parciany', '1500 Guthrie Dr', NULL, NULL, 'IL', 'Inverness', '60010-5723', 'US', '8472693780', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1065, 'US-1019', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Teri Marino', 'Teri Marino', '9511 NW 80th St', NULL, NULL, 'FL', 'Tamarac', '33321-1352', 'US', '7139626657', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1066, 'US-1018', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'M. T. Gray', 'M. T. Gray', '1739 San Benito St.', NULL, NULL, 'CA', 'Richmond', '94804-5328', 'US', '7076964605', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1067, 'US-1017', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Pamela Bartus', 'Pamela Bartus', '17370', 'Susquehanna Trail Sourh', NULL, 'PA', 'New Freedom', '17349-8947', 'US', '4435069013', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1068, 'US-1016', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Rich Parciany', 'Rich Parciany', '1500 Guthrie Dr', NULL, NULL, 'IL', 'Inverness', '60010-5723', 'US', '8472693780', NULL, '8.35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1069, 'US-1015', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Susan Cowart', 'Susan Cowart', '3 BrandyWine Ct', NULL, NULL, 'TX', 'Amarillo', '79119-6277', 'US', '4326497133', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1070, 'US-1014', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'jim martelli', 'jim martelli', '159 Blanchard Rd', NULL, NULL, 'PA', 'Drexel Hill', '19026-2805', 'US', '6109995887', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1071, 'US-1013', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Mack Besharse', 'Mack Besharse', 'PO BOX 573', NULL, NULL, 'AR', 'MONETTE', '72447-0573', 'US', '8708187257', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1072, 'US-1012', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Jeannette  Wallover', 'Jeannette  Wallover', '5 Spruce Ct', NULL, NULL, 'PA', 'Newtown', '18940-3215', 'US', '6096725992', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1073, 'US-1011', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Shanna  Webb', 'Shanna  Webb', '1371 Pedro St', 'Apt 3', NULL, 'CA', 'San Jose', '95126-3842', 'US', '4086074065', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1074, 'US-1010', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Jessica Green', 'Jessica Green', '3680 Gardenside Ct', NULL, NULL, 'GA', 'Alpharetta', '30004-0010', 'US', '7274653242', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1075, 'US-1009', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Cheyanne Ingram', 'Cheyanne Ingram', '904 SE Sienna Ct', NULL, NULL, 'MO', 'Blue Springs', '64014-7834', 'US', '8162600476', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1076, 'US-1008', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Nikki Grumbein', 'Nikki Grumbein', '6206 Chatham Glenn Way', NULL, NULL, 'PA', 'Harrisburg', '17111-4270', 'US', '7175747077', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1077, 'US-1007', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Rikki Scott', 'Rikki Scott', '412 Alvarado Dr SE', 'Apt 409W', NULL, 'NM', 'Albuquerque', '87108-2958', 'US', '5055500615', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1078, 'US-1006', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Linda E Innis', 'Linda E Innis', '1208 Kingston Blvd', NULL, NULL, 'OK', 'Edmond', '73034-3226', 'US', '4053970703', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1079, 'US-1005', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Edward L. Gates III', 'Edward L. Gates III', '1523 Main St SW', NULL, NULL, 'VA', 'Roanoke', '24015-2538', 'US', '5409043237', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1080, 'US-1004', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'Lisa Thorpe', 'Lisa Thorpe', '804 Willow Road', NULL, NULL, 'NJ', 'Toms River', '08753-7810', 'US', '7325069747', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1081, 'US-1003', '74', 76, '7/17/2020', '9.99', 'Standard Shipping', 'xiaoying yang', 'xiaoying yang', '1300 NE Miami Gardens Dr', 'Apt 418E', NULL, 'FL', 'Miami', '33179-4756', 'US', '7869161000', NULL, '3.55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 06:48:08', '2020-07-20 06:48:31'),
(1082, 'US-1155', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'GAIL TAYLOR', 'GAIL TAYLOR', 'PO BOX 1455 1200 TAOYLOR RD', NULL, NULL, 'CA', 'BETHEL ISLAND', '94511', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1083, 'US-1154', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'BURNU WATKINS', 'BURNU WATKINS', 'PO BOX 13129 ', NULL, NULL, 'AK', 'TRAPPER CREEK', '99683-0129', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1084, 'US-1130', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'Jeannette  Wallover', 'Jeannette  Wallover', '5 Spruce Ct', NULL, NULL, 'PA', 'Newtown', '18940-3215', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1085, 'US-1129', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'M. T. Gray', 'M. T. Gray', '1739 San Benito St.', NULL, NULL, 'CA', 'Richmond', '94804-5328', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1086, 'US-1128', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'maria escobar', 'maria escobar', '69 Bellevue Ave', 'A', NULL, 'CA', 'Daly City', '94014-1423', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1087, 'US-1127', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'Sherri Wilson', 'Sherri Wilson', '5601 Tahoe Pines Rd', NULL, NULL, 'CA', 'Bakersfield', '93312-4191', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1088, 'US-1126', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'Catherine C. Cupps', 'Catherine C. Cupps', 'CCAD', '60 Cleveland Avenue', NULL, 'OH', 'Columbus', '43215-3808', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1089, 'US-1125', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'Marci Saunders', 'Marci Saunders', '1171 Browns Crossroads Rd', NULL, NULL, 'NC', 'Staley', '27355-8015', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1090, 'US-1124', '75', 75, '7/18/20', '9.99', 'Standard Shipping', 'Khalell Rubio', 'Khalell Rubio', '812 Evergreen Farm Drive', NULL, NULL, 'TX', 'Temple', '76502-5357', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 08:23:18', '2020-07-20 08:23:33'),
(1091, 'SKY-BOX 1', '76', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Krista A. Blakely', 'Krista A. Blakely', '702 Meadow Lark Ln', NULL, NULL, 'UT', 'Smithfield', '84335-1003', 'United States', NULL, NULL, '0.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 08:37:21', '2020-07-20 08:56:24'),
(1092, 'SKY-BOX 2', '76', 74, '06-05-2020', '4.99', 'Standard Shipping', 'sheryl colburn', 'sheryl colburn', '59 Linden St Unit 1402', NULL, NULL, 'MA', 'taunton', '02780-3664', 'United States', NULL, NULL, '2.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 08:37:21', '2020-07-20 08:56:24'),
(1093, 'SKY-BOX 3', '76', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Robin Eckholt', 'Robin Eckholt', '12601 SE River Rd Apt 117', NULL, NULL, 'OR', 'Milwaukie', '97222-9708', 'United States', NULL, NULL, '1.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 08:37:21', '2020-07-20 08:56:24'),
(1094, 'SKY-BOX 4', '76', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Anita Neuburger', 'Anita Neuburger', '11037 Lynn Lake Circle', NULL, NULL, 'FL', 'Tampa', '33625-5642', 'United States', NULL, NULL, '4.19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 08:37:21', '2020-07-20 08:56:24'),
(1095, 'SKY-BOX 5', '76', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Becky S Love', 'Becky S Love', '13370 NW Westlawn Ter, Apt 6', NULL, NULL, 'OR', 'Portland', '97229-5547', 'United States', NULL, NULL, '3.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 08:37:21', '2020-07-20 08:56:24'),
(1096, 'US-1160', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'GAIL TAYLOR', 'GAIL TAYLOR', 'PO BOX 1455 1200 TAOYLOR RD', NULL, NULL, 'CA', 'BETHEL ISLAND', '94511', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1097, 'US-1161', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'BURNU WATKINS', 'BURNU WATKINS', 'PO BOX 13129 ', NULL, NULL, 'AK', 'TRAPPER CREEK', '99683-0129', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1098, 'US-1162', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'Jeannette  Wallover', 'Jeannette  Wallover', '5 Spruce Ct', NULL, NULL, 'PA', 'Newtown', '18940-3215', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1099, 'US-1163', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'M. T. Gray', 'M. T. Gray', '1739 San Benito St.', NULL, NULL, 'CA', 'Richmond', '94804-5328', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1100, 'US-1164', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'maria escobar', 'maria escobar', '69 Bellevue Ave', 'A', NULL, 'CA', 'Daly City', '94014-1423', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1101, 'US-1165', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'Sherri Wilson', 'Sherri Wilson', '5601 Tahoe Pines Rd', NULL, NULL, 'CA', 'Bakersfield', '93312-4191', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1102, 'US-1166', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'Catherine C. Cupps', 'Catherine C. Cupps', 'CCAD', '60 Cleveland Avenue', NULL, 'OH', 'Columbus', '43215-3808', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1103, 'US-1167', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'Marci Saunders', 'Marci Saunders', '1171 Browns Crossroads Rd', NULL, NULL, 'NC', 'Staley', '27355-8015', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1104, 'US-1168', '77', 75, '7/18/20', '9.99', 'Standard Shipping', 'Khalell Rubio', 'Khalell Rubio', '812 Evergreen Farm Drive', NULL, NULL, 'TX', 'Temple', '76502-5357', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'booked', '2020-07-20 08:48:31', '2020-07-20 08:49:26'),
(1105, 'AK-BOX 1', '78', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Krista A. Blakely', 'Krista A. Blakely', '702 Meadow Lark Ln', NULL, NULL, 'UT', 'Smithfield', '84335-1003', 'United States', NULL, NULL, '0.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 09:22:10', '2020-07-20 09:27:31'),
(1106, 'AK-BOX 2', '78', 74, '06-05-2020', '4.99', 'Standard Shipping', 'sheryl colburn', 'sheryl colburn', '59 Linden St Unit 1402', NULL, NULL, 'MA', 'taunton', '02780-3664', 'United States', NULL, NULL, '2.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 09:22:10', '2020-07-20 09:27:31'),
(1107, 'AK-BOX 3', '78', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Robin Eckholt', 'Robin Eckholt', '12601 SE River Rd Apt 117', NULL, NULL, 'OR', 'Milwaukie', '97222-9708', 'United States', NULL, NULL, '1.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 09:22:10', '2020-07-20 09:27:31'),
(1108, 'AK-BOX 4', '78', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Anita Neuburger', 'Anita Neuburger', '11037 Lynn Lake Circle', NULL, NULL, 'FL', 'Tampa', '33625-5642', 'United States', NULL, NULL, '4.19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 09:22:10', '2020-07-20 09:27:31'),
(1109, 'AK-BOX 5', '78', 74, '06-05-2020', '4.99', 'Standard Shipping', 'Becky S Love', 'Becky S Love', '13370 NW Westlawn Ter, Apt 6', NULL, NULL, 'OR', 'Portland', '97229-5547', 'United States', NULL, NULL, '3.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 09:22:10', '2020-07-20 09:27:31'),
(1110, 'AKSK-BOX 1', '82', 73, '06-05-2020', '4.99', 'Standard Shipping', 'Krista A. Blakely', 'Krista A. Blakely', '702 Meadow Lark Ln', NULL, NULL, 'UT', 'Smithfield', '84335-1003', 'United States', NULL, NULL, '0.96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-20 09:31:35', '2020-07-21 09:47:44'),
(1117, 'AKSK-BOX 3', '82', 73, '06-05-2020', '4.99', 'Standard Shipping', 'Robin Eckholt', 'Robin Eckholt', '12601 SE River Rd Apt 117', NULL, NULL, 'OR', 'Milwaukie', '97222-9708', 'United States', NULL, NULL, '1.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-20 09:31:38', '2020-07-21 11:10:54'),
(1119, 'AKSK-BOX 5', '82', 73, '06-05-2020', '4.99', 'Standard Shipping', 'Becky S Love', 'Becky S Love', '13370 NW Westlawn Ter, Apt 6', NULL, NULL, 'OR', 'Portland', '97229-5547', 'United States', NULL, NULL, '3.11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'initial', '2020-07-20 09:31:38', '2020-07-21 09:55:58'),
(1120, 'US-1093', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'Nansi Mc Daniel', 'Nansi Mc Daniel', '141 E 39th St', NULL, NULL, 'MI', 'Holland', '49423-7064', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1121, 'US-1092', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'CHRISTINA WILSON', 'CHRISTINA WILSON', '559 E 2600 N', NULL, NULL, 'UT', 'Provo', '84604-5902', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1122, 'US-1091', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'Lisa OQuinn', 'Lisa OQuinn', '628 Woodbine Dr', NULL, NULL, 'FL', 'Pensacola', '32503-3242', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1123, 'US-1090', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'celia pereira', 'celia pereira', '10201 Cristalino Rd SW', NULL, NULL, 'NM', 'Albuquerque', '87121-5424', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1124, 'US-1089', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'I V Jackson', 'I V Jackson', '804 Olde Pioneer Trail  #140', NULL, NULL, 'TN', 'Knoxville', '37923-6218', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1125, 'US-1088', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'Dorothy Mosby', 'Dorothy Mosby', '438 larkwood Dr', NULL, NULL, 'AL', 'Montgomery', '36109-4946', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1126, 'US-1087', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'Deryl Brunner', 'Deryl Brunner', '11049 south smith rd', NULL, NULL, 'MI', 'Perrinton', '48871-9717', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1127, 'US-1086', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'Jessica Lanham', 'Jessica Lanham', '2704 Timber Ct', NULL, NULL, 'VA', 'Richmond', '23228-1125', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1128, 'US-1085', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'Elizabeth Huntington', 'Elizabeth Huntington', '5092 Mandalay Springs Dr', '#101', NULL, 'NV', 'Las Vegas', '89120-5235', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1129, 'US-1084', '81', 75, '7/18/2020', '9.99', 'Standard Shipping', 'Paula Santiago', 'Paula Santiago', '724 Bowie Blvd', NULL, NULL, 'FL', 'ORANGE PARK', '32073-4210', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'confirm', '2020-07-20 10:41:52', '2020-07-20 10:42:02'),
(1130, 'US-A-A1', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'Nansi Mc Daniel', 'Nansi Mc Daniel', '141 E 39th St', NULL, NULL, 'MI', 'Holland', '49423-7064', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1131, 'US-A-A2', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'CHRISTINA WILSON', 'CHRISTINA WILSON', '559 E 2600 N', NULL, NULL, 'UT', 'Provo', '84604-5902', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1132, 'US-A-A3', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'Lisa OQuinn', 'Lisa OQuinn', '628 Woodbine Dr', NULL, NULL, 'FL', 'Pensacola', '32503-3242', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1133, 'US-A-A4', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'celia pereira', 'celia pereira', '10201 Cristalino Rd SW', NULL, NULL, 'NM', 'Albuquerque', '87121-5424', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1134, 'US-A-A5', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'I V Jackson', 'I V Jackson', '804 Olde Pioneer Trail  #140', NULL, NULL, 'TN', 'Knoxville', '37923-6218', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1135, 'US-A-A6', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'Dorothy Mosby', 'Dorothy Mosby', '438 larkwood Dr', NULL, NULL, 'AL', 'Montgomery', '36109-4946', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1136, 'US-A-A7', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'Deryl Brunner', 'Deryl Brunner', '11049 south smith rd', NULL, NULL, 'MI', 'Perrinton', '48871-9717', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1137, 'US-A-A8', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'Jessica Lanham', 'Jessica Lanham', '2704 Timber Ct', NULL, NULL, 'VA', 'Richmond', '23228-1125', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1138, 'US-A-A9', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'Elizabeth Huntington', 'Elizabeth Huntington', '5092 Mandalay Springs Dr', '#101', NULL, 'NV', 'Las Vegas', '89120-5235', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19'),
(1139, 'US-A-A10', '82', 74, '7/18/20', '9.99', 'Standard Shipping', 'Paula Santiago', 'Paula Santiago', '724 Bowie Blvd', NULL, NULL, 'FL', 'ORANGE PARK', '32073-4210', 'US', NULL, NULL, '3.15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dispatched', '2020-07-20 13:06:09', '2020-07-20 13:07:19');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `foldername` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languagename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `flag_image` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `foldername`, `languagename`, `description`, `flag_image`, `created_at`, `updated_at`) VALUES
(1, 'en', 'English', 'English', 'america.png', '2016-08-15 10:26:26', '2016-08-15 10:26:26'),
(22, 'bn', 'bengali', 'Bangla', '1475429828.png', '2016-10-02 11:37:08', '2016-10-02 11:37:08');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `replay_id` int(11) UNSIGNED NOT NULL,
  `subject` text CHARACTER SET utf8,
  `description` text CHARACTER SET utf8,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `receiver_id`, `sender_id`, `replay_id`, `subject`, `description`, `created_at`, `updated_at`, `status`) VALUES
(1, 68, 61, 0, 'dsds', 'ddsd', '2016-12-24 10:03:59', '2016-12-24 10:03:59', 0),
(2, 61, 68, 0, 'trtrt', 'trtrtrt', '2016-12-28 08:57:29', '2016-12-28 08:57:29', 1),
(3, 68, 68, 0, 'trtrt', 'trtrtrt', '2016-12-28 08:57:29', '2016-12-28 08:57:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_01_15_105324_create_roles_table', 1),
('2015_01_15_114412_create_role_user_table', 1),
('2015_01_26_115212_create_permissions_table', 1),
('2015_01_26_115523_create_permission_role_table', 1),
('2015_02_09_132439_create_permission_user_table', 1),
('2016_10_12_45785540_create_country_table', 1),
('2020_06_12_111540_create_orders_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `OrderDetails`
--

CREATE TABLE `OrderDetails` (
  `orderid` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `orgid` int(11) NOT NULL,
  `sailDate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `OrderDetails`
--

INSERT INTO `OrderDetails` (`orderid`, `user_id`, `orgid`, `sailDate`, `created_at`, `updated_at`) VALUES
(50, 75, 3, '2020-07-11', '2020-07-11 12:22:42', '2020-07-11 12:22:42'),
(51, 75, 0, '2020-07-11', '2020-07-11 15:33:56', '2020-07-11 15:33:56'),
(52, 75, 0, '2020-07-14', '2020-07-14 15:45:24', '2020-07-14 15:45:24'),
(63, 73, 6, '2020-07-15', '2020-07-15 06:47:20', '2020-07-15 06:47:20'),
(67, 75, 8, '2020-07-15', '2020-07-15 13:07:30', '2020-07-15 13:07:30'),
(68, 75, 8, '2020-07-16', '2020-07-16 06:48:32', '2020-07-16 06:48:32'),
(69, 75, 8, '2020-07-16', '2020-07-16 08:22:22', '2020-07-16 08:22:22'),
(70, 75, 8, '2020-07-16', '2020-07-16 08:55:25', '2020-07-16 08:55:25'),
(71, 75, 8, '2020-07-17', '2020-07-16 21:27:45', '2020-07-16 21:27:45'),
(72, 75, 8, '2020-07-17', '2020-07-16 21:28:55', '2020-07-16 21:28:55'),
(73, 75, 8, '2020-07-17', '2020-07-16 21:35:55', '2020-07-16 21:35:55'),
(74, 76, 5, '2020-07-20', '2020-07-20 06:48:08', '2020-07-20 06:48:08'),
(75, 75, 8, '2020-07-20', '2020-07-20 08:23:18', '2020-07-20 08:23:18'),
(76, 73, 5, '2020-07-20', '2020-07-20 08:37:21', '2020-07-20 08:37:21'),
(77, 75, 8, '2020-07-20', '2020-07-20 08:48:31', '2020-07-20 08:48:31'),
(78, 73, 6, '2020-07-20', '2020-07-20 09:22:10', '2020-07-20 09:22:10'),
(79, 73, 7, '2020-07-20', '2020-07-20 09:31:35', '2020-07-20 09:31:35'),
(80, 73, 8, '2020-07-20', '2020-07-20 09:31:38', '2020-07-20 09:31:38'),
(81, 75, 8, '2020-07-20', '2020-07-20 10:41:52', '2020-07-20 10:41:52'),
(82, 75, 8, '2020-07-20', '2020-07-20 13:06:09', '2020-07-20 13:06:09'),
(84, 75, 8, '2020-07-21', '2020-07-21 12:34:47', '2020-07-21 12:34:47'),
(91, 73, 8, '2020-07-21', '2020-07-21 13:25:03', '2020-07-21 13:25:03'),
(92, 75, 8, '2020-07-21', '2020-07-21 13:40:35', '2020-07-21 13:40:35'),
(93, 75, 8, '2020-07-22', '2020-07-21 20:38:28', '2020-07-21 20:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `OrderStatus`
--

CREATE TABLE `OrderStatus` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderid` int(11) NOT NULL,
  `orderstatus` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `statusdate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bookingstatus` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `OrderStatus`
--

INSERT INTO `OrderStatus` (`id`, `orderid`, `orderstatus`, `statusdate`, `bookingstatus`, `created_at`, `updated_at`) VALUES
(7, 50, 'initial', '2020-07-11', 'initial', '2020-07-11 12:22:42', '2020-07-11 12:22:42'),
(8, 52, 'initial', '2020-07-14', 'transfer', '2020-07-14 15:45:24', '2020-07-16 08:19:35'),
(9, 53, 'initial', '2020-07-15', 'initial', '2020-07-15 05:37:07', '2020-07-15 05:37:07'),
(10, 54, 'initial', '2020-07-15', 'initial', '2020-07-15 05:41:18', '2020-07-15 05:41:18'),
(11, 55, 'initial', '2020-07-15', 'initial', '2020-07-15 05:55:36', '2020-07-15 05:55:36'),
(12, 56, 'initial', '2020-07-15', 'initial', '2020-07-15 06:02:46', '2020-07-15 06:02:46'),
(13, 57, 'initial', '2020-07-15', 'initial', '2020-07-15 06:13:53', '2020-07-15 06:13:53'),
(14, 58, 'initial', '2020-07-15', 'initial', '2020-07-15 06:20:10', '2020-07-15 06:20:10'),
(15, 59, 'initial', '2020-07-15', 'initial', '2020-07-15 06:22:05', '2020-07-15 06:22:05'),
(16, 60, 'initial', '2020-07-15', 'initial', '2020-07-15 06:23:33', '2020-07-15 06:23:33'),
(17, 61, 'initial', '2020-07-15', 'initial', '2020-07-15 06:27:28', '2020-07-15 06:27:28'),
(18, 62, 'initial', '2020-07-15', 'transfer', '2020-07-15 06:39:45', '2020-07-15 09:36:57'),
(19, 63, 'dispatched', '2020-07-15', 'transfer', '2020-07-15 06:47:20', '2020-07-15 12:30:25'),
(20, 67, 'dispatched', '2020-07-15', 'transfer', '2020-07-15 13:07:30', '2020-07-15 19:54:42'),
(21, 68, 'booked', '2020-07-16', 'transfer', '2020-07-16 06:48:32', '2020-07-16 06:53:13'),
(22, 69, 'confirm', '2020-07-16', 'AtSortFacility-USA', '2020-07-16 08:22:22', '2020-07-16 11:45:01'),
(23, 70, 'dispatched', '2020-07-16', 'ProcessedAtSort-USA', '2020-07-16 08:55:25', '2020-07-16 11:42:13'),
(24, 71, 'initial', '2020-07-17', 'initial', '2020-07-16 21:27:45', '2020-07-16 21:27:45'),
(25, 72, 'confirm', '2020-07-17', 'transfer', '2020-07-16 21:28:55', '2020-07-16 21:31:28'),
(26, 73, 'confirm', '2020-07-17', 'ProcessedAtSort-USA', '2020-07-16 21:35:55', '2020-07-16 21:46:48'),
(27, 74, 'confirm', '2020-07-20', 'transfer', '2020-07-20 06:48:08', '2020-07-20 06:52:38'),
(28, 75, 'confirm', '2020-07-20', 'transfer', '2020-07-20 08:23:18', '2020-07-20 08:23:41'),
(29, 76, 'dispatched', '2020-07-20', 'transfer', '2020-07-20 08:37:21', '2020-07-20 08:56:24'),
(30, 77, 'booked', '2020-07-20', 'ProcessedAtSort-USA', '2020-07-20 08:48:31', '2020-07-20 09:15:07'),
(31, 78, 'dispatched', '2020-07-20', 'ProcessedAtSort-USA', '2020-07-20 09:22:10', '2020-07-20 09:27:31'),
(32, 79, 'booked', '2020-07-20', 'transfer', '2020-07-20 09:31:35', '2020-07-20 12:56:03'),
(33, 80, 'initial', '2020-07-20', 'initial', '2020-07-20 09:31:38', '2020-07-20 09:31:38'),
(34, 81, 'confirm', '2020-07-20', 'transfer', '2020-07-20 10:41:52', '2020-07-20 10:42:14'),
(35, 82, 'dispatched', '2020-07-20', 'ProcessedAtSort-USA', '2020-07-20 13:06:09', '2020-07-20 13:07:29'),
(36, 84, 'confirm', '2020-07-21', 'transfer', '2020-07-21 12:34:47', '2020-07-22 23:56:29'),
(42, 91, 'confirm', '2020-07-21', 'initial', NULL, '2020-07-23 06:23:37'),
(43, 92, 'dispatched', '2020-07-21', 'ProcessedAtSort-USA', NULL, '2020-07-21 13:50:19'),
(44, 93, 'initial', '2020-07-22', 'initial', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('aakash@puretechnology.in', '$2y$10$6qIhNpuVXE/3OWSZkQAuOegX53jF96u92x1mAHtw5jjDDdJF7OZ9m', '2020-07-23 06:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `removable`, `created_at`, `updated_at`) VALUES
(1, 'users.manage', 'Manage Users', 'Manage users and their sessions.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(3, 'roles.manage', 'Manage Roles', 'Manage system roles.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(4, 'permissions.manage', 'Manage Permissions', 'Manage role permissions.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(5, 'company.manage', 'Manage Company', 'Manage company', 0, NULL, NULL),
(8, 'settings.general', 'Settings', 'Update system settings.', 0, '2016-06-17 21:58:04', '2016-09-24 09:30:15'),
(9, 'OrderBasic', 'OrderBasic', 'Order Basic', 1, '2020-06-19 20:37:09', '2020-06-19 20:37:09'),
(10, 'OrderPlus', 'OrderPlus', 'Order Plus', 1, '2020-06-19 20:45:07', '2020-06-19 20:45:07'),
(11, 'OrderAdvance', 'OrderAdvance', 'Order Advance', 1, '2020-06-19 20:45:19', '2020-06-19 20:45:19'),
(12, 'Booking', 'Booking', 'Booking', 1, '2020-06-19 20:46:06', '2020-06-19 20:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(3, 1),
(4, 1),
(8, 1),
(9, 3),
(10, 3),
(9, 4),
(10, 4),
(11, 4),
(10, 5),
(11, 5),
(12, 5),
(1, 6),
(5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `removable`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'System administrator.', 0, '2016-06-17 21:58:04', '2016-06-17 21:58:04'),
(3, 'Customer', 'Customer', 'Customer', 1, '2020-06-19 18:26:23', '2020-06-19 18:26:23'),
(4, 'I-Customer', 'I-Customer', 'I-Customer', 1, '2020-06-19 18:26:38', '2020-06-19 18:27:03'),
(5, 'Agent', 'Agent', 'Agent', 1, '2020-06-19 18:27:22', '2020-06-19 18:27:22'),
(6, 'Webmaster', 'Webmaster', 'Webmaster', 1, '2020-06-19 19:34:22', '2020-06-19 19:34:22');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(61, 1),
(75, 3),
(73, 4),
(76, 4),
(74, 5),
(72, 6);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `app_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_me` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forget_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notify_signup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_capcha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `app_name`, `app_title`, `app_email`, `phone`, `mobile`, `currency`, `remember_me`, `forget_password`, `notify_signup`, `re_capcha`, `logo`, `address`, `created_at`, `updated_at`) VALUES
(10, 'Samsar Shippings', 'Samsar Shippings', 'samsar@gmail.com', '', '9970111283', 'Rs', 'ON', 'ON', 'OFF', 'OFF', 'logo.png', 'Pune', '2016-09-01 19:38:54', '2020-06-19 18:32:04');

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE `tbl_company` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_profile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`id`, `company_name`, `company_profile`, `address`, `country`, `status`, `created_at`, `updated_at`) VALUES
(3, 'abc pvt ltd', '1592214397.png', 'pune', 'Bahamas', 'Active', '2020-04-28 15:00:29', '2020-06-15 04:46:37'),
(5, 'Ledcor Group', '1592214377.png', '11834 149 st nw, Edmonton , Alberta', 'Canada', 'Active', '2020-05-06 17:38:29', '2020-06-15 04:46:17'),
(6, 'SILVER GEMS JEWELLERY', '1592509801.jpg', '83 RAJEEV PURI GOLIMAR GARDEN\r\nRAMGARH MODE', 'India', '', '2020-06-18 14:50:01', '2020-06-18 14:50:01'),
(7, 'Samsar Express', '1592613185.jpeg', 'Jaipur', 'India', '', '2020-06-19 19:33:05', '2020-06-19 19:33:05'),
(8, 'Maddona Jewells', '1592683740.jpg', 'Johari Bajar', 'India', '', '2020-06-20 15:09:00', '2020-06-20 15:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `trackings`
--

CREATE TABLE `trackings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `print_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adj_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quoted_amt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recipient` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_delivered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carrier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insured_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insurance_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_request_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_requested` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trackings`
--

INSERT INTO `trackings` (`id`, `item_id`, `order_id`, `user_id`, `print_date`, `amount`, `adj_amount`, `quoted_amt`, `recipient`, `status`, `tracking_id`, `date_delivered`, `carrier`, `class_service`, `insured_value`, `insurance_id`, `cost_code`, `weight`, `ship_date`, `refund_type`, `user`, `refund_request_date`, `refund_status`, `refund_requested`, `reference1`, `reference2`, `created_at`, `updated_at`) VALUES
(1, 'RZ35-Box 2', '35', 73, '06-05-2020', '$2.74 ', NULL, '$2.74 ', 'ALICIA FONTAINE, ALICIA FONTAINE, 320 TENNESSEE AVE, ELIZABETHVILLE, PA 17023-9640', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-02 06:49:32', '2020-07-02 06:49:32'),
(2, 'RZ-688', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Solara Pastore, SOLARA PASTORE, 844 TOWNSEND AVE, WAPELLO, IA 52653-1238', 'Printed', '9400111699000547135876', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(3, 'KI-25', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'Gail Messerschmidt, GAIL MESSERSCHMIDT, 1201 BELSLY BLVD APT 102, MOORHEAD, MN 56560-5189', 'Printed', '9400111699000547135753', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(4, 'KI-26', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'Tricia Williams, TRICIA WILLIAMS, 12208 SQUIRE DR, BALCH SPRINGS, TX 75180-3336', 'Printed', '9400111699000547135791', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(5, 'KI-27', '36', 75, '7/1/2020', '$2.74', NULL, '$2.74', 'Karen L Humphreys, KAREN L HUMPHREYS, 240 ROCK RIDGE RD, BOONTON, NJ 07005-9212', 'Printed', '9400111699000547135968', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(6, 'KI-28', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'Beverly J. Courtney, BEVERLY J. COURTNEY, 115 CLAUDE ST, DILL CITY, OK 73641-9531', 'Printed', '9400111699000547135944', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(7, 'KI-29', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Urmi Boyd, URMI BOYD, 25235 TYRO AVE, VENETA, OR 97487-9550', 'Printed', '9400111699000547135975', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(8, 'KI-30', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Olena Tereshchenko, OLENA TERESHCHENKO, 1967 EMERALD DR, GREEN BAY, WI 54311-5011', 'Printed', '9400111699000547135616', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(9, 'KI-31', '36', 75, '7/1/2020', '$2.78', NULL, '$2.78', 'Toni Tapp, TONI TAPP, 304 W PENNSYLVANIA ST, SHELBYVILLE, IN 46176-1134', 'Printed', '9400111699000547135609', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(10, 'KI-32', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'kari Hackleman, KARI HACKLEMAN, 1046 AL TAHOE BLVD # 18323, SOUTH LAKE TAHOE, CA 96150-4603', 'Printed', '9400111699000547135685', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(11, 'KI-33', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'April Koonce, APRIL KOONCE, 1751 CITRACADO PKWY SPC 239, ESCONDIDO, CA 92029-4139', 'Printed', '9400111699000547135357', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(12, 'KI-34', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Debra Brand, DEBRA BRAND, 6001 NW 61ST AVE APT 201 BLDG 15, TAMARAC, FL 33319-6222', 'Printed', '9400111699000547135012', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(13, 'KI-35', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'kenda kitter, KENDA KITTER, 2487 BENSON LN, EUGENE, OR 97408-4771', 'Printed', '9400111699000547135005', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(14, 'KI-36', '36', 75, '7/1/2020', '$2.74', NULL, '$2.74', 'Karen Hennessy, KAREN HENNESSY, 2206 LAKE DR, PASADENA, MD 21122-3626', 'Printed', '9400111699000547135036', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(15, 'KI-37', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Mary Lynn Turner, MARY LYNN TURNER, 6323 SPRINGFIELD ST, SAN DIEGO, CA 92114-2025', 'Printed', '9400111699000547135463', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(16, 'KI-38', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'Susan Swain, SUSAN SWAIN, 3401 W COUNTY ROAD 183, MIDLAND, TX 79706-7803', 'Printed', '9400111699000547135449', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(17, 'KI-39', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'Joleen A Ryan, JOLEEN A RYAN, 2205 CEDAR ST, PUEBLO, CO 81004-3901', 'Printed', '9400111699000547135524', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(18, 'KI-40', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'STACI NONNENMACHER, STACI NONNENMACHER, 3613 ROYAL FERN CIR, DELAND, FL 32724-1233', 'Printed', '9400111699000547119944', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(19, 'KI-41', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'LINDA D HAMILTON, LINDA D HAMILTON, 106 AMBER OAKS DR, SHERWOOD, AR 72120-2231', 'Printed', '9400111699000547119616', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(20, 'KI-42', '36', 75, '7/1/2020', '$2.74', NULL, '$2.74', 'SALLY A FLYNN, SALLY A FLYNN, 4258 GEM BRIDGE RD, NEEDMORE, PA 17238-9271', 'Printed', '9400111699000547119692', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(21, 'KI-43', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'LORA COMO, LORA COMO, 17 ASHDOWN RD APT B, BALLSTON LAKE, NY 12019-2342', 'Printed', '9400111699000547119166', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(22, 'KI-44', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'JONNY HOOKER, JONNY HOOKER, 10307 HONDO HILL RD, HOUSTON, TX 77064-7040', 'Printed', '9400111699000547119142', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(23, 'KI-45', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'ANGELICA RODRIGUEZ, ANGELICA RODRIGUEZ, 570 NE 21ST TER, HOMESTEAD, FL 33033-6042', 'Printed', '9400111699000547119173', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(24, 'KI-46', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'TIM JOHNSON, TIM JOHNSON, 425 CASCADE WAY, LYNDEN, WA 98264-1037', 'Printed', '9400111699000547119395', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(25, 'KI-47', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'BONNIE ZECK, BONNIE ZECK, 5340 GIRARD AVE N, BROOKLYN CTR, MN 55430-3136', 'Printed', '9400111699000547119029', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(26, 'KI-48', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'KYLEN SIMPSON, KYLEN SIMPSON, 2326 BIG PINE RD, ESCONDIDO, CA 92027-4954', 'Printed', '9400111699000547119036', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(27, 'KI-49', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'SHRILEY MILLER, SHRILEY MILLER, 2122 GREEN GATE CIR E, PALMVIEW, TX 78572-5283', 'Printed', '9400111699000547119500', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(28, 'KI-50', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'LISA  MAULDIN, LISA MAULDIN, 1529 PINECREST ST, COVINGTON, TN 38019-3215', 'Printed', '9400111699000547113218', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(29, 'KI-51', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'ANITA GROVE, ANITA GROVE, 305 W 1ST ST, COFFEYVILLE, KS 67337-5509', 'Printed', '9400111699000547113850', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(30, 'KI-52', '36', 75, '7/1/2020', '$2.74', NULL, '$2.74', 'DIANE W. HALATA, DIANE W. HALATA, 516 NEWPORT CIR, LANGHORNE, PA 19053-2489', 'Printed', '9400111699000547113874', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(31, 'KI-53', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'SUSAN PETERSON, SUSAN PETERSON, 1125 N MAIN ST APT 3H, LAYTON, UT 84041-5911', 'Printed', '9400111699000547113744', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(32, 'KI-54', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'LYNN WILSON-BRUCE, LYNN WILSON-BRUCE, 722 N 6TH ST, LAWRENCE, KS 66044-5311', 'Printed', '9400111699000547113959', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(33, 'KI-55', '36', 75, '7/1/2020', '$2.74', NULL, '$2.74', 'TINE BALBO - HICKERSON, TINE BALBO - HICKERSON, 322 E LIBERTY ST, CHARLES TOWN, WV 25414-1826', 'Printed', '9400111699000547113980', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(34, 'KI-56', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'KATHLEEN MORGAN, KATHLEEN MORGAN, 9243 POPPY CT, MISSOULA, MT 59808-9626', 'Printed', '9400111699000547113102', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(35, 'KI-57', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'CAROLANN BOYLE, CAROLANN BOYLE, 42 TERESA DR, HUDSON, MA 01749-2027', 'Printed', '9400111699000547113300', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(36, 'KI-58', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'LAURIE HUMPHREYS, LAURIE HUMPHREYS, N9503 COUNTRY ROAD E, MERRILLAN, WI 54754', 'Printed', '9400111699000547113096', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(37, 'KI-59', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'nicole weil, NICOLE WEIL, 3690 PRONGHORN MEADOWS CIR, COLORADO SPRINGS, CO 80922-4622', 'Printed', '9400111699000547113478', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(38, 'KI-60', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'MARIA A. CORRAL, MARIA A. CORRAL, 753 FIG AVE, CHULA VISTA, CA 91910-5409', 'Printed', '9400111699000547113577', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(39, 'KI-61', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Juliana Hoewing, JULIANA HOEWING, 2663 HIDDEN ACRES RD, SANTA ROSA, CA 95404-9513', 'Printed', '9400111699000547118732', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(40, 'KI-62', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Solara Pastore, SOLARA PASTORE, 844 TOWNSEND AVE, WAPELLO, IA 52653-1238', 'Printed', '9400111699000547118978', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(41, 'KI-63', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'Tina Bramante, TINA BRAMANTE, 29 LAWNDALE RD, STONEHAM, MA 02180-1014', 'Printed', '9400111699000547118138', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(42, 'KI-64', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'April Koonce, APRIL KOONCE, 1751 CITRACADO PKWY SPC 239, ESCONDIDO, CA 92029-4139', 'Printed', '9400111699000547118060', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(43, 'KI-65', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'CD-INK, CD-INK, 221 ESEVERRI LN, LA HABRA, CA 90631-8333', 'Printed', '9400111699000547118527', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(44, 'KI-66', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'Preslye Elveus, PRESLYE ELVEUS, 72 PERRY ST, STOUGHTON, MA 02072-4312', 'Printed', '9400111699000547117216', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(45, 'KI-67', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'Crystal Brown, CRYSTAL BROWN, 705 SHERIDAN ST, OAK GROVE, MO 64075-9459', 'Printed', '9400111699000547117803', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(46, 'KI-68', '36', 75, '7/1/2020', '$2.78', NULL, '$2.78', 'Brian Brimm, BRIAN BRIMM, 20C FERNDALE APARTMENTS RD, PINEVILLE, KY 40977-8578', 'Printed', '9400111699000547117889', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(47, 'KI-69', '36', 75, '7/1/2020', '$2.74', NULL, '$2.74', 'Teresa Grinnell, TERESA GRINNELL, 74 GREENHOUSE RD UNIT A, BRIDGEPORT, CT 06606-2146', 'Printed', '9400111699000547117759', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(48, 'KI-70', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Josephine Lee, JOSEPHINE LEE, 706 MCHUGH RD APT 7, HOLMEN, WI 54636-5904', 'Printed', '9400111699000547117797', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(49, 'KI-71', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Mary Lynn Turner, MARY LYNN TURNER, 6323 SPRINGFIELD ST, SAN DIEGO, CA 92114-2025', 'Printed', '9400111699000547117773', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(50, 'KI-72', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'donna Scott, DONNA SCOTT, PO BOX 113, GOLDEN VALLEY, ND 58541-0113', 'Printed', '9400111699000547117995', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(51, 'KI-73', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'June Wigglesworth, JUNE WIGGLESWORTH, 2132 DOWNING DR, COLORADO SPRINGS, CO 80909-2146', 'Printed', '9400111699000547117650', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(52, 'KI-74', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'Tracy Garretson, TRACY GARRETSON, 9315 NORTHGATE BLVD APT 116, AUSTIN, TX 78758-6171', 'Printed', '9400111699000547117698', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(53, 'US-470', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'Cathy Palus, CATHY PALUS, 7801 N CHELSEA AVE, KANSAS CITY, MO 64119-8611', 'Printed', '9400111699000547144090', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(54, 'US-471', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'CINDY PRITZKAU, CINDY PRITZKAU, 3898 TWILIGHT DR, RAPID CITY, SD 57703-4754', 'Printed', '9400111699000547144458', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(55, 'US-472', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'JULIE WENGLINSKI, JULIE WENGLINSKI, 4809 PARK AVE, RICHMOND, VA 23226-1225', 'Printed', '9400111699000547144441', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(56, 'US-473', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'Lisa DeSilva, LISA DESILVA, 188 BERKLEY ST, TAUNTON, MA 02780-4984', 'Printed', '9400111699000547144472', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(57, 'US-474', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Tami Forbus, TAMI FORBUS, 2925 TUSCARORA TRL, MIDDLEBURG, FL 32068-4263', 'Printed', '9400111699000547144564', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(58, 'US-475', '36', 75, '7/1/2020', '$3.31', NULL, '$3.31', 'r b, R B, 255 W TOMPKINS ST APT 1008, GALESBURG, IL 61401-4404', 'Printed', '9400111699000547144571', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(59, 'US-476', '36', 75, '7/1/2020', '$3.67', NULL, '$3.67', 'Margaret Fujii, MARGARET FUJII, 3400 SAN MARINO ST, LOS ANGELES, CA 90006-1106', 'Printed', '9400111699000547145219', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(60, 'US-477', '36', 75, '7/1/2020', '$2.93', NULL, '$2.93', 'LESLIE ZACKER, LESLIE ZACKER, 2509 SADDLEBACK DR, EDMOND, OK 73034-5906', 'Printed', '9400111699000547145264', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(61, 'US-478', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'Jane Pabst, JANE PABST, 215 JACKSON KELLER RD APT 111, SAN ANTONIO, TX 78216-7619', 'Printed', '9400111699000547145295', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(62, 'US-479', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Wendy York, WENDY YORK, 12155 OCOTILLO RD UNIT 2, DESERT HOT SPRINGS, CA 92240-3941', 'Printed', '9400111699000547145288', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(63, 'US-480', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Dana Hein, DANA HEIN, 17821 LYNN WAY, PINE GROVE, CA 95665-9452', 'Printed', '9400111699000547145851', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(64, 'US-481', '36', 75, '7/1/2020', '$2.74', NULL, '$2.74', 'Kristi Pappas, KRISTI PAPPAS, 15405 MILTON HALL PL, MANASSAS, VA 20112-5487', 'Printed', '9400111699000547145844', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(65, 'US-482', '36', 75, '7/1/2020', '$2.78', NULL, '$2.78', 'Joy Orr, JOY ORR, 19834 IRONGATE CT, NORTHVILLE, MI 48167-2504', 'Printed', '9400111699000547145875', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(66, 'US-483', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'Violet Benjamin, VIOLET BENJAMIN, 1168 BELL HILL RD, PAINTED POST, NY 14870-9761', 'Printed', '9400111699000547145769', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(67, 'US-484', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Mary McCormack, MARY MCCORMACK, 8122 E MADERA DR, SIERRA VISTA, AZ 85650-9012', 'Printed', '9400111699000547145745', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(68, 'US-485', '36', 75, '7/1/2020', '$2.76', NULL, '$2.76', 'Loretta Ciccia, LORETTA CICCIA, 194 BREMEN ST, EAST BOSTON, MA 02128-1738', 'Printed', '9400111699000547145738', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(69, 'US-486', '36', 75, '7/1/2020', '$4.18', NULL, '$4.18', 'sarah pool, SARAH POOL, 1363 BLYTHE RD, BOONEVILLE, AR 72927-5565', 'Printed', '9400111699000547190363', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 10oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(70, 'US-487', '36', 75, '7/1/2020', '$3.31', NULL, '$3.31', 'robert wojcik, ROBERT WOJCIK, 1729 SW 14TH CT, FORT LAUDERDALE, FL 33312-4109', 'Printed', '9400111699000547190394', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(71, 'US-488', '36', 75, '7/1/2020', '$3.39', NULL, '$3.39', 'Steven Hicks, STEVEN HICKS, 2321 SW DUNCAN DR, TOPEKA, KS 66614-1118', 'Printed', '9400111699000547190332', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(72, 'US-489', '36', 75, '7/1/2020', '$3.05', NULL, '$3.05', 'DEE ROBSON, DEE ROBSON, 1366 SUNSET DR, ABILENE, TX 79605-4049', 'Printed', '9400111699000547190011', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(73, 'US-490', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Kim Stoudinger, KIM STOUDINGER, 1938 ADELAIDA RD # 21, PASO ROBLES, CA 93446-9776', 'Printed', '9400111699000547190400', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(74, 'US-491', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Laura Widzins, LAURA WIDZINS, 1857 CONTINENTAL AVE, LAS VEGAS, NV 89156-6922', 'Printed', '9400111699000547190448', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(75, 'US-492', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Sharon Stover, SHARON STOVER, 123 ALISA DR, SEBASTIAN, FL 32958-5804', 'Printed', '9400111699000547190554', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(76, 'US-493', '36', 75, '7/1/2020', '$3.31', NULL, '$3.31', 'Christine Stern, CHRISTINE STERN, 1138 EGRET LAKE WAY, MELBOURNE, FL 32940-6862', 'Printed', '9400111699000547190547', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(77, 'US-495', '36', 75, '7/1/2020', '$3.23', NULL, '$3.23', 'Sunny McAlister, SUNNY MCALISTER, 3386 FOREST LN, SALUDA, VA 23149-2922', 'Printed', '9400111699000547198819', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(78, 'US-496', '36', 75, '7/1/2020', '$2.78', NULL, '$2.78', 'Donald Dugger, DONALD DUGGER, 229 HALEY WALK, BRISTOL, TN 37620-5874', 'Printed', '9400111699000547198888', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(79, 'US-497', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Luisa Morca-Bush, LUISA MORCA-BUSH, 1676 GALBRAITH LN, BELLINGHAM, WA 98229-8921', 'Printed', '9400111699000547198987', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(80, 'US-498', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Pascale Headley, PASCALE HEADLEY, 1376 SEA PINES DR, BANNING, CA 92220-7104', 'Printed', '9400111699000547198147', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(81, 'US-499', '36', 75, '7/1/2020', '$5.70', NULL, '$5.70', 'Anthony Costanzo, Anthony Costanzo, 8504 NE 83rd Ct, Vancouver, WA 98662-2700', 'Printed', '9400111699000547198178', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 13oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(82, 'US-500', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'Jacqulyn, JACQULYN, 11012 S FAIRFIELD AVE, CHICAGO, IL 60655-1814', 'Printed', '9400111699000547198345', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(83, 'US-501', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'CAROLINA SANCHEZ, CAROLINA SANCHEZ, 8601 NW 27TH ST, SUITS 714-69448, DORAL, DORAL, FL 33122-1918', 'Printed', '9400111699000547198338', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(84, 'US-502', '36', 75, '7/1/2020', '$2.84', NULL, '$2.84', 'MARIA CARNEIRO, MARIA CARNEIRO, PO BOX 645, NAPLES, FL 34106-0645', 'Printed', '9400111699000547198093', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(85, 'Us-494', '36', 75, '7/1/2020', '$3.18', NULL, '$3.18', 'Kathleen King, KATHLEEN KING, 6900 ALMOND AVE SPC 112, ORANGEVALE, CA 95662-3341', 'Printed', '9400111699000547198260', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/7/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-02 09:10:46', '2020-07-02 09:10:46'),
(86, 'US-469', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'Brenda Jerles, BRENDA JERLES, 259 OAK PLAINS RD, CLARKSVILLE, TN 37043-6908', 'Printed', '9400111699000554731030', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(87, 'US-468', '37', 76, '6/26/2020', '$3.18', NULL, '$3.18', 'Elizabeth Stewart, ELIZABETH STEWART, 64 TIERRA VISTA CT, BOUNTIFUL, UT 84010-7096', 'Printed', '9400111699000554731047', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(88, 'US-467', '37', 76, '6/26/2020', '$2.78', NULL, '$2.78', 'Angela Boyce, ANGELA BOYCE, 2039 STATE ROUTE 183, ATWATER, OH 44201-9535', 'Printed', '9400111699000554731016', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(89, 'US-466', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'Kevin Bailes, KEVIN BAILES, 525 N ONEIDA ST APT 606, APPLETON, WI 54911-4788', 'Printed', '9400111699000554731382', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(90, 'US-464', '37', 76, '6/26/2020', '$3.31', NULL, '$3.31', 'Kim Green, KIM GREEN, 21 SHARROTT ST NW, HARTSELLE, AL 35640-1524', 'Printed', '9400111699000554731108', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(91, 'US-463', '37', 76, '6/26/2020', '$2.78', NULL, '$2.78', 'Donna Holmes, SASHA HAWKE, 224 FALLS ST APT J, MORGANTON, NC 28655-4361', 'Printed', '9400111699000554731634', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(92, 'US-462', '37', 76, '6/26/2020', '$3.18', NULL, '$3.18', 'Kathleen E. Lindquester, KATHLEEN LINDQUESTER, 727 SE PARROTT ST, ROSEBURG, OR 97470-3460', 'Printed', '9400111699000554731603', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(93, 'US-461', '37', 76, '6/26/2020', '$2.78', NULL, '$2.78', 'martha Linkenhoker, MARTHA LINKENHOKER, 208 VILLAGE CT, SCOTT DEPOT, WV 25560-9350', 'Printed', '9400111699000554731627', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(94, 'US-460', '37', 76, '6/26/2020', '$2.93', NULL, '$2.93', 'Kathy M. Cott, KATHY COTT, 1500 REGENT ST, MESQUITE, TX 75149-5978', 'Printed', '9400111699000554731658', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(95, 'US-459', '37', 76, '6/26/2020', '$2.93', NULL, '$2.93', 'shana miller, SHANA MILLER, 309 VELVET ST, LEAGUE CITY, TX 77573-2232', 'Printed', '9400111699000554731948', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(96, 'US-458', '37', 76, '6/26/2020', '$2.76', NULL, '$2.76', 'Joshua Quiles, JOSHUA QUILES, 56 ELIOT AVE, ALBANY, NY 12203-2612', 'Printed', '9400111699000554731917', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(97, 'US-457', '37', 76, '6/26/2020', '$3.31', NULL, '$3.31', 'Audrey Burgs, AUDREY BURGS, 810 25TH ST, DES MOINES, IA 50312-4815', 'Printed', '9400111699000554731771', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(98, 'US-456', '37', 76, '6/26/2020', '$5.40', NULL, '$5.40', 'Dolores King, DOLORES KING, 8536 SHAGROCK LN, DALLAS, TX 75238-4920', 'Printed', '9400111699000554731795', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 15oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(99, 'US-455', '37', 76, '6/26/2020', '$4.00', NULL, '$4.00', 'stacy van horn, STACY VAN HORN, 215 LANE 510 LAKE JAMES, FREMONT, IN 46737-9635', 'Printed', '9400111699000554731702', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 10oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(100, 'US-454', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'jacqueline hanson, JACQUELINE HANSON, 18290 CREEKSIDE CT, PINE CITY, MN 55063-5583', 'Printed', '9400111699000554731870', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(101, 'US-453', '37', 76, '6/26/2020', '$2.74', NULL, '$2.74', 'Cecilia Versace, CECILIA VERSACE, 4515 WILLARD AVE APT 2418, CHEVY CHASE, MD 20815-3651', 'Printed', '9400111699000554731887', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(102, 'US-452', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'Pamala Hutton, PAMALA HUTTON, 5191 HOUSERS MILL RD, BYRON, GA 31008-5555', 'Printed', '9400111699000554731849', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(103, 'US-451', '37', 76, '6/26/2020', '$2.74', NULL, '$2.74', 'ROGAN ODONNELL, ROGAN ODONNELL, 318 SWEETMANS LN, PERRINEVILLE, NJ 08535-8116', 'Printed', '9400111699000554731238', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(104, 'US-450', '37', 76, '6/26/2020', '$2.93', NULL, '$2.93', 'Linda Darling, LJ SHAW-DARLING, 809 W PLATO RD, DUNCAN, OK 73533-3307', 'Printed', '9400111699000554731269', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(105, 'US-449', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'Carolina Vaughan, CAROLINA VAUGHAN, 15770 SW 106TH TER APT 206, MIAMI, FL 33196-4230', 'Printed', '9400111699000554736554', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(106, 'US-448', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'Justine Crichton, JUSTINE CRICHTON, 1814 PICKETT PARK HWY, JAMESTOWN, TN 38556-2909', 'Printed', '9400111699000554736431', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(107, 'US-447', '37', 76, '6/26/2020', '$3.67', NULL, '$3.67', 'Linda Albracht, LINDA ALBRACHT, 5016 NANTUCKET ST, ROSEVILLE, CA 95747-4232', 'Printed', '9400111699000554736400', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(108, 'US-446', '37', 76, '6/26/2020', '$5.54', NULL, '$5.54', 'Cindy Pritzkau, CYNTHIA PRITZKAU, 3898 TWILIGHT DR, RAPID CITY, SD 57703-4754', 'Printed', '9400111699000554736356', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 14oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(109, 'US-445', '37', 76, '6/26/2020', '$5.70', NULL, '$5.70', 'LISA OTERO, LISAABCDEFG OTERO, 9349 JORNADA LN APT 5, ATASCADERO, CA 93422-5827', 'Printed', '9400111699000554736639', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 15oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(110, 'US-444', '37', 76, '6/26/2020', '$3.18', NULL, '$3.18', 'Dea Kellom, DEA KELLOM, 5662 E WOODCROSS DR, BOISE, ID 83716-8718', 'Printed', '9400111699000554736936', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(111, 'US-443', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'Carol Simpson Gill, CAROL SIMPSON GILL, 213 SUNSET DR, WAVERLY, TN 37185-1847', 'Printed', '9400111699000554736738', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(112, 'US-442', '37', 76, '6/26/2020', '$4.18', NULL, '$4.18', 'Henri LeBlanc, HENRI LEBLANC, 7871 PINE BLUFF RD, DENHAM SPRINGS, LA 70726-8904', 'Printed', '9400111699000554736752', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 10oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(113, 'RZ-733', '37', 76, '6/26/2020', '$3.18', NULL, '$3.18', 'JEANNE SIMS, JEANNE SIMS, 18329 E STATE ROUTE 3 UNIT 302, ALLYN, WA 98524-3213', 'Printed', '9400111699000554736806', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/27/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(114, 'US-465', '37', 76, '6/26/2020', '$2.76', NULL, '$2.76', 'Violet Benjamin, VIOLET BENJAMIN, 1168 BELL HILL RD, PAINTED POST, NY 14870-9761', 'Printed', '9400111699000554748519', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(115, 'RZ-732', '37', 76, '6/26/2020', '$4.18', NULL, '$4.18', 'MICHELLE ADAMS, MICHELLE ADAMS, 585 ASCOT WAY, AZLE, TX 76020-2686', 'Printed', '9400111699000559218598', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 9oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(116, 'RZ-731', '37', 76, '6/26/2020', '$2.93', NULL, '$2.93', 'CAROL UBERBACHER, CAROL UBERBACHER, 10212 SHELBURNE DR, DALLAS, TX 75227-7637', 'Printed', '9400111699000559218475', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(117, 'RZ-730', '37', 76, '6/26/2020', '$2.84', NULL, '$2.84', 'LAURA A REDMOND, LAURA A REDMOND, 2719 25TH AVE N, SAINT PETERSBURG, FL 33713-3922', 'Printed', '9400111699000559218499', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(118, 'RZ-728', '37', 76, '6/26/2020', '$4.46', NULL, '$4.46', 'CYNTHIA  B LUCE, CYNTHIA B LUCE, 1004 HUDSON LN, NAPA, CA 94558-5504', 'Printed', '9400111699000559218031', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 8oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(119, 'RZ-706', '37', 76, '6/26/2020', '$3.18', NULL, '$3.18', 'BRIAN DOLAN, BRIAN DOLAN, 309 BATES RD, KELSO, WA 98626-4911', 'Printed', '9400111699000559218024', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(120, 'RZ-705', '37', 76, '6/26/2020', '$3.31', NULL, '$3.31', 'E SHOLTY, E SHOLTY, 4426 NW 82ND AVE, CORAL SPRINGS, FL 33065-1314', 'Printed', '9400111699000559218345', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(121, 'RZ-693', '37', 76, '6/26/2020', '$3.18', NULL, '$3.18', 'ROBERT SERVICE, ROBERT SERVICE, 6677 SW 196TH AVE, BEAVERTON, OR 97078-4389', 'Printed', '9400111699000559218130', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(122, 'RZ-690', '37', 76, '6/26/2020', '$3.05', NULL, '$3.05', 'STEWART YELLOWHORSE, STEWART YELLOWHORSE, PO BOX 3116, GALLUP, NM 87305-3116', 'Printed', '9400111699000559218642', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/26/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 10:54:07', '2020-07-03 10:54:07'),
(123, 'BA-361', '38', 74, '6/23/2020', '$3.39', NULL, '$3.39', 'Cathy Fuller, CATHY FULLER, 6524 LYNDALE DR, WATAUGA, TX 76148-2823', 'Printed', '9400111699000587725655', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '6/23/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(124, 'BA-362', '38', 74, '6/23/2020', '$2.76', NULL, '$2.76', 'Loretta Ciccia, LORETTA CICCIA, 194 BREMEN ST, EAST BOSTON, MA 02128-1738', 'Printed', '9400111699000587725648', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/23/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(125, 'BA-381', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Deborah Coleman, DEBORAH COLEMAN, 2017 S 71ST ST, PHILADELPHIA, PA 19142-1132', 'Printed', '9400111699000587726294', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/23/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(126, 'BA-401', '38', 74, '6/23/2020', '$3.05', NULL, '$3.05', 'sharon z wise, SHARON Z WISE, 16680 CAVANAUGH RD, KEENESBURG, CO 80643-4212', 'Printed', '9400111699000587754020', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/23/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(127, 'BA-301', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Amanda Chavarria, AMANDA CHAVARRIA, 7 CLARA BARTON LN, GALVESTON, TX 77551-1102', 'Printed', '9400111699000587520595', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(128, 'BA-302', '38', 74, '6/23/2020', '$3.05', NULL, '$3.05', 'Dale Adams, DALE ADAMS, 1861 RAVEN AVE UNIT A8, ESTES PARK, CO 80517-9417', 'Printed', '9400111699000587528225', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(129, 'BA-303', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'pamela abshear, PAMELA ABSHEAR, 555 N FLAG VALLEY RD, PRESCOTT, AZ 86303-3110', 'Printed', '9400111699000587528270', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(130, 'BA-304', '38', 74, '6/23/2020', '$3.21', NULL, '$3.21', 'Arib Jerai, ARIB JERAI, 151 FAYETTE AVE, WAYNE, NJ 07470-6780', 'Printed', '9400111699000587528898', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(131, 'BA-305', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'R. Susan  Carpenter, R. SUSAN CARPENTER, 6855 REFUGEE RD, PICKERINGTON, OH 43147-8983', 'Printed', '9400111699000587528133', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(132, 'BA-306', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Barbara Meyers, BARBARA MEYERS, 2307 SUMMIT DR, MCKINNEY, TX 75071-2613', 'Printed', '9400111699000587528348', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(133, 'BA-307', '38', 74, '6/23/2020', '$3.67', NULL, '$3.67', 'Gretchen Haganlocher, GRETCHEN HAGANLOCHER, 1736 SUMMIT AVE APT 306, SEATTLE, WA 98122-2169', 'Printed', '9400111699000587528003', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(134, 'BA-308', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Adam Levenberg, ADAM LEVENBERG, 8161 MANITOBA ST APT 7, PLAYA DEL REY, CA 90293-8234', 'Printed', '9400111699000587528416', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(135, 'BA-309', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'lorena schami, LORENA SCHAMI, 355 CORONA DR, LA CANADA, CA 91011-4134', 'Printed', '9400111699000587528423', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(136, 'BA-310', '38', 74, '6/23/2020', '$4.18', NULL, '$4.18', 'Aliyliyah  Jansma, ALIYLIYAH JANSMA, 4515 WESTCHESTER GLEN DR, GRAND PRAIRIE, TX 75052-3550', 'Printed', '9400111699000587528560', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 8oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(137, 'BA-311', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'angelia rhodes, ANGELIA RHODES, 155 MISSION COVE CIR, SAINT AUGUSTINE, FL 32084-1151', 'Printed', '9400111699000587528577', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19');
INSERT INTO `trackings` (`id`, `item_id`, `order_id`, `user_id`, `print_date`, `amount`, `adj_amount`, `quoted_amt`, `recipient`, `status`, `tracking_id`, `date_delivered`, `carrier`, `class_service`, `insured_value`, `insurance_id`, `cost_code`, `weight`, `ship_date`, `refund_type`, `user`, `refund_request_date`, `refund_status`, `refund_requested`, `reference1`, `reference2`, `created_at`, `updated_at`) VALUES
(138, 'BA-312', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Ann Pendleton, ANN PENDLETON, 22449 HAND RD, ATCHISON, KS 66002-4003', 'Printed', '9400111699000587527730', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(139, 'BA-313', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Audrey Griffith, AUDREY GRIFFITH, 1945 LAKE POINT DR, WESTON, FL 33326-2356', 'Printed', '9400111699000587527662', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(140, 'BA-314', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Sue Jones, SUE JONES, 790 BAYSIDE LN, WESTON, FL 33326-3337', 'Printed', '9400111699000587527648', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(141, 'BA-315', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'SUSAN C.NOLTING, SUSAN C.NOLTING, 15543 GARNET WAY, APPLE VALLEY, MN 55124-5158', 'Printed', '9400111699000587527198', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(142, 'BA-316', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Erin OReilly, ERIN OREILLY, 1301 KEARNEY ST, BELLINGHAM, WA 98225-3334', 'Printed', '9400111699000587527358', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(143, 'BA-317', '38', 74, '6/23/2020', '$3.21', NULL, '$3.21', 'Scott Brown, SCOTT BROWN, 248 RUMBLING ROCK RD, HEDGESVILLE, WV 25427-4432', 'Printed', '9400111699000587527341', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(144, 'BA-318', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Carol DellaPenna, CAROL DELLAPENNA, 6046 BRIDGET ST, PHILADELPHIA, PA 19144-3702', 'Printed', '9400111699000587527013', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(145, 'BA-319', '38', 74, '6/23/2020', '$2.76', NULL, '$2.76', 'Cathleen Cabral, CATHLEEN CABRAL, 31 ARTHUR AVE APT 1, EAST PROVIDENCE, RI 02914-4017', 'Printed', '9400111699000587527020', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(146, 'BA-320', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Debra White, DEBRA WHITE, 4477 MULDOON ST, CARSON CITY, NV 89701-7824', 'Printed', '9400111699000587527419', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(147, 'BA-321', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Linda McKinney, LINDA MCKINNEY, 1403 57TH ST W, BRADENTON, FL 34209-4729', 'Printed', '9400111699000587741358', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(148, 'BA-322', '38', 74, '6/23/2020', '$3.21', NULL, '$3.21', 'Carla Besosa, CARLA BESOSA, 171 SOMERVELLE ST APT 313, ALEXANDRIA, VA 22304-8651', 'Printed', '9400111699000587741327', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(149, 'BA-323', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Juan Perez, JUAN PEREZ, 9820 SW 80TH DR, MIAMI, FL 33173-4037', 'Printed', '9400111699000587741334', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(150, 'BA-324', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'Courtney Brokaw, COURTNEY BROKAW, 40 REVERE DR, DAVISON, MI 48423-2645', 'Printed', '9400111699000587741099', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(151, 'BA-325', '38', 74, '6/23/2020', '$4.46', NULL, '$4.46', 'Carrie Thomas, CARRIE THOMAS, 330 E MILLAN ST, CHULA VISTA, CA 91910-6314', 'Printed', '9400111699000587741495', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 9oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(152, 'BA-326', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'danielle newman, DANIELLE NEWMAN, 562 ANNA MARIA ST, LIVERMORE, CA 94550-5221', 'Printed', '9400111699000587741433', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(153, 'BA-327', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'Debbie Cardarelli, DEBBIE CARDARELLI, 1060 BROOKE AVE, CINCINNATI, OH 45230-3611', 'Printed', '9400111699000587741501', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(154, 'BA-328', '38', 74, '6/23/2020', '$3.05', NULL, '$3.05', 'Deborah Letteau, DEBORAH LETTEAU, 6324 DANTE LN NW, ALBUQUERQUE, NM 87114-5021', 'Printed', '9400111699000587779214', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(155, 'BA-329', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'Deborah Phillips, DEBORAH PHILLIPS, 2435 FEWS BRIDGE RD, GREER, SC 29651-4970', 'Printed', '9400111699000587779290', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(156, 'BA-330', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Dawn Cohen, DAWN COHEN, 10707 GRAYSLAKE DR, RENO, NV 89521-8243', 'Printed', '9400111699000587779146', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(157, 'BA-331', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Debbie Keller, DEBBIE KELLER, 1326 DEANWOOD RD, PARKVILLE, MD 21234-6004', 'Printed', '9400111699000587779139', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(158, 'BA-332', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'DeAna Owens, DEANA OWENS, 10751 BLUE SPRUCE DR, FISHERS, IN 46037-9269', 'Printed', '9400111699000587779368', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(159, 'BA-333', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Debra Leslie, DEBRA LESLIE, 21 FLEETWOOD DR, PALM COAST, FL 32137-8459', 'Printed', '9400111699000587779399', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(160, 'BA-334', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'LYNA LOW, LYNA LOW, 11614 HAZELNUT CT, OREGON CITY, OR 97045-6750', 'Printed', '9400111699000587779382', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(161, 'BA-335', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Randall Whittier, RANDALL WHITTIER, 126 NW ELMWOOD AVE, TOPEKA, KS 66606-1203', 'Printed', '9400111699000587779054', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(162, 'BA-336', '38', 74, '6/23/2020', '$3.21', NULL, '$3.21', 'Dayle Allen, DAYLE ALLEN, 248 FOX RD, DOVER, DE 19901-4717', 'Printed', '9400111699000587779047', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 7oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(163, 'BA-337', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'louise seeley, LOUISE SEELEY, 10275 GULF BLVD APT 402, TREASURE ISLAND, FL 33706-4800', 'Printed', '9400111699000587779498', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(164, 'BA-338', '38', 74, '6/23/2020', '$2.76', NULL, '$2.76', 'elise karish, elise karish, 39 Lynnfield St, Peabody, MA 01960-5731', 'Printed', '9400111699000587779511', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(165, 'BA-339', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Fontaine Boutwell, FONTAINE BOUTWELL, 12556 RICHARDS ROOK LN, JACKSONVILLE, FL 32246-7076', 'Printed', '9400111699000587779504', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(166, 'BA-340', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Isabel Fiolek, ISABEL FIOLEK, 17247 CANNONADE DR, LEESBURG, VA 20176-7193', 'Printed', '9400111699000587779535', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(167, 'BA-341', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Cally Forster, CALLY FORSTER, 4011 PIPELINE RD, BLAINE, WA 98230-9745', 'Printed', '9400111699000587770013', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(168, 'BA-342', '38', 74, '6/23/2020', '$2.76', NULL, '$2.76', 'iris goldfarb, iris goldfarb, 5636 South Street Rd, auburn, NY 13021-9602', 'Printed', '9400111699000587770051', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(169, 'BA-343', '38', 74, '6/23/2020', '$3.67', NULL, '$3.67', 'Christa Mitchell, CHRISTA MITCHELL, 13217 38TH AVE S, TUKWILA, WA 98168-3113', 'Printed', '9400111699000587770075', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(170, 'BA-344', '38', 74, '6/23/2020', '$3.67', NULL, '$3.67', 'Jacqueline Engel, JACQUELINE ENGEL, 11838 SUSAN DR, GRANADA HILLS, CA 91344-2637', 'Printed', '9400111699000587770464', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(171, 'BA-345', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'janice moring, JANICE MORING, 1260 LELAND AVE APT 1C, BRONX, NY 10472-4834', 'Printed', '9400111699000587770495', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(172, 'BA-346', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Jeanne Furchtenicht, JEANNE FURCHTENICHT, 1007 CLINTON AVE, DES MOINES, IA 50313-4023', 'Printed', '9400111699000587770471', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(173, 'BA-347', '38', 74, '6/23/2020', '$5.27', NULL, '$5.27', 'Jo Angela Maniaci CMP, JO ANGELA MANIACI CMP, 26 10TH ST W UNIT 1602, SAINT PAUL, MN 55102-1040', 'Printed', '9400111699000587770525', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 13oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(174, 'BA-348', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'J V Thatcher, J V THATCHER, 9401 TANAGER AVE, FOUNTAIN VALLEY, CA 92708-6558', 'Printed', '9400111699000587770570', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(175, 'BA-349', '38', 74, '6/23/2020', '$4.46', NULL, '$4.46', 'Kathleen E. Lindquester, KATHLEEN E. LINDQUESTER, 727 SE PARROTT ST, ROSEBURG, OR 97470-3460', 'Printed', '9400111699000587778224', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 10oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(176, 'BA-350', '38', 74, '6/23/2020', '$3.67', NULL, '$3.67', 'Katherine Lewis, KATHERINE LEWIS, 1595 LOS OSOS VALLEY RD SPC 20B, LOS OSOS, CA 93402-2958', 'Printed', '9400111699000587778866', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(177, 'BA-351', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Kathy Blackmarr, KATHY BLACKMARR, 5291 VICTORIA PL, WESTMINSTER, CA 92683-4847', 'Printed', '9400111699000587778897', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(178, 'BA-352', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'kerry edwards, KERRY EDWARDS, 5915 AVILA ST APT A, EL CERRITO, CA 94530-3451', 'Printed', '9400111699000587778767', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(179, 'BA-353', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Kathryn Wildgen, KATHRYN WILDGEN, 826 E 4TH AVE, COVINGTON, LA 70433-4149', 'Printed', '9400111699000587778798', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(180, 'BA-354', '38', 74, '6/23/2020', '$3.31', NULL, '$3.31', 'Kenneth Klein, KENNETH KLEIN, N8385 CALDRON FALLS RD, CRIVITZ, WI 54114-9030', 'Printed', '9400111699000587778910', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(181, 'BA-355', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Salli Karuschkat, SALLI KARUSCHKAT, 15110 FALLING CREEK DR, HOUSTON, TX 77068-2418', 'Printed', '9400111699000587778903', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(182, 'BA-356', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Deni Hix, DENI HIX, 15657 MEADOW CT, PLATTE CITY, MO 64079-7235', 'Printed', '9400111699000587778941', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(183, 'BA-357', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Elista Gerace, ELISTA GERACE, 8809 28TH ST E, PARRISH, FL 34219-8303', 'Printed', '9400111699000587778620', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(184, 'BA-358', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Leonel Contreras, LEONEL CONTRERAS, 3408 SOMERSET AVE APT D, CASTRO VALLEY, CA 94546-3362', 'Printed', '9400111699000587778644', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(185, 'BA-359', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'Jennifer Holloway, JENNIFER HOLLOWAY, 7116 SOMERSET SPRINGS DR, CHARLOTTE, NC 28262-3583', 'Printed', '9400111699000587778675', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(186, 'BA-360', '38', 74, '6/23/2020', '$3.05', NULL, '$3.05', 'Linda Spencer, LINDA SPENCER, PO BOX 2877, PAGOSA SPRINGS, CO 81147-2877', 'Printed', '9400111699000587778163', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(187, 'BA-363', '38', 74, '6/23/2020', '$4.08', NULL, '$4.08', 'lisa macci, LISA MACCI, 8791 SONOMA LAKE BLVD, BOCA RATON, FL 33434-4068', 'Printed', '9400111699000587725143', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 8oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(188, 'BA-364', '38', 74, '6/23/2020', '$3.67', NULL, '$3.67', 'jacquelyn Olbrychowski, JACQUELYN OLBRYCHOWSKI, 7741 TINTED MESA CT, LAS VEGAS, NV 89149-6440', 'Printed', '9400111699000587725365', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(189, 'BA-365', '38', 74, '6/23/2020', '$3.25', NULL, '$3.25', 'Ashley Matthews, ASHLEY MATTHEWS, 4600 ALEXANDER VALLEY DR APT 102, CHARLOTTE, NC 28270-1516', 'Printed', '9400111699000587725389', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 7oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(190, 'BA-366', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Melissa Donaldson, MELISSA DONALDSON, 373 EMMA DR, WINDER, GA 30680-4084', 'Printed', '9400111699000587725013', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(191, 'BA-367', '38', 74, '6/23/2020', '$3.39', NULL, '$3.39', 'Olga Tillett, OLGA TILLETT, 1285 EUBANKS RD, SEAGOVILLE, TX 75159-6013', 'Printed', '9400111699000587725006', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 7oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(192, 'BA-368', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'Ann Fisher, ANN FISHER, 1514 ZULETA AVE, CORAL GABLES, FL 33146-2318', 'Printed', '9400111699000587725082', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(193, 'BA-369', '38', 74, '6/23/2020', '$2.76', NULL, '$2.76', 'Mildred Gibson, MILDRED GIBSON, 34 DUNN CEMETARY RD, MORGANTOWN, WV 26508-5240', 'Printed', '9400111699000587725464', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(194, 'BA-370', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Maya Hudson, MAYA HUDSON, 1573 BRASS LANTERN WAY, RESTON, VA 20194-1224', 'Printed', '9400111699000587725402', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(195, 'BA-371', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Loretta Gardiner, LORETTA GARDINER, PO BOX 258, FAWNSKIN, CA 92333-0258', 'Printed', '9400111699000587725488', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(196, 'BA-372', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'Dixie DeLoach, DIXIE DELOACH, 314 SHETLAND LN, LEXINGTON, SC 29073-8795', 'Printed', '9400111699000587725518', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(197, 'BA-373', '38', 74, '6/23/2020', '$3.52', NULL, '$3.52', 'Nancy Standridge, NANCY STANDRIDGE, 309 S SHERMAN ST # 44, BYERS, CO 80103-9793', 'Printed', '9400111699000587725525', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(198, 'BA-374', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'linda lydon, LINDA LYDON, 90 HIGH ST UNIT 1, PORTLAND, ME 04101-3865', 'Printed', '9400111699000587722210', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(199, 'BA-375', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'dianna nerrren, DIANNA NERRREN, 107 MARION AVE, HICKMAN, KY 42050-2041', 'Printed', '9400111699000587722265', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(200, 'BA-376', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Pamela Jackson, PAMELA JACKSON, 4305 PEPPERDINE DR, SAN BERNARDINO, CA 92407-3340', 'Printed', '9400111699000587722203', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(201, 'BA-377', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'patricia luce, PATRICIA LUCE, 2603 CARMICHAEL WAY APT 3, CARMICHAEL, CA 95608-5304', 'Printed', '9400111699000587722241', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(202, 'BA-378', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Debbi Sherman, DEBBI SHERMAN, 3221 BROOKWOOD RD, SACRAMENTO, CA 95821-3709', 'Printed', '9400111699000587722814', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(203, 'BA-379', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Suzanne Pires, SUZANNE PIRES, 13719 VAN DOREN RD, MANASSAS, VA 20112-3801', 'Printed', '9400111699000587722845', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(204, 'BA-380', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Angela S Saxon, ANGELA S SAXON, 1322 EL CAMINO REAL, REDWOOD CITY, CA 94063-1809', 'Printed', '9400111699000587722715', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(205, 'BA-382', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Rheta Hughes, RHETA HUGHES, 1261 ZELZAH AVE, PAHRUMP, NV 89048-2753', 'Printed', '9400111699000587726270', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(206, 'BA-383', '38', 74, '6/23/2020', '$3.52', NULL, '$3.52', 'Patricia Scott, PATRICIA SCOTT, 22990 SHARPE RD, NEW UNDERWOOD, SD 57761-6017', 'Printed', '9400111699000587726829', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(207, 'BA-384', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Ronald Stillman, RONALD STILLMAN, 2828 GALENA DR, HENDERSON, NV 89074-6448', 'Printed', '9400111699000587726836', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(208, 'BA-385', '38', 74, '6/23/2020', '$2.78', NULL, '$2.78', 'Ronald N Glass, RONALD N GLASS, 4935 TOWER RD APT D, GREENSBORO, NC 27410-5713', 'Printed', '9400111699000587726973', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(209, 'BA-386', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Dana Perkins, DANA PERKINS, 12421 W NOLEN RD TRLR 2, TUCSON, AZ 85743-7675', 'Printed', '9400111699000587726621', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(210, 'BA-387', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'ROXANA LABAGNARA, ROXANA LABAGNARA, 1112 MONTANA AVE STE 3, SANTA MONICA, CA 90403-1669', 'Printed', '9400111699000587726676', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(211, 'BA-388', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Saranda Wymer, SARANDA WYMER, 33 S SPANISH TRAIL DR, VEYO, UT 84782-4002', 'Printed', '9400111699000587726164', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(212, 'BA-389', '38', 74, '6/23/2020', '$3.05', NULL, '$3.05', 'Shirley Jones, SHIRLEY JONES, 2890 C 1/2 RD, GRAND JUNCTION, CO 81501-4714', 'Printed', '9400111699000587726355', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(213, 'BA-390', '38', 74, '6/23/2020', '$3.67', NULL, '$3.67', 'Mary Lynn Turner, MARY LYNN TURNER, 6323 SPRINGFIELD ST, SAN DIEGO, CA 92114-2025', 'Printed', '9400111699000587726393', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 6oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(214, 'BA-391', '38', 74, '6/23/2020', '$3.97', NULL, '$3.97', 'Jenny Kucherawy, JENNY KUCHERAWY, 115 WILMONT AVE, WASHINGTON, PA 15301-3526', 'Printed', '9400111699000587726041', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 11oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(215, 'BA-392', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Alla Maier, ALLA MAIER, 5011 SAINT SIMON CT, FREDERICK, MD 21703-6812', 'Printed', '9400111699000587726423', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(216, 'BA-393', '38', 74, '6/23/2020', '$3.25', NULL, '$3.25', 'Amanda Copeland, AMANDA COPELAND, 3124 1/2 KANAWHA TER, SAINT ALBANS, WV 25177-2284', 'Printed', '9400111699000587726478', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(217, 'BA-394', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Steve Edmondson, STEVE EDMONDSON, 1227 HUMBOLDT ST, SANTA ROSA, CA 95404-2842', 'Printed', '9400111699000587726591', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(218, 'BA-395', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Yuriy Skorokhodov, YURIY SKOROKHODOV, 1445 SHORE PKWY APT 7E, BROOKLYN, NY 11214-6119', 'Printed', '9400111699000587721268', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(219, 'BA-396', '38', 74, '6/23/2020', '$3.31', NULL, '$3.31', 'Teri Marino, TERI MARINO, 9511 NW 80TH ST, TAMARAC, FL 33321-1352', 'Printed', '9400111699000587721220', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(220, 'BA-397', '38', 74, '6/23/2020', '$2.74', NULL, '$2.74', 'Susan Lounsbury, SUSAN LOUNSBURY, 59 SAINT JAMES ST APT 2, KINGSTON, NY 12401-0518', 'Printed', '9400111699000587721282', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(221, 'BA-398', '38', 74, '6/23/2020', '$2.76', NULL, '$2.76', 'Jessica Lulay Petzold, JESSICA LULAY PETZOLD, 200 GOLDFINCH DR APT 202, COVENTRY, RI 02816-6751', 'Printed', '9400111699000587721275', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(222, 'BA-399', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'L.A. Shubin, L.A. SHUBIN, 415 19TH ST APT B, HUNTINGTON BEACH, CA 92648-3833', 'Printed', '9400111699000587721886', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(223, 'BA-400', '38', 74, '6/23/2020', '$3.18', NULL, '$3.18', 'Natalia Olivares, NATALIA OLIVARES, 11275 SWENSON ST, RIVERSIDE, CA 92505-2278', 'Printed', '9400111699000587721763', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(224, 'BA-402', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Carolyn Ryerson, CAROLYN RYERSON, 2907 COVEY CIR, MISSOURI CITY, TX 77459-3305', 'Printed', '9400111699000587754037', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(225, 'BA-422', '38', 74, '6/23/2020', '$5.70', NULL, '$5.70', 'Vikki Dunaway, VIKKI DUNAWAY, 10825 7TH AVE SE, OLYMPIA, WA 98513-9699', 'Printed', '9400111699000587754495', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 13oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(226, 'BA-423', '38', 74, '6/23/2020', '$4.46', NULL, '$4.46', 'Ellen Ortiz, ELLEN ORTIZ, 6481 N SNOWFLAKE DR, FLAGSTAFF, AZ 86004-2635', 'Printed', '9400111699000587754563', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 9oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(227, 'BA-424', '38', 74, '6/23/2020', '$5.04', NULL, '$5.04', 'Franne Kaplan, FRANNE KAPLAN, 220 PARK LN # 123, PORT EWEN, NY 12466-7801', 'Printed', '9400111699000587754501', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 13oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(228, 'BA-425', '38', 74, '6/23/2020', '$2.84', NULL, '$2.84', 'iloveallthisstuff, ILOVEALLTHISSTUFF, PO BOX 2485, ACWORTH, GA 30102-0009', 'Printed', '9400111699000587754532', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(229, 'BA-426', '38', 74, '6/23/2020', '$5.54', NULL, '$5.54', 'Michelle Ivery, MICHELLE IVERY, 3716 FLORENCE GRV, SCHERTZ, TX 78154-3651', 'Printed', '9400111699000587755201', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 13oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(230, 'BA-427', '38', 74, '6/23/2020', '$5.27', NULL, '$5.27', 'Luann Reynolds, LUANN REYNOLDS, 2347 HERITAGE CIR, NAVARRE, FL 32566-2883', 'Printed', '9400111699000587755270', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 14oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(231, 'BA-428', '38', 74, '6/23/2020', '$2.93', NULL, '$2.93', 'Terry Weldon, TERRY WELDON, 39447 SABLE LN, PONCHATOULA, LA 70454-7343', 'Printed', '9400111699000587755898', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(232, 'BA-429', '38', 74, '6/23/2020', '$3.39', NULL, '$3.39', 'sheila haley, SHEILA HALEY, 912 INDIAN RD, CLAY CENTER, KS 67432-9040', 'Printed', '9400111699000587755706', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 5oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(233, 'BA-430', '38', 74, '6/23/2020', '$5.27', NULL, '$5.27', 'eunice walker, EUNICE WALKER, 970 GRANT ST SE, ATLANTA, GA 30315-2013', 'Printed', '9400111699000587755775', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 14oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(234, 'BA-431', '38', 74, '6/23/2020', '$3.25', NULL, '$3.25', 'kris  johnson, KRIS JOHNSON, 908 LYLE CT, LAGRANGE, KY 40031-2300', 'Printed', '9400111699000587755904', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 8oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(235, 'BA-432', '38', 74, '6/23/2020', '$3.25', NULL, '$3.25', 'Tammy Maria Settles, TAMMY MARIA SETTLES, 9006 SAINT LUCIA LN, CHARLOTTE, NC 28277-8662', 'Printed', '9400111699000587755973', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 4oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(236, 'BA-433', '38', 74, '6/23/2020', '$3.93', NULL, '$3.93', 'Mustafa Saleem, MUSTAFA SALEEM, 2801 ASHFORD CT, MIDDLETOWN, NJ 07748-4231', 'Printed', '9400111699000587755607', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 11oz', '6/24/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-03 17:07:19', '2020-07-03 17:07:19'),
(237, '34', '34', 75, NULL, NULL, NULL, NULL, NULL, NULL, '345343454', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/08/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-10 13:05:56', '2020-07-10 13:05:56'),
(238, '30', '30', 75, NULL, NULL, NULL, NULL, NULL, NULL, '243534345435', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/07/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-10 13:07:37', '2020-07-10 13:07:37'),
(239, '49', '49', 75, NULL, NULL, NULL, NULL, NULL, NULL, '3458459847', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/07/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-10 16:45:02', '2020-07-10 16:45:02'),
(240, 'USA-799', '49', 74, '7/4/20', '$2.76', NULL, '$2.76', 'JANET SPITZ, JANET SPITZ, 386 POTTER HILL RD, PETERSBURGH, NY 12138-3213', 'Printed', '9400111699000572685111', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(241, 'USA-798', '49', 74, '7/4/20', '$2.93', NULL, '$2.93', 'CHRISTINE RISSLER, CHRISTINE RISSLER, 3302 S ELM ST, SILOAM SPRINGS, AR 72761-8816', 'Printed', '9400111699000572685112', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(242, 'USA-797', '49', 74, '7/4/20', '$2.78', NULL, '$2.78', 'SUSAN SPENCE, SUSAN SPENCE, 256 HEATHWOOD DR, SPARTANBURG, SC 29307-3737', 'Printed', '9400111699000572685113', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(243, 'USA-796', '49', 74, '7/4/20', '$2.84', NULL, '$2.84', 'HOLLY NELSON, HOLLY NELSON, 419 SW 34TH TER, CAPE CORAL, FL 33914-7824', 'Printed', '9400111699000572685114', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(244, 'USA-795', '49', 74, '7/4/20', '$3.18', NULL, '$3.18', 'KIM BAKER, KIM BAKER, PO BOX 1154, CARNATION, WA 98014-1154', 'Printed', '9400111699000572685115', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(245, 'USA-794', '49', 74, '7/4/20', '$2.84', NULL, '$2.84', 'REGINA DEFRANGO, REGINA DEFRANGO, PO BOX 826, CRESCENT CITY, FL 32112-0826', 'Printed', '9400111699000572685116', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 3oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(246, 'USA-793', '49', 74, '7/4/20', '$3.05', NULL, '$3.05', 'PEGGY PURKHOSROW, PEGGY PURKHOSROW, 637B S BROADWAY ST PMB 307, BOULDER, CO 80305-5926', 'Printed', '9400111699000572685117', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 2oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(247, 'USA-792', '49', 74, '7/4/20', '$3.18', NULL, '$3.18', 'EISABETH SUTTON, EISABETH SUTTON, PO BOX 723, SITKA, AK 99835-0723', 'Printed', '9400111699000572685118', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(248, 'USA-791', '49', 74, '7/4/20', '$3.18', NULL, '$3.18', 'BONNIE SARGENT, BONNIE SARGENT, PO BOX 1386, PAHOA, HI 96778-1386', 'Printed', '9400111699000572685119', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 0oz', '7/9/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(249, 'USA-790', '49', 74, '7/4/20', '$2.84', NULL, '$2.84', 'Alex Campbell, ALEX CAMPBELL, 105 N CARAWAY UNIT 1512, STATE UNIVERSITY, AR 72467-7351', 'Printed', '9400111699000572685120', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 1oz', '7/10/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-10 17:01:09', '2020-07-10 17:01:09'),
(251, '', '', 73, NULL, NULL, NULL, NULL, NULL, NULL, 'xczxcxcxc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/22/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-11 04:18:24', '2020-07-11 04:18:24'),
(252, '45', '45', 73, NULL, NULL, NULL, NULL, NULL, NULL, 'lkhfghj14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-11 04:25:56', '2020-07-11 04:26:07'),
(253, '50', '50', 75, NULL, NULL, NULL, NULL, NULL, NULL, '45634563546', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/08/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:23:07', '2020-07-11 12:23:07'),
(254, 'TEST-123', '50', 74, '7/8/20', '$3.18 ', NULL, '$3.18 ', 'GARRETT  PERNEY, GARRETT PERNEY, PO BOX 4599, PASO ROBLES, CA 93447-4599', 'Printed', '9400111699000519306549', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(255, 'TEST-124', '50', 74, '7/8/20', '$3.18 ', NULL, '$3.18 ', 'CHRISTINE ROTHNIE, CHRISTINE ROTHNIE, PO BOX 6724, OCEAN VIEW, HI 96737-6724', 'Printed', '9400111699000519306563', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(256, 'TEST-125', '50', 74, '7/8/20', '$3.21 ', NULL, '$3.21 ', 'MAYA FREDO, MAYA FREDO, PO BOX 249, GREAT CACAPON, WV 25422-0249', 'Printed', '9400111699000519306464', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 6oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(257, 'TEST-126', '50', 74, '7/8/20', '$2.84 ', NULL, '$2.84 ', 'NOREEN DOBBELS, NOREEN DOBBELS, 311 12TH AVE, SILVIS, IL 61282-1869', 'Printed', '9400111699000519306082', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(258, 'TEST-127', '50', 74, '7/8/20', '$2.74 ', NULL, '$2.74 ', 'SHIRLEY ENGELHARDT, SHIRLEY ENGELHARDT, 1401 OCEAN AVE APT 4H, BROOKLYN, NY 11230-3908', 'Printed', '9400111699000519306068', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(259, 'TEST-128', '50', 74, '7/8/20', '$2.78 ', NULL, '$2.78 ', 'DIANA POMPIE, DIANA POMPIE, 146 LESTER DR, DANIELS, WV 25832-9557', 'Printed', '9400111699000519306389', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(260, 'TEST-129', '50', 74, '7/8/20', '$2.74 ', NULL, '$2.74 ', 'VLAD DUBROVSKI, VLAD DUBROVSKI, 113 BRANDYWINE DR, BETHLEHEM, PA 18020-9575', 'Printed', '9400111699000519306310', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(261, 'TEST-130', '50', 74, '7/8/20', '$2.84 ', NULL, '$2.84 ', 'DONNA P SILVA, DONNA P SILVA, 3691 GATLIN DR, VIERA, FL 32955-6050', 'Printed', '9400111699000519306198', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(262, 'TEST-131', '50', 74, '7/8/20', '$3.18 ', NULL, '$3.18 ', 'GARRIS LEISTEN-GREGORY, GARRIS LEISTEN-GREGORY, 850 SW TOUCHMARK WAY APT 402, PORTLAND, OR 97225-6457', 'Printed', '9400111699000519306631', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/13/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-11 12:25:50', '2020-07-11 12:25:50'),
(263, '51', '51', 75, NULL, NULL, NULL, NULL, NULL, NULL, '3463246', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/08/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-14 15:44:08', '2020-07-14 15:44:08'),
(264, '52', '52', 73, NULL, NULL, NULL, NULL, NULL, NULL, '3634563456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-14 15:45:54', '2020-07-16 08:19:35'),
(265, 'SHOE-03', '52', 74, '7/13/2020', '$3.93', NULL, '$3.93', 'BESMIR MUSTAFAJ, BESMIR MUSTAFAJ, PO BOX 1856, OSSINING, NY 10562-0076', 'Printed', '9400111699000532947637', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 10oz', '7/17/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-14 15:47:42', '2020-07-14 15:47:42'),
(266, 'SHOE-01', '52', 74, '7/13/2020', '$5.70', NULL, '$5.70', 'MELISSA SIETSMA, MELISSA SIETSMA, PO BOX 1277, TALKEETNA, AK 99676-1277', 'Printed', '9400111699000532947125', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 14oz', '7/17/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-14 15:47:42', '2020-07-14 15:47:42'),
(267, 'SHOE-02', '52', 74, '7/13/2020', '$5.40', NULL, '$5.40', 'EDEN DARLING, EDEN DARLING, 315 N SHERMAN St, Jennings, LA 70546-5959', 'Printed', '9400111699000532947316', NULL, 'USPS', 'First Class (R)', '$0.00', NULL, '<None>', '0lb 14oz', '7/17/2020', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-14 15:47:42', '2020-07-14 15:47:42'),
(268, '62', '62', 73, NULL, NULL, NULL, NULL, NULL, NULL, '1234567889', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-15 09:26:40', '2020-07-15 09:36:57'),
(294, 'BA5-Box 4', '63', 74, '06-05-2020', '$4.18 ', NULL, '$4.18 ', 'Amy DeVoe-Moses, AMY DEVOE-MOSES, PO BOX 1602, TRINITY, TX 75862-1602', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 9oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-15 11:03:49', '2020-07-15 11:03:49'),
(291, 'BA2-Box 4', '63', 74, '06-05-2020', '$3.52 ', NULL, '$3.52 ', 'Amanda long, AMANDA LONG, 5504 ALVARADO RD, AMARILLO, TX 79106-5010', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 6oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-15 11:03:49', '2020-07-15 11:03:49'),
(293, 'BA4-Box 4', '63', 74, '06-05-2020', '$2.84 ', NULL, '$2.84 ', 'amina Luna, amina Luna, 2266 NW 33rd st, Miami, FL 33142-5470', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-15 11:03:49', '2020-07-15 11:03:49'),
(292, 'BA3-Box 4', '63', 74, '06-05-2020', '$3.18 ', NULL, '$3.18 ', 'AMELIA TAMMEL, AMELIA TAMMEL, 324 BROOKS ST, LAGUNA BEACH, CA 92651-2931', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-15 11:03:49', '2020-07-15 11:03:49'),
(290, 'BA1-Box 4', '63', 74, '06-05-2020', '$2.74 ', NULL, '$2.74 ', 'ALICIA FONTAINE, ALICIA FONTAINE, 320 TENNESSEE AVE, ELIZABETHVILLE, PA 17023-9640', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-15 11:03:49', '2020-07-15 11:03:49'),
(295, '63', '63', 73, NULL, NULL, NULL, NULL, NULL, NULL, '145268', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/11/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-15 11:06:50', '2020-07-15 11:06:50'),
(296, '67', '67', 75, NULL, NULL, NULL, NULL, NULL, NULL, '563456345645', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/17/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:36:04', '2020-07-15 18:36:04'),
(297, 'BA-877', '67', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Cathy Baillet, CATHY BAILLET, 2459 FIR ST, GLENVIEW, IL 60025-2705', 'Printed', '9400111699000513428438', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 1oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(298, 'BA-878', '67', 74, '7/12/20', '$3.67 ', NULL, '$3.67 ', 'Betty Meyer, BETTY MEYER, 3855 MADISON AVE, SOUTH OGDEN, UT 84403-2203', 'Printed', '9400111699000513428476', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 7oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(299, 'BA-879', '67', 74, '7/12/20', '$4.32 ', NULL, '$4.32 ', 'Dale Adams, DALE ADAMS, 1861 RAVEN AVE UNIT A8, ESTES PARK, CO 80517-9417', 'Printed', '9400111699000513428513', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 8oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(300, 'BA-880', '67', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Aliyliyah  Jansma, ALIYLIYAH JANSMA, 4515 WESTCHESTER GLEN DR, GRAND PRAIRIE, TX 75052-3550', 'Printed', '9400111699000513428551', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11');
INSERT INTO `trackings` (`id`, `item_id`, `order_id`, `user_id`, `print_date`, `amount`, `adj_amount`, `quoted_amt`, `recipient`, `status`, `tracking_id`, `date_delivered`, `carrier`, `class_service`, `insured_value`, `insurance_id`, `cost_code`, `weight`, `ship_date`, `refund_type`, `user`, `refund_request_date`, `refund_status`, `refund_requested`, `reference1`, `reference2`, `created_at`, `updated_at`) VALUES
(301, 'BA-881', '67', 74, '7/12/20', '$3.93 ', NULL, '$3.93 ', 'Lisa M. Fantinato, LISA M. FANTINATO, 634 SEVERN RD, SEVERNA PARK, MD 21146-3421', 'Printed', '9400111699000513428568', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 11oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(302, 'BA-882', '67', 74, '7/12/20', '$3.05 ', NULL, '$3.05 ', 'Amber Reichelt, AMBER REICHELT, 1506 NOLAN ST, SAN ANTONIO, TX 78202-2441', 'Printed', '9400111699000513428520', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(303, 'BA-883', '67', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Leslie  French, LESLIE FRENCH, 2316 RANCH WAY, LAWRENCE, KS 66047-3324', 'Printed', '9400111699000513428506', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 2oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(304, 'BA-884', '67', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Patricia Biagioni, PATRICIA BIAGIONI, 203 RIDGE RD, JUPITER, FL 33477-9660', 'Printed', '9400111699000513428599', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(305, 'BA-885', '67', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'ANNE  HOWARD, ANNE HOWARD, 3145 EL KU AVE, ESCONDIDO, CA 92025-7333', 'Printed', '9400111699000513428544', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(306, 'BA-886', '67', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'Stacy Aralica, STACY ARALICA, 9021 CAPE WOOD CT # 3, LAS VEGAS, NV 89117-2321', 'Printed', '9400111699000513428582', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-15 18:40:11', '2020-07-15 18:40:11'),
(307, 'BA-866', '68', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Cathy Baillet, CATHY BAILLET, 2459 FIR ST, GLENVIEW, IL 60025-2705', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 1oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(308, 'BA-865', '68', 74, '7/12/20', '$3.67 ', NULL, '$3.67 ', 'Betty Meyer, BETTY MEYER, 3855 MADISON AVE, SOUTH OGDEN, UT 84403-2203', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 7oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(309, 'BA-864', '68', 74, '7/12/20', '$4.32 ', NULL, '$4.32 ', 'Dale Adams, DALE ADAMS, 1861 RAVEN AVE UNIT A8, ESTES PARK, CO 80517-9417', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 8oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(310, 'BA-863', '68', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Aliyliyah  Jansma, ALIYLIYAH JANSMA, 4515 WESTCHESTER GLEN DR, GRAND PRAIRIE, TX 75052-3550', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(311, 'BA-862', '68', 74, '7/12/20', '$3.93 ', NULL, '$3.93 ', 'Lisa M. Fantinato, LISA M. FANTINATO, 634 SEVERN RD, SEVERNA PARK, MD 21146-3421', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 11oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(312, 'BA-861', '68', 74, '7/12/20', '$3.05 ', NULL, '$3.05 ', 'Amber Reichelt, AMBER REICHELT, 1506 NOLAN ST, SAN ANTONIO, TX 78202-2441', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(313, 'BA-860', '68', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Leslie  French, LESLIE FRENCH, 2316 RANCH WAY, LAWRENCE, KS 66047-3324', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 2oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(314, 'BA-859', '68', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Patricia Biagioni, PATRICIA BIAGIONI, 203 RIDGE RD, JUPITER, FL 33477-9660', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(315, 'BA-858', '68', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'ANNE  HOWARD, ANNE HOWARD, 3145 EL KU AVE, ESCONDIDO, CA 92025-7333', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(316, 'BA-857', '68', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'Stacy Aralica, STACY ARALICA, 9021 CAPE WOOD CT # 3, LAS VEGAS, NV 89117-2321', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:52:28', '2020-07-16 06:52:28'),
(317, '68', '68', 75, NULL, NULL, NULL, NULL, NULL, NULL, '345324534534', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/17/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 06:53:13', '2020-07-16 06:53:13'),
(318, '69', '69', 73, NULL, NULL, NULL, NULL, NULL, NULL, '123456789', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/13/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:34:14', '2020-07-16 08:34:14'),
(319, 'FA-8771', '69', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Cathy Baillet, CATHY BAILLET, 2459 FIR ST, GLENVIEW, IL 60025-2705', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 1oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(320, 'FA-8772', '69', 74, '7/12/20', '$3.67 ', NULL, '$3.67 ', 'Betty Meyer, BETTY MEYER, 3855 MADISON AVE, SOUTH OGDEN, UT 84403-2203', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 7oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(321, 'FA-8773', '69', 74, '7/12/20', '$4.32 ', NULL, '$4.32 ', 'Dale Adams, DALE ADAMS, 1861 RAVEN AVE UNIT A8, ESTES PARK, CO 80517-9417', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 8oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(322, 'FA-8774', '69', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Aliyliyah  Jansma, ALIYLIYAH JANSMA, 4515 WESTCHESTER GLEN DR, GRAND PRAIRIE, TX 75052-3550', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(323, 'FA-8775', '69', 74, '7/12/20', '$3.93 ', NULL, '$3.93 ', 'Lisa M. Fantinato, LISA M. FANTINATO, 634 SEVERN RD, SEVERNA PARK, MD 21146-3421', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 11oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(324, 'FA-8776', '69', 74, '7/12/20', '$3.05 ', NULL, '$3.05 ', 'Amber Reichelt, AMBER REICHELT, 1506 NOLAN ST, SAN ANTONIO, TX 78202-2441', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(325, 'FA-8777', '69', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Leslie  French, LESLIE FRENCH, 2316 RANCH WAY, LAWRENCE, KS 66047-3324', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 2oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(326, 'FA-8778', '69', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Patricia Biagioni, PATRICIA BIAGIONI, 203 RIDGE RD, JUPITER, FL 33477-9660', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(327, 'FA-8779', '69', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'ANNE  HOWARD, ANNE HOWARD, 3145 EL KU AVE, ESCONDIDO, CA 92025-7333', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(328, 'FA-8780', '69', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'Stacy Aralica, STACY ARALICA, 9021 CAPE WOOD CT # 3, LAS VEGAS, NV 89117-2321', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:39:40', '2020-07-16 08:39:40'),
(329, 'FA-8661', '69', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Cathy Baillet, CATHY BAILLET, 2459 FIR ST, GLENVIEW, IL 60025-2705', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 1oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(330, 'FA-8662', '69', 74, '7/12/20', '$3.67 ', NULL, '$3.67 ', 'Betty Meyer, BETTY MEYER, 3855 MADISON AVE, SOUTH OGDEN, UT 84403-2203', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 7oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(331, 'FA-8663', '69', 74, '7/12/20', '$4.32 ', NULL, '$4.32 ', 'Dale Adams, DALE ADAMS, 1861 RAVEN AVE UNIT A8, ESTES PARK, CO 80517-9417', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 8oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(332, 'FA-8664', '69', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Aliyliyah  Jansma, ALIYLIYAH JANSMA, 4515 WESTCHESTER GLEN DR, GRAND PRAIRIE, TX 75052-3550', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(333, 'FA-8665', '69', 74, '7/12/20', '$3.93 ', NULL, '$3.93 ', 'Lisa M. Fantinato, LISA M. FANTINATO, 634 SEVERN RD, SEVERNA PARK, MD 21146-3421', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 11oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(334, 'FA-8666', '69', 74, '7/12/20', '$3.05 ', NULL, '$3.05 ', 'Amber Reichelt, AMBER REICHELT, 1506 NOLAN ST, SAN ANTONIO, TX 78202-2441', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(335, 'FA-8667', '69', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Leslie  French, LESLIE FRENCH, 2316 RANCH WAY, LAWRENCE, KS 66047-3324', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 2oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(336, 'FA-8668', '69', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Patricia Biagioni, PATRICIA BIAGIONI, 203 RIDGE RD, JUPITER, FL 33477-9660', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(337, 'FA-8669', '69', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'ANNE  HOWARD, ANNE HOWARD, 3145 EL KU AVE, ESCONDIDO, CA 92025-7333', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(338, 'FA-8670', '69', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'Stacy Aralica, STACY ARALICA, 9021 CAPE WOOD CT # 3, LAS VEGAS, NV 89117-2321', 'Printed', '9.4e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:41:23', '2020-07-16 08:41:23'),
(339, 'BA-2-1001', '70', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Cathy Baillet, CATHY BAILLET, 2459 FIR ST, GLENVIEW, IL 60025-2705', 'Printed', '94001115123412341241', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 1oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(340, 'BA-2-1002', '70', 74, '7/12/20', '$3.67 ', NULL, '$3.67 ', 'Betty Meyer, BETTY MEYER, 3855 MADISON AVE, SOUTH OGDEN, UT 84403-2203', 'Printed', '94001115123412341242', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 7oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(341, 'BA-2-1003', '70', 74, '7/12/20', '$4.32 ', NULL, '$4.32 ', 'Dale Adams, DALE ADAMS, 1861 RAVEN AVE UNIT A8, ESTES PARK, CO 80517-9417', 'Printed', '94001115123412341243', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 8oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(342, 'BA-2-1004', '70', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Aliyliyah  Jansma, ALIYLIYAH JANSMA, 4515 WESTCHESTER GLEN DR, GRAND PRAIRIE, TX 75052-3550', 'Printed', '94001115123412341244', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(343, 'BA-2-1005', '70', 74, '7/12/20', '$3.93 ', NULL, '$3.93 ', 'Lisa M. Fantinato, LISA M. FANTINATO, 634 SEVERN RD, SEVERNA PARK, MD 21146-3421', 'Printed', '94001115123412341245', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 11oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(344, 'BA-2-1006', '70', 74, '7/12/20', '$3.05 ', NULL, '$3.05 ', 'Amber Reichelt, AMBER REICHELT, 1506 NOLAN ST, SAN ANTONIO, TX 78202-2441', 'Printed', '94001115123412341246', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(345, 'BA-2-1007', '70', 74, '7/12/20', '$2.93 ', NULL, '$2.93 ', 'Leslie  French, LESLIE FRENCH, 2316 RANCH WAY, LAWRENCE, KS 66047-3324', 'Printed', '94001115123412341247', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 2oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(346, 'BA-2-1008', '70', 74, '7/12/20', '$2.84 ', NULL, '$2.84 ', 'Patricia Biagioni, PATRICIA BIAGIONI, 203 RIDGE RD, JUPITER, FL 33477-9660', 'Printed', '94001115123412341248', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(347, 'BA-2-1009', '70', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'ANNE  HOWARD, ANNE HOWARD, 3145 EL KU AVE, ESCONDIDO, CA 92025-7333', 'Printed', '94001115123412341249', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(348, 'BA-2-1000', '70', 74, '7/12/20', '$3.18 ', NULL, '$3.18 ', 'Stacy Aralica, STACY ARALICA, 9021 CAPE WOOD CT # 3, LAS VEGAS, NV 89117-2321', 'Printed', '94001115123412341250', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 0oz', '7/17/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:57:32', '2020-07-16 08:57:32'),
(349, '70', '70', 75, NULL, NULL, NULL, NULL, NULL, NULL, '435643565456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/22/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 08:58:12', '2020-07-16 08:58:12'),
(350, '40', '40', 73, NULL, NULL, NULL, NULL, NULL, NULL, '4567894561', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06/30/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 11:52:46', '2020-07-16 11:52:46'),
(351, '72', '72', 75, NULL, NULL, NULL, NULL, NULL, NULL, '343453345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/22/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 21:31:28', '2020-07-16 21:31:28'),
(352, '73', '73', 75, NULL, NULL, NULL, NULL, NULL, NULL, '34623534', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/14/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16 21:45:19', '2020-07-16 21:45:19'),
(353, '74', '74', 76, NULL, NULL, NULL, NULL, NULL, NULL, '439857398573', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/28/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 06:52:38', '2020-07-20 06:52:38'),
(354, '75', '75', 75, NULL, NULL, NULL, NULL, NULL, NULL, '456456546', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/22/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:23:41', '2020-07-20 08:23:41'),
(355, 'US-1155', '75', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'Elizabeth Huntington, ELIZABETH HUNTINGTON, 5092 MANDALAY SPRINGS DR UNIT 101, LAS VEGAS, NV 89120-5235', 'Printed', '9400111699000928283530', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(356, 'US-1154', '75', 74, '7/19/20', '$2.76 ', NULL, '$2.76 ', 'Jessica Lanham, JESSICA LANHAM, 2704 TIMBER CT, RICHMOND, VA 23228-1125', 'Printed', '9400111699000928283578', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(357, 'US-1130', '75', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Deryl Brunner, DERYL BRUNNER, 11049 S SMITH RD, PERRINTON, MI 48871-9717', 'Printed', '9400111699000928284216', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(358, 'US-1129', '75', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Dorothy Mosby, DOROTHY MOSBY, 438 LARKWOOD DR, MONTGOMERY, AL 36109-4946', 'Printed', '9400111699000928284209', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(359, 'US-1128', '75', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'I V Jackson, I V JACKSON, 804 OLDE PIONEER TRL APT 140, KNOXVILLE, TN 37923-6218', 'Printed', '9400111699000928284247', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(360, 'US-1127', '75', 74, '7/19/20', '$3.05 ', NULL, '$3.05 ', 'celia pereira, CELIA PEREIRA, 10201 CRISTALINO RD SW, ALBUQUERQUE, NM 87121-5424', 'Printed', '9400111699000928284278', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(361, 'US-1126', '75', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Lisa OQuinn, LISA OQUINN, 628 WOODBINE DR, PENSACOLA, FL 32503-3242', 'Printed', '9400111699000928284858', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(362, 'US-1125', '75', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'CHRISTINA WILSON, CHRISTINA WILSON, 559 E 2600 N, PROVO, UT 84604-5902', 'Printed', '9400111699000928284827', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(363, 'US-1124', '75', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '9400111699000928284841', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:24:27', '2020-07-20 08:24:27'),
(364, '76', '76', 73, NULL, NULL, NULL, NULL, NULL, NULL, 'SFSF4587541', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06/30/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:37:46', '2020-07-20 08:37:46'),
(365, 'SKY-BOX 1', '76', 74, '06-05-2020', '$2.74 ', NULL, '$2.74 ', 'ALICIA FONTAINE, ALICIA FONTAINE, 320 TENNESSEE AVE, ELIZABETHVILLE, PA 17023-9640', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:41:33', '2020-07-20 08:41:33'),
(366, 'SKY-BOX 2', '76', 74, '06-05-2020', '$3.52 ', NULL, '$3.52 ', 'Amanda long, AMANDA LONG, 5504 ALVARADO RD, AMARILLO, TX 79106-5010', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 6oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:41:33', '2020-07-20 08:41:33'),
(367, 'SKY-BOX 3', '76', 74, '06-05-2020', '$3.18 ', NULL, '$3.18 ', 'AMELIA TAMMEL, AMELIA TAMMEL, 324 BROOKS ST, LAGUNA BEACH, CA 92651-2931', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:41:33', '2020-07-20 08:41:33'),
(368, 'SKY-BOX 4', '76', 74, '06-05-2020', '$2.84 ', NULL, '$2.84 ', 'amina Luna, amina Luna, 2266 NW 33rd st, Miami, FL 33142-5470', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:41:33', '2020-07-20 08:41:33'),
(369, 'SKY-BOX 5', '76', 74, '06-05-2020', '$4.18 ', NULL, '$4.18 ', 'Amy DeVoe-Moses, AMY DEVOE-MOSES, PO BOX 1602, TRINITY, TX 75862-1602', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 9oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:41:33', '2020-07-20 08:41:33'),
(370, '77', '77', 75, NULL, NULL, NULL, NULL, NULL, NULL, '3456345345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/30/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:48:49', '2020-07-20 08:48:49'),
(371, 'US-1160', '77', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'Elizabeth Huntington, ELIZABETH HUNTINGTON, 5092 MANDALAY SPRINGS DR UNIT 101, LAS VEGAS, NV 89120-5235', 'Printed', '9400111699000928283530', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(372, 'US-1161', '77', 74, '7/19/20', '$2.76 ', NULL, '$2.76 ', 'Jessica Lanham, JESSICA LANHAM, 2704 TIMBER CT, RICHMOND, VA 23228-1125', 'Printed', '9400111699000928283578', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(373, 'US-1162', '77', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Deryl Brunner, DERYL BRUNNER, 11049 S SMITH RD, PERRINTON, MI 48871-9717', 'Printed', '9400111699000928284216', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(374, 'US-1163', '77', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Dorothy Mosby, DOROTHY MOSBY, 438 LARKWOOD DR, MONTGOMERY, AL 36109-4946', 'Printed', '9400111699000928284209', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(375, 'US-1164', '77', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'I V Jackson, I V JACKSON, 804 OLDE PIONEER TRL APT 140, KNOXVILLE, TN 37923-6218', 'Printed', '9400111699000928284247', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(376, 'US-1165', '77', 74, '7/19/20', '$3.05 ', NULL, '$3.05 ', 'celia pereira, CELIA PEREIRA, 10201 CRISTALINO RD SW, ALBUQUERQUE, NM 87121-5424', 'Printed', '9400111699000928284278', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(377, 'US-1166', '77', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Lisa OQuinn, LISA OQUINN, 628 WOODBINE DR, PENSACOLA, FL 32503-3242', 'Printed', '9400111699000928284858', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(378, 'US-1167', '77', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'CHRISTINA WILSON, CHRISTINA WILSON, 559 E 2600 N, PROVO, UT 84604-5902', 'Printed', '9400111699000928284827', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(379, 'US-1168', '77', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '9400111699000928284841', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 08:49:26', '2020-07-20 08:49:26'),
(380, '78', '78', 73, NULL, NULL, NULL, NULL, NULL, NULL, '546JKUI45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/29/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 09:23:17', '2020-07-20 09:23:17'),
(381, 'AK-BOX 1', '78', 74, '06-05-2020', '$2.74 ', NULL, '$2.74 ', 'ALICIA FONTAINE, ALICIA FONTAINE, 320 TENNESSEE AVE, ELIZABETHVILLE, PA 17023-9640', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 09:23:59', '2020-07-20 09:23:59'),
(382, 'AK-BOX 2', '78', 74, '06-05-2020', '$3.52 ', NULL, '$3.52 ', 'Amanda long, AMANDA LONG, 5504 ALVARADO RD, AMARILLO, TX 79106-5010', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 6oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 09:23:59', '2020-07-20 09:23:59'),
(383, 'AK-BOX 3', '78', 74, '06-05-2020', '$3.18 ', NULL, '$3.18 ', 'AMELIA TAMMEL, AMELIA TAMMEL, 324 BROOKS ST, LAGUNA BEACH, CA 92651-2931', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 09:23:59', '2020-07-20 09:23:59'),
(384, 'AK-BOX 4', '78', 74, '06-05-2020', '$2.84 ', NULL, '$2.84 ', 'amina Luna, amina Luna, 2266 NW 33rd st, Miami, FL 33142-5470', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 09:23:59', '2020-07-20 09:23:59'),
(385, 'AK-BOX 5', '78', 74, '06-05-2020', '$4.18 ', NULL, '$4.18 ', 'Amy DeVoe-Moses, AMY DEVOE-MOSES, PO BOX 1602, TRINITY, TX 75862-1602', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 9oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 09:23:59', '2020-07-20 09:23:59'),
(386, '81', '81', 75, NULL, NULL, NULL, NULL, NULL, NULL, '456456456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/29/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 10:42:14', '2020-07-20 10:42:14'),
(387, 'US-1093', '81', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'Elizabeth Huntington, ELIZABETH HUNTINGTON, 5092 MANDALAY SPRINGS DR UNIT 101, LAS VEGAS, NV 89120-5235', 'Printed', '9400111699000928281000', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(388, 'US-1092', '81', 74, '7/19/20', '$2.76 ', NULL, '$2.76 ', 'Jessica Lanham, JESSICA LANHAM, 2704 TIMBER CT, RICHMOND, VA 23228-1125', 'Printed', '9400111699000928281001', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(389, 'US-1091', '81', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Deryl Brunner, DERYL BRUNNER, 11049 S SMITH RD, PERRINTON, MI 48871-9717', 'Printed', '9400111699000928281002', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(390, 'US-1090', '81', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Dorothy Mosby, DOROTHY MOSBY, 438 LARKWOOD DR, MONTGOMERY, AL 36109-4946', 'Printed', '9400111699000928281003', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(391, 'US-1089', '81', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'I V Jackson, I V JACKSON, 804 OLDE PIONEER TRL APT 140, KNOXVILLE, TN 37923-6218', 'Printed', '9400111699000928281004', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(392, 'US-1088', '81', 74, '7/19/20', '$3.05 ', NULL, '$3.05 ', 'celia pereira, CELIA PEREIRA, 10201 CRISTALINO RD SW, ALBUQUERQUE, NM 87121-5424', 'Printed', '9400111699000928281005', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(393, 'US-1087', '81', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Lisa OQuinn, LISA OQUINN, 628 WOODBINE DR, PENSACOLA, FL 32503-3242', 'Printed', '9400111699000928281006', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(394, 'US-1086', '81', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'CHRISTINA WILSON, CHRISTINA WILSON, 559 E 2600 N, PROVO, UT 84604-5902', 'Printed', '9400111699000928281007', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(395, 'US-1085', '81', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '9400111699000928281008', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(396, 'US-1084', '81', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '9400111699000928281009', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:43:03', '2020-07-20 11:43:03'),
(397, '79', '79', 73, NULL, NULL, NULL, NULL, NULL, NULL, '32135461', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/15/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 11:57:13', '2020-07-20 11:57:13'),
(432, 'AKSK-BOX 5', '79', 74, '06-05-2020', '$4.18 ', NULL, '$4.18 ', 'Amy DeVoe-Moses, AMY DEVOE-MOSES, PO BOX 1602, TRINITY, TX 75862-1602', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 9oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 12:56:03', '2020-07-20 12:56:03'),
(430, 'AKSK-BOX 3', '79', 74, '06-05-2020', '$3.18 ', NULL, '$3.18 ', 'AMELIA TAMMEL, AMELIA TAMMEL, 324 BROOKS ST, LAGUNA BEACH, CA 92651-2931', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 12:56:03', '2020-07-20 12:56:03'),
(431, 'AKSK-BOX 4', '79', 74, '06-05-2020', '$2.84 ', NULL, '$2.84 ', 'amina Luna, amina Luna, 2266 NW 33rd st, Miami, FL 33142-5470', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 12:56:03', '2020-07-20 12:56:03'),
(428, 'AKSK-BOX 1', '79', 74, '06-05-2020', '$2.74 ', NULL, '$2.74 ', 'ALICIA FONTAINE, ALICIA FONTAINE, 320 TENNESSEE AVE, ELIZABETHVILLE, PA 17023-9640', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 4oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 12:56:02', '2020-07-20 12:56:02'),
(429, 'AKSK-BOX 2', '79', 74, '06-05-2020', '$3.52 ', NULL, '$3.52 ', 'Amanda long, AMANDA LONG, 5504 ALVARADO RD, AMARILLO, TX 79106-5010', 'Printed', '9.40011e21', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 6oz', '06-05-2020', 'E-refund', 'Samsar', NULL, NULL, NULL, NULL, NULL, '2020-07-20 12:56:03', '2020-07-20 12:56:03'),
(433, '82', '82', 75, NULL, NULL, NULL, NULL, NULL, NULL, '4323634563456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/29/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:30', '2020-07-20 13:06:30'),
(434, 'US-A-A1', '82', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'Elizabeth Huntington, ELIZABETH HUNTINGTON, 5092 MANDALAY SPRINGS DR UNIT 101, LAS VEGAS, NV 89120-5235', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(435, 'US-A-A2', '82', 74, '7/19/20', '$2.76 ', NULL, '$2.76 ', 'Jessica Lanham, JESSICA LANHAM, 2704 TIMBER CT, RICHMOND, VA 23228-1125', 'Printed', '56456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(436, 'US-A-A3', '82', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Deryl Brunner, DERYL BRUNNER, 11049 S SMITH RD, PERRINTON, MI 48871-9717', 'Printed', '456463456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(437, 'US-A-A4', '82', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Dorothy Mosby, DOROTHY MOSBY, 438 LARKWOOD DR, MONTGOMERY, AL 36109-4946', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(438, 'US-A-A5', '82', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'I V Jackson, I V JACKSON, 804 OLDE PIONEER TRL APT 140, KNOXVILLE, TN 37923-6218', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(439, 'US-A-A6', '82', 74, '7/19/20', '$3.05 ', NULL, '$3.05 ', 'celia pereira, CELIA PEREIRA, 10201 CRISTALINO RD SW, ALBUQUERQUE, NM 87121-5424', 'Printed', '45645', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(440, 'US-A-A7', '82', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Lisa OQuinn, LISA OQUINN, 628 WOODBINE DR, PENSACOLA, FL 32503-3242', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(441, 'US-A-A8', '82', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'CHRISTINA WILSON, CHRISTINA WILSON, 559 E 2600 N, PROVO, UT 84604-5902', 'Printed', '34563456346', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(442, 'US-A-A9', '82', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(443, 'US-A-A10', '82', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '345634563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-20 13:06:57', '2020-07-20 13:06:57'),
(444, '92', '92', 75, NULL, NULL, NULL, NULL, NULL, NULL, '253465345345435', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/29/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:45:02', '2020-07-21 13:45:02'),
(445, 'RZ-1934', '92', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'Elizabeth Huntington, ELIZABETH HUNTINGTON, 5092 MANDALAY SPRINGS DR UNIT 101, LAS VEGAS, NV 89120-5235', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(446, 'RZ-1935', '92', 74, '7/19/20', '$2.76 ', NULL, '$2.76 ', 'Jessica Lanham, JESSICA LANHAM, 2704 TIMBER CT, RICHMOND, VA 23228-1125', 'Printed', '56456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(447, 'RZ-1936', '92', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Deryl Brunner, DERYL BRUNNER, 11049 S SMITH RD, PERRINTON, MI 48871-9717', 'Printed', '456463456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(448, 'RZ-1937', '92', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Dorothy Mosby, DOROTHY MOSBY, 438 LARKWOOD DR, MONTGOMERY, AL 36109-4946', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(449, 'RZ-1939', '92', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'I V Jackson, I V JACKSON, 804 OLDE PIONEER TRL APT 140, KNOXVILLE, TN 37923-6218', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(450, 'RZ-1940', '92', 74, '7/19/20', '$3.05 ', NULL, '$3.05 ', 'celia pereira, CELIA PEREIRA, 10201 CRISTALINO RD SW, ALBUQUERQUE, NM 87121-5424', 'Printed', '45645', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(451, 'RZ-1941', '92', 74, '7/19/20', '$2.84 ', NULL, '$2.84 ', 'Lisa OQuinn, LISA OQUINN, 628 WOODBINE DR, PENSACOLA, FL 32503-3242', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(452, 'RZ-1942', '92', 74, '7/19/20', '$3.18 ', NULL, '$3.18 ', 'CHRISTINA WILSON, CHRISTINA WILSON, 559 E 2600 N, PROVO, UT 84604-5902', 'Printed', '34563456346', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(453, 'RZ-1962', '92', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '34563456', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(454, 'RZ-1963', '92', 74, '7/19/20', '$2.78 ', NULL, '$2.78 ', 'Nansi Mc Daniel, NANSI MC DANIEL, 141 E 39TH ST, HOLLAND, MI 49423-7064', 'Printed', '345635000000', NULL, 'USPS', 'First Class (R)', '$0.00 ', NULL, '<None>', '0lb 3oz', '7/25/20', 'E-refund', 'samsar-ships', NULL, NULL, NULL, NULL, NULL, '2020-07-21 13:49:16', '2020-07-21 13:49:16'),
(455, '84', '84', 73, NULL, NULL, NULL, NULL, NULL, NULL, '2313565jhkj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/29/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-22 23:55:53', '2020-07-22 23:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `orgid` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dribbble` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `orgid`, `first_name`, `last_name`, `username`, `email`, `password`, `address`, `phone`, `country`, `date_of_birth`, `role`, `status`, `gender`, `image`, `google`, `facebook`, `twitter`, `linkedin`, `skype`, `dribbble`, `remember_token`, `created_at`, `updated_at`) VALUES
(61, 6, 'Admin', '123', 'admin123', 'admin@samsar.com', '$2y$10$r.cGQQnpf5tEWNiQEkcXAOvJwGZv2nwhMLT9ZLOvww84/64b8d5.y', 'Test', '01745519614', 'Bangladesh', '2015-01-28', 'Admin', 'Active', 'Male', '1480345486.png', 'https://www.google.com/', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', 'jewel_farazi', 'https://dribbble.com/', '0hVb6dF66mP1BibI7BXepLaqpid4yR1HpccjLEv3Yui7cTosdCxSKCAzVqEb', '2016-10-22 12:03:26', '2020-06-19 15:45:54'),
(68, 6, 'user', '123', 'user123', 'user@samsar.com', '$2y$10$NhdbCkl2Ir4K.gPkOf9ICegZw72LRkF5mTRiizpkVC6i.cx4cgCge', 'dsdd', '01745519614', 'India', '2016-09-28', 'Sub Admin', 'Active', 'Male', '1482937747.png', 'https://www.google.com/', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com/', 'jewel_farazi', 'https://dribbble.com/', 'EGtfmWFMXnCne8Fy4IfRhH5pOi1bmejYfWaOUMzMSI5N0ZCmLCNXAiQB2aHZ', '2016-10-22 12:03:26', '2020-06-19 15:44:55'),
(69, 3, 'Rupa', 'Khandelwal', 'rupa12', 'ajayji@gmail.com', '$2y$10$ByAIZFvchxUMhdlfx44.XujEE7jNAV68A.3pIZVrwZcz7OYdAHRKm', 'nashik', '1234567890', 'United States', '06/24/2020', 'Sub Admin', 'Active', 'Male', '', '', '', '', '', '', '', NULL, '2020-06-12 20:47:27', '2020-06-19 15:45:38'),
(70, 7, 'Rajat', 'Khandelwal', 'rjt@sjg.com', 'aakash@puretechnology.in', '$2y$10$aWTHRfFJmMJqfp1jEDaQLu5NNQtD/XvoDrd4dO5PY.mrQlyuXYQg2', '83 RAJEEV PURI GOLIMAR GARDEN\r\nRAMGARH MODE', '', 'India', '', 'Sub Admin', 'Active', 'Male', '', '', '', '', '', '', '', NULL, '2020-06-18 14:51:43', '2020-06-18 14:51:43'),
(72, 0, 'Web', 'Master', 'webmaster@samsar.com', 'webmaster@samsar.com', '$2y$10$ARizJ14nx7g.xGas.c9kHerpbxIXqvlpLS4GWTO5xB.PfqLXP8klq', '', '4848380864', '', '', 'Webmaster', 'Active', 'Male', '', '', '', '', '', '', '', 'fWdmHnN64wUOPgRwOoLLLcUtGRes0LLtX1zoBm9pNUcNShT2yaYxiv1QLVLq', '2020-06-19 21:56:47', '2020-06-19 21:56:47'),
(73, 7, 'I-customer', 'seller', 'customer@samsar.com', 'customer@samsar.com', '$2y$10$5RrI9MPH8LtkqFw35XEfI.Jt0Eo.QPyz.jool0Uw/OqZ23CzGJO2K', '', '', 'India', '', 'I-Customer', 'Active', 'Male', '', '', '', '', '', '', '', 'tkTUWMq2RKl6te3EUkjmxTmnOuNksxihSE0ktRrVIywK0fO5yZBGOrzNBN0u', '2020-06-19 22:21:04', '2020-06-21 10:20:05'),
(74, 7, 'Ajay', 'Khandelwal', 'agent@samsar.com', 'agent@samsar.com', '$2y$10$NG73sLnTsjFdWCQFqttR/ufPsnMo/dBRkmDRM7YMlc1Ky54O.zUi2', '', '4848380864', '', '', 'Agent', 'Active', 'Male', '', '', '', '', '', '', '', 'gbTHioQ3C18mhvIEqymkVJ6EsiLrYwao9IivbO7XNeZGlz1LBgc0T9gVOyyI', '2020-06-20 07:43:35', '2020-06-20 07:43:35'),
(75, 8, 'Bharat', 'Agrawal', 'bharat@samsar.com', 'bharat@samsar.com', '$2y$10$MxXE1S9k/z468HHpnI6DLu7LRHos1ff4L1t5R/v8gDnc7Szb6hvLa', '', '9876543210', '', '', 'Customer', 'Active', 'Male', '', '', '', '', '', '', '', 'Trq9UyHg7MnO9kefFH9KP5dYcIzBoLIwICXlouKy6mYWZm3CobjzGEZ0LdOx', '2020-06-20 15:10:33', '2020-06-20 15:10:33'),
(76, 7, 'Icustomer', 'Samsar', 'i-customer@samsar.com', 'i-customer@samsar.com', '$2y$10$r.cGQQnpf5tEWNiQEkcXAOvJwGZv2nwhMLT9ZLOvww84/64b8d5.y', '', '', '', '', 'I-Customer', 'Active', 'Male', '', '', '', '', '', '', '', 'pAGHLFgHxzqMHhCK67JqbSava6p953ghpDl7tbBvKASblRznz5fPTGj3hlup', '2020-06-21 10:24:49', '2020-06-21 10:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`id`, `description`, `user_id`, `ip_address`, `user_agent`, `created_at`, `updated_at`) VALUES
(1, 'Role Updated.', 61, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-10 08:40:28', '2019-01-10 08:40:28'),
(7, 'Role Delete.', 61, '157.33.167.182', 'Mozilla/5.0 (Windows NT 6.1; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-11 13:55:01', '2020-06-11 13:55:01'),
(8, 'Role Updated.', 61, '157.33.167.182', 'Mozilla/5.0 (Windows NT 6.1; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-11 13:55:56', '2020-06-11 13:55:56'),
(9, 'Permission Delete.', 61, '157.33.167.182', 'Mozilla/5.0 (Windows NT 6.1; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-11 13:56:29', '2020-06-11 13:56:29'),
(10, 'Permission Delete.', 61, '157.33.167.182', 'Mozilla/5.0 (Windows NT 6.1; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-11 13:56:43', '2020-06-11 13:56:43'),
(11, 'Permission Delete.', 61, '157.33.167.182', 'Mozilla/5.0 (Windows NT 6.1; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-11 13:56:54', '2020-06-11 13:56:54'),
(12, 'Permission Delete.', 61, '157.33.167.182', 'Mozilla/5.0 (Windows NT 6.1; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-11 13:57:05', '2020-06-11 13:57:05'),
(13, 'Order Placed', 61, '49.35.117.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-12 14:00:32', '2020-06-12 14:00:32'),
(14, 'Order Placed', 68, '49.35.117.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-12 14:02:58', '2020-06-12 14:02:58'),
(15, 'User Added', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:77.0) Gecko/20100101 Firefox/77.0', '2020-06-13 01:47:27', '2020-06-13 01:47:27'),
(16, 'User Updated.', 61, '49.35.110.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-17 14:40:53', '2020-06-17 14:40:53'),
(17, 'User Updated.', 61, '49.35.110.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-17 14:42:36', '2020-06-17 14:42:36'),
(18, 'User Added', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-18 19:51:43', '2020-06-18 19:51:43'),
(19, 'User Updated.', 68, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 00:39:27', '2020-06-19 00:39:27'),
(20, 'Roles wise permission Update', 61, '49.35.117.201', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-19 14:48:17', '2020-06-19 14:48:17'),
(21, 'User Updated.', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 20:44:55', '2020-06-19 20:44:55'),
(22, 'User Updated.', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 20:45:38', '2020-06-19 20:45:38'),
(23, 'User Updated.', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 20:45:54', '2020-06-19 20:45:54'),
(24, 'Role Added', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 23:26:23', '2020-06-19 23:26:23'),
(25, 'Role Added', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 23:26:38', '2020-06-19 23:26:38'),
(26, 'Role Updated.', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 23:27:03', '2020-06-19 23:27:03'),
(27, 'Role Added', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-19 23:27:22', '2020-06-19 23:27:22'),
(28, 'Role Added', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 00:34:22', '2020-06-20 00:34:22'),
(29, 'Permission Updated', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 01:37:09', '2020-06-20 01:37:09'),
(30, 'Roles wise permission Update', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 01:39:18', '2020-06-20 01:39:18'),
(31, 'Permission Updated', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 01:45:07', '2020-06-20 01:45:07'),
(32, 'Permission Updated', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 01:45:19', '2020-06-20 01:45:19'),
(33, 'Permission Updated', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 01:46:06', '2020-06-20 01:46:06'),
(34, 'Roles wise permission Update', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 01:47:30', '2020-06-20 01:47:30'),
(35, 'Roles wise permission Update', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 02:52:40', '2020-06-20 02:52:40'),
(36, 'User Added', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 02:56:47', '2020-06-20 02:56:47'),
(37, 'Roles wise permission Update', 61, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 03:01:03', '2020-06-20 03:01:03'),
(38, 'User Added', 72, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 03:21:04', '2020-06-20 03:21:04'),
(39, 'User Updated.', 73, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 03:23:03', '2020-06-20 03:23:03'),
(40, 'User Added', 72, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 12:43:35', '2020-06-20 12:43:35'),
(41, 'User Added', 72, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', '2020-06-20 20:10:33', '2020-06-20 20:10:33'),
(42, 'User Updated.', 72, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-21 15:19:02', '2020-06-21 15:19:02'),
(43, 'User Updated.', 72, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-21 15:20:05', '2020-06-21 15:20:05'),
(44, 'User Added', 72, '72.94.92.62', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36', '2020-06-21 15:24:49', '2020-06-21 15:24:49'),
(45, 'Role Delete.', 61, '120.138.125.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', '2021-02-25 07:32:52', '2021-02-25 07:32:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`receiver_id`),
  ADD KEY `FK_message_user_2` (`sender_id`);

--
-- Indexes for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
  ADD PRIMARY KEY (`orderid`);

--
-- Indexes for table `OrderStatus`
--
ALTER TABLE `OrderStatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `tbl_company`
--
ALTER TABLE `tbl_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trackings`
--
ALTER TABLE `trackings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_activity_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1227;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `OrderDetails`
--
ALTER TABLE `OrderDetails`
  MODIFY `orderid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `OrderStatus`
--
ALTER TABLE `OrderStatus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_company`
--
ALTER TABLE `tbl_company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `trackings`
--
ALTER TABLE `trackings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=456;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `FK_message_users` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_message_users_2` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD CONSTRAINT `user_activity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
